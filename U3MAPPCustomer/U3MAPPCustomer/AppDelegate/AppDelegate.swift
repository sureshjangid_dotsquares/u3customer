//
//  AppDelegate.swift
//  U3MAPPCustomer
//
//  Created by Dheeraj Kumar on 09/01/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SWRevealViewController
import GooglePlaces
import GoogleMaps
import GoogleSignIn
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import TwitterKit


@UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        registerForPushNotifications(application)
        
        setupIQKeyboardManager()
        
        let manager = PCLocationManager.shared()!
        manager.startUpdateingLocation()
        
        // google account
        //findme@Mcfaps.com
        configureGoogleSignIn()
        
        GMSServices.provideAPIKey(GoogleConstants.apiKey)
        GMSPlacesClient.provideAPIKey(GoogleConstants.apiKey)
        
        TWTRTwitter.sharedInstance().start(withConsumerKey: TwitterConstants.consumerKey, consumerSecret: TwitterConstants.consumerSecretKey)
        
        AppManager.shared.showRootView()
//        APPDELEGATE.showCompleteProfileWindow()
        
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )

        
        return true
    }
    
    private func setupIQKeyboardManager() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        let appId: String = Settings.appID ?? ""
//        if url.scheme != nil && url.scheme!.hasPrefix("fb\(appId)") && url.host ==  "authorize" {
//            return ApplicationDelegate.shared.application(app, open: url, options: options)
//        }
        
        if TWTRTwitter.sharedInstance().application(app, open: url, options: options){
            return true
        }
        
        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
        return false
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
        if LISDKCallbackHandler.shouldHandle(url) {
            return LISDKCallbackHandler.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        AppManager.shared.isAdOpenedAlready = false
    }

    // MARK: - Show Login Window
    func showLoginWindow() {
        let loginVC: U3NavigationController = Storyboards.loginStoryboard.instantiateViewController(withIdentifier: StoryboardIdentifiers.loginNavigation) as! U3NavigationController
        self.window?.rootViewController = loginVC
    }
    
    // MARK: - Show Home Window
    func showHomeWindow() {
        let homeVC = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: StoryboardIdentifiers.dashboardNavigation)
        self.window?.rootViewController = homeVC
    }
    
    // MARK: - Show Complete Profile Window
    func showCompleteProfileWindow() {
        let homeVC: U3NavigationController = Storyboards.loginStoryboard.instantiateViewController(withIdentifier: StoryboardIdentifiers.completeProfileNavigation) as! U3NavigationController
        self.window?.rootViewController = homeVC
    }
    
    // MARK:  Api Logout Endpoint
    
    func wsLogout(completion: @escaping (_ result: Bool)->())
    {
        showHud()
        let headers = AppManager.shared.getLoggedInHeaders()
        
        WebServiceHelper.request(path: .logout, method: .post, token: nil, headers: headers, parameters: ["":""]) { (response, error, isSuccess) in
            dissmissHud()
            DispatchQueue.main.async {
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            completion(true)
                        }else{
                            completion(false)
                        }
                    }
                }else{
                    completion(false)
                }
                
            }
        }
        
    }

}

