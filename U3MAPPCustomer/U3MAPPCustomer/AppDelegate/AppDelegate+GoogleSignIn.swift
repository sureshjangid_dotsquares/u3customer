//
//  AppDelegate+GoogleSignIn.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 13/04/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import GoogleSignIn
 
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase
 

extension Notification.Name {
    static let ReceivedGoogleData = Notification.Name("ReceivedGoogleData")
    
}


extension AppDelegate:GIDSignInDelegate
{
    func configureGoogleSignIn()
    {
        // Initialize sign-in
        GIDSignIn.sharedInstance().clientID = GoogleConstants.clientId
        GIDSignIn.sharedInstance().delegate = self
    }
    func configureFBLogin( application: UIApplication, launchOptions: [UIApplication.LaunchOptionsKey: Any]?)
    {
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        print("*****Application can open url -- > \(url)" )
//        return true
//    }
    func application(application: UIApplication,
                     openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
        
        print("*****Application can open url -- > \(url)" )
        
        //let bool=FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
        
        return  (GIDSignIn.sharedInstance()?.handle(url as URL))!
    }
    //MARK : - GIDSignInDelegate
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
       
        
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            let userId = user.userID ?? ""                 // For client-side use only!
            let idToken = user.authentication.idToken  ?? ""// Safe to send to the server
            let fullName = user.profile.name ?? ""
            let givenName = user.profile.givenName ?? ""
            let email = user.profile.email ?? ""
            let imageURL = user.profile.imageURL(withDimension: 500).absoluteString
            let firstName = ""
            let lastName = ""
            
            let model=SocialModel(email: email, userId: userId, idToken: idToken, fullName: fullName, givenName: givenName,imageURL:imageURL, firstName: firstName, lastName: lastName)
            NotificationCenter.default.post(name: .ReceivedGoogleData, object: nil, userInfo: ["model":model])
        }
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
    }
}



