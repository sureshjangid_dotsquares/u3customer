//
//  AppDelegate+Notification.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 16/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import UserNotifications
import FirebaseMessaging


extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        NSLog("Device Token %@", deviceTokenString)
        print(String(format: "Device Token %@", deviceTokenString))
        Messaging.messaging().apnsToken = deviceToken
        
        AppManager.shared.deviceToken = Messaging.messaging().fcmToken ?? deviceTokenString
        print("Device Token For firebase:-",AppManager.shared.deviceToken as Any)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Error while fetching token data! \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        applicationDidReceiveRemoteNotification(application.applicationState, notificationInfo:userInfo)
    }
    
    //MARK:- setup application Did Receive Remote Notification In Background
    func applicationDidReceiveRemoteNotification(_ appState: UIApplication.State, notificationInfo: [AnyHashable: Any]) {
        
        print(notificationInfo)
        NSLog("%@",notificationInfo)
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        if let objDictionary = notificationInfo as? [String:AnyObject] {
            
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
        print("###### Notification Info: \(response.notification.request.content.userInfo)")
        NSLog("%@",response.notification.request.content.userInfo)
        applicationDidReceiveRemoteNotification(UIApplication.State.background, notificationInfo: response.notification.request.content.userInfo)
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter( _ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping ( _ options:   UNNotificationPresentationOptions) -> Void) {
        
        print("###### Notification Info: \(notification.request.content.userInfo)")
        NSLog("%@",notification.request.content.userInfo)
        completionHandler([.alert, .sound])
        // applicationDidReceiveRemoteNotification(UIApplication.State.active, notificationInfo: notification.request.content.userInfo)
    }
    
    //MARK:- Setup Push Notification Setting
    func registerForPushNotifications(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil {
                    DispatchQueue.main.async {
                        application.registerForRemoteNotifications()
                        UIApplication.shared.registerForRemoteNotifications()
                        
                        // For iOS 10 data message (sent via FCM)
                        Messaging.messaging().delegate = self
                    }
                }
            }
        } else {
            let notificationSettings = UIUserNotificationSettings(
                types: [.badge, .sound, .alert], categories: nil)
            application.registerUserNotificationSettings(notificationSettings)
            application.registerForRemoteNotifications()
        }
    }
}

// MARK: - FCM Messaging (Notification) MessagingDelegate.
extension AppDelegate: MessagingDelegate {
    
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}
