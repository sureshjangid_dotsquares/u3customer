//
//  Constant.swift
//  U3MAPPCustomer
//
//  Created by Dheeraj Kumar on 10/01/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

let APPLICATION_NAME = Bundle.main.infoDictionary!["CFBundleName"] as! String
let APPDELEGATE: AppDelegate = UIApplication.shared.delegate as! AppDelegate

typealias UITableViewMethods = UITableViewDelegate & UITableViewDataSource

/**DEMO URL*/
//let baseURL = "http://api.projectstatus.in/"
//let termsURL = "http://3usystem.projectstatus.in/termsofuse"

/**UAT URL*/
let baseURL = "http://apiuat.projectstatus.in/"
let termsURL = "http://3usystemuat.projectstatus.in/termsofuse"

//static let privacyURL = "http://3usystem.projectstatus.in/home/privacy"

// App Button Hex Color Code
let firstColorHex = "#9BEEFF"
let secondColorHex = "#E5F7FF"
let thirdColorHex = "#EDF9FF"
let forthColorHex = "#EAFFE0"
let skyColorHex = "#2DCBEB"
let grayColorHex = "6D6D6D"

let rFirstCgColor = UIColor(hexString: "#A9EFFF")
let rSecondCgColor = UIColor(hexString: "#CAE1F8")
let rThirdCgColor = UIColor(hexString: "#EDFFDF")

//App Button Hex UIColor
let firstCgColor = UIColor(hexString: firstColorHex)
let secondCgColor = UIColor(hexString: secondColorHex)
let thirdCgColor = UIColor(hexString: thirdColorHex)
let forthCgColor = UIColor(hexString: forthColorHex)
let skyCgColor = UIColor(hexString: skyColorHex)
let shadowGrayColor = UIColor(hexString: grayColorHex, alpha: 0.2)

struct Storyboards {
    internal static let homeStoryboard        =  UIStoryboard(name: "Main", bundle: nil)
    internal static let loginStoryboard       =  UIStoryboard(name: "Login", bundle: nil)
}

struct StoryboardIdentifiers {
    internal static let dashboardNavigation         =  "DashboardNavigation"
    internal static let loginNavigation             =  "LoginNavigation"
    internal static let completeProfileNavigation   =  "CompleteProfileNavigation"
}

struct LinkedinConstants {
    static let clientId = "81kaydvt4vo83n"
    static let clientSecret = "umNSLtHZIx2khhUd"
    static let redirectUri = "https://com.u3customer.linkedin.oauth/oauth"
    static let host = "com.u3customer.linkedin.oauth"
}

struct GoogleConstants {
    static let clientId = "299432893218-jc0d4gqhl24nfe6n8pcpqttc6tsfj63t.apps.googleusercontent.com"
    static let apiKey = "AIzaSyBf04QBHfnO18pcrPEGny8SjqxH5s2m1rs"
}

struct TwitterConstants {
    static let consumerKey = "c6PW2wpERjAGAReMFVdRjgKtC"
    static let consumerSecretKey = "fan32fs7n4cVdKWhojFPLda8sDYM8aIEVIzUeidaP9SiQnul9P"
}

//MARK:- API Methods.
enum APIMethods: String {
    case signup                               = "Account/SignUp"
    case customerLogin                        = "Account/CustomerLogin"
    case socialLogin                          = "Account/SocialLogin"
    case logout                               = "Account/UserLogout"
    case forgotPassword                       = "Account/ForgotPassword"
    case contactUsQuery                       = "Account/ContactUsQuery"
    case changePassword                       = "Account/ChangePassword"
    
    case getCustomerProfile                   = "Customer/GetCustomerProfile"
    case updateProfile                        = "Customer/UpdateProfile"
    case redeemFreebox                        = "Customer/RedeemFreebox"
    case masterCheckList                      = "Customer/GetMasterCheckList"
    case myCheckList                          = "Customer/GetMyChecklist"
    case changeChecklistItemStatus            = "Customer/ChangeChecklistItemStatus_V1"//"Customer/ChangeChecklistItemStatus"
    case changeToDoChecklistItemStatus        = "Customer/ChangeToDoChecklistItemStatus_V1"//"Customer/ChangeToDoChecklistItemStatus"
    case addMyCheckList                       = "Customer/AddMyCheckList_V1"//"Customer/AddMyCheckList"
    case searchNearbyDistributor              = "Customer/SearchNearbyDistributor"
    case scanAdvertisementQRCode              = "Customer/ScanAdvertisementQRCode"

    case searchServiceDirectory               = "ServiceDirectory/SearchServiceDirectory"
    case serviceDetails                       = "ServiceDirectory/ServiceDetails"
    case rateReviewDirectory                  = "ServiceDirectory/RateReviewDirectory"

    case businessCategoryList                 = "Common/BusinessCategoryList"
    case uploadImage                          = "Common/UploadImage"
    
    case sellItemCategory                     = "ItemForSell/SellItemCategory"
    case addSellItem                          = "ItemForSell/AddSellItem"
    case updateSellItem                       = "ItemForSell/UpdateSellItem"
    case getMySellItem                        = "ItemForSell/GetMySellItem"
    case mySellItemList                       = "ItemForSell/MySellItemList"
    case markSellItemOccupied                 = "ItemForSell/MarkSellItemOccupied"
    case deleteSellItem                       = "ItemForSell/DeleteSellItem"
    case saleItemDetail                       = "ItemForSell/ItemDetail"
    case browseSellItem                       = "ItemForSell/BrowseSellItem"
    
    case getPromotionalAdvertisementPopup     = "PromotionAdvertisement/GetPromotionalAdvertisementPopup"
    case getPromotionalAdvertisementStrip     = "PromotionAdvertisement/GetPromotionalAdvertisementStrip"
}

//MARK:-API Headers
enum apiHeaderKeys : String,CaseIterable {
    case contentTypeKey                       = "Content-Type"
    case apiServiceToken                      = "ApiServiceToken"
    case offsetKey                            = "Offset"
    case deviceType                           = "DeviceType"
    case authTokenKey                         = "AuthorizationToken"
    case useridKey                            = "UserId"

}

enum apiHeaderValues : String,CaseIterable {
    case contentTypeValue                     = "application/json"
    case apiServiceTokenValue                 = "U3System@2020"
    case offsetValue                          = "1"
}


enum Images {
    case downArrow
    case profilePlaceholder
    case rectPlaceholder
    case itemPlaceholder
    case backArrow
    case menu
    case location
    case edit
    case plus
    case demoAdvertise
    case search
    
    var icon: UIImage {
        switch self {
        case .downArrow: return UIImage(named: "down_arrowd_icon")?.withRenderingMode(.alwaysOriginal) ?? UIImage()
        case .profilePlaceholder: return UIImage(named: "user") ?? UIImage()
        case .rectPlaceholder: return UIImage(named: "user_rect") ?? UIImage()
        case .itemPlaceholder: return UIImage(named: "ItemPlaceholder") ?? UIImage()
        case .backArrow: return UIImage(named: "back_arrow")?.withRenderingMode(.alwaysOriginal) ?? UIImage()
        case .menu: return UIImage(named: "menuIC")?.withRenderingMode(.alwaysOriginal) ?? UIImage()
        case .location: return UIImage(named: "map_pin")?.withRenderingMode(.alwaysOriginal) ?? UIImage()
        case .edit: return UIImage(named: "edit_list")?.withRenderingMode(.alwaysOriginal) ?? UIImage()
        case .plus: return UIImage(named: "add_header_icon")?.withRenderingMode(.alwaysOriginal) ?? UIImage()
        case .demoAdvertise: return UIImage(named: "DemoAdvertise")?.withRenderingMode(.alwaysOriginal) ?? UIImage()
        case .search: return UIImage(named: "searchss")?.withRenderingMode(.alwaysOriginal) ?? UIImage()
        }
    }
}

enum gender : Int {
    case male = 1
    case female = 2
    case other = 3
    case unknown = 0
}

enum saleType : String, CaseIterable {
    
    case forSale = "For Sale"
    case forGiveAway = "For Give Away"
    case all = "All"
    
    func getTypeId()  -> Int {
        switch self {
        case saleType.forGiveAway:
            return 0
        case saleType.forSale:
            return 1
        default:
            return -1
        }
    }
}
