//
//  AppManager.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 26/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class AppManager: NSObject {

    
    // Can't init is singleton
    private override init() { }
    
    // MARK: Shared Instance
    
    static let shared: AppManager = {
        return AppManager()
    }()
    
    
    var isLoggedIn : Bool{
        set {
            UserDefaults.standard.set(newValue, forKey: "isLoggedIn")
            UserDefaults.standard.synchronize()
        }
        get{
            if let userloggedIn = UserDefaults.standard.object(forKey: "isLoggedIn")  as? Bool {
                return userloggedIn
            }
            else{
                return false
            }
        }
    }
    
    //MARK:-User variables
    var userModel : UserModel{
        set{
            let userDefaults = UserDefaults.standard
            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: newValue)
            userDefaults.set(encodedData, forKey: "UserModel")
            userDefaults.synchronize()
        }
        get{
            if let decoded  = UserDefaults.standard.object(forKey: "UserModel") as? Data{
                let decodedUser = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! UserModel
                return decodedUser
            }else{
                let dict = ["APIToken":"dsdasddsad", "IsProfileComplete":false,"UserId":0] as [String : Any]
                return UserModel.init(fromDictionary: dict)
            }
        }
    }
    
    var checkListPercentage : Int {
        set {
            let model = AppManager.shared.userModel
            model.checklistCompletionPercentage = newValue
            AppManager.shared.userModel = model
        }
        get {
            return 0
        }
    }
    
    var hasRedeemedFreeBox : Bool{
        set {
            UserDefaults.standard.set(newValue, forKey: "hasRedeemedFreeBox")
            UserDefaults.standard.synchronize()
        }
        get{
            if let rememberMe = UserDefaults.standard.object(forKey: "hasRedeemedFreeBox")  as? Bool {
                return rememberMe
            }
            else{
                return false
            }
        }
    }
    
    var isAdOpenedAlready : Bool{
        set {
            UserDefaults.standard.set(newValue, forKey: "isAdOpenedAlready")
            UserDefaults.standard.synchronize()
        }
        get{
            if let rememberMe = UserDefaults.standard.object(forKey: "isAdOpenedAlready")  as? Bool {
                return rememberMe
            }
            else{
                return false
            }
        }
    }
    
    var isRememberMe : Bool{
        set {
            UserDefaults.standard.set(newValue, forKey: "isRememberMe")
            UserDefaults.standard.synchronize()
        }
        get{
            if let rememberMe = UserDefaults.standard.object(forKey: "isRememberMe")  as? Bool {
                return rememberMe
            }
            else{
                return false
            }
        }
    }
    
    var isProfileCompleted : Bool{
        set {
            UserDefaults.standard.set(newValue, forKey: "isProfileCompleted")
            UserDefaults.standard.synchronize()
        }
        get{
            if let isProfileCompleted = UserDefaults.standard.object(forKey: "isProfileCompleted")  as? Bool {
                return isProfileCompleted
            }
            else{
                return false
            }
        }
    }
    
    
    var email : String{
        set {
            UserDefaults.standard.set(newValue, forKey: "email")
            UserDefaults.standard.synchronize()
        }
        get{
            if let code = UserDefaults.standard.value(forKey: "email") as? String {
                return code
            } else {
                return ""
            }
        }
    }
    
    var password : String{
        set {
            UserDefaults.standard.set(newValue, forKey: "password")
            UserDefaults.standard.synchronize()
        }
        get{
            if let code = UserDefaults.standard.value(forKey: "password") as? String {
                return code
            } else {
                return ""
            }
        }
    }
    


    //MARK:-PushNotification variables
    
    var deviceToken : String{
        set {
            UserDefaults.standard.set(newValue, forKey: "deviceToken")
            UserDefaults.standard.synchronize()
        }
        get{
            if let deviceToken = UserDefaults.standard.value(forKey: "deviceToken") as? String {
                return deviceToken
            } else {
                return "tokenNotFound"
            }
        }
    }
    
    open func showRootView() {
        if AppManager.shared.isLoggedIn{
            if AppManager.shared.isProfileCompleted {
                APPDELEGATE.showHomeWindow()
            }else {
                APPDELEGATE.showCompleteProfileWindow()
            }
        } else {
            APPDELEGATE.showLoginWindow()
        }
    }
  
    
    
    //MARK:-Utility Methods
    
    
    func getHeaders() -> [String:String] {
        var headers : [String:String] = [:]
        headers[apiHeaderKeys.contentTypeKey.rawValue] = apiHeaderValues.contentTypeValue.rawValue
        headers[apiHeaderKeys.apiServiceToken.rawValue] = apiHeaderValues.apiServiceTokenValue.rawValue
        headers[apiHeaderKeys.offsetKey.rawValue] = apiHeaderValues.offsetValue.rawValue
        headers[apiHeaderKeys.deviceType.rawValue] = apiHeaderValues.offsetValue.rawValue
       
        return headers
    }
    
    func getLoggedInHeaders() -> [String:String] {
        var headers : [String:String] = [:]
        headers[apiHeaderKeys.contentTypeKey.rawValue] = apiHeaderValues.contentTypeValue.rawValue
        headers[apiHeaderKeys.apiServiceToken.rawValue] = apiHeaderValues.apiServiceTokenValue.rawValue
        headers[apiHeaderKeys.offsetKey.rawValue] = apiHeaderValues.offsetValue.rawValue
        headers[apiHeaderKeys.authTokenKey.rawValue] = AppManager.shared.userModel.authToken
        headers[apiHeaderKeys.useridKey.rawValue] = "\(AppManager.shared.userModel.id!)"
        headers[apiHeaderKeys.deviceType.rawValue] = apiHeaderValues.offsetValue.rawValue
        return headers
    }
    
    
    
    
    func timeStampToString(timStamp : Int) -> String {
        let dateTimeStamp =  Date.init(timeIntervalSince1970: Double(timStamp))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "MMM dd yyyy"
        let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
        return strDateSelect
    }
   
    
    func openSettings(controller : UIViewController)  {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Unable to use notifications".localized,
                                          message: "To enable notifications, go to Settings and enable notifications for this app.".localized,
                                          preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "Ok".localized, style: .cancel, handler: nil)
            alert.addAction(okAction)
            
            let settingsAction = UIAlertAction(title: "Settings".localized, style: .default, handler: { _ in
                // Take the user to Settings app to possibly change permission.
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    })
                }
            })
            alert.addAction(settingsAction)
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Redirection on Complete Profile Screen
    func  redirectionOnCompleteProfile(viewController : UIViewController)  {
        let profileController = Storyboards.loginStoryboard.instantiateViewController(withIdentifier: "CompleteSignUpViewController") as! CompleteSignUpViewController
        viewController.navigationController?.pushViewController(profileController, animated: true)
    }
    
    
}
