//
//  ServiceDirectoryDetailModel.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 18/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation

class ServiceDirectoryDetailModel : NSObject, NSCoding{

    var address : String!
    var areaOfOperation : String!
    var businessName : String!
    var contactNumber : String!
    var currentRating : String!
    var directoryCategory : String!
    var directoryName : String!
    var distance : String!
    var latitude : Double!
    var longitude : Double!
    var pricing : String!
    var serviceDirectorId : Int!
    var serviceDirectoryId : Int!
    var serviceDirectoryLogo : String!
    var websiteAppLink : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        address = dictionary["address"] as? String
        areaOfOperation = dictionary["areaOfOperation"] as? String
        businessName = dictionary["businessName"] as? String
        contactNumber = dictionary["contactNumber"] as? String
        currentRating = dictionary["currentRating"] as? String
        directoryCategory = dictionary["directoryCategory"] as? String
        directoryName = dictionary["directoryName"] as? String
        distance = dictionary["distance"] as? String
        latitude = dictionary["latitude"] as? Double
        longitude = dictionary["longitude"] as? Double
        pricing = dictionary["pricing"] as? String
        serviceDirectorId = dictionary["serviceDirectorId"] as? Int
        serviceDirectoryId = dictionary["serviceDirectoryId"] as? Int
        serviceDirectoryLogo = dictionary["serviceDirectoryLogo"] as? String
        websiteAppLink = dictionary["websiteAppLink"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if areaOfOperation != nil{
            dictionary["areaOfOperation"] = areaOfOperation
        }
        if businessName != nil{
            dictionary["businessName"] = businessName
        }
        if contactNumber != nil{
            dictionary["contactNumber"] = contactNumber
        }
        if currentRating != nil{
            dictionary["currentRating"] = currentRating
        }
        if directoryCategory != nil{
            dictionary["directoryCategory"] = directoryCategory
        }
        if directoryName != nil{
            dictionary["directoryName"] = directoryName
        }
        if distance != nil{
            dictionary["distance"] = distance
        }
        if latitude != nil{
            dictionary["latitude"] = latitude
        }
        if longitude != nil{
            dictionary["longitude"] = longitude
        }
        if pricing != nil{
            dictionary["pricing"] = pricing
        }
        if serviceDirectorId != nil{
            dictionary["serviceDirectorId"] = serviceDirectorId
        }
        if serviceDirectoryId != nil{
            dictionary["serviceDirectoryId"] = serviceDirectoryId
        }
        if serviceDirectoryLogo != nil{
            dictionary["serviceDirectoryLogo"] = serviceDirectoryLogo
        }
        if websiteAppLink != nil{
            dictionary["websiteAppLink"] = websiteAppLink
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         address = aDecoder.decodeObject(forKey: "address") as? String
         areaOfOperation = aDecoder.decodeObject(forKey: "areaOfOperation") as? String
         businessName = aDecoder.decodeObject(forKey: "businessName") as? String
         contactNumber = aDecoder.decodeObject(forKey: "contactNumber") as? String
         currentRating = aDecoder.decodeObject(forKey: "currentRating") as? String
         directoryCategory = aDecoder.decodeObject(forKey: "directoryCategory") as? String
         directoryName = aDecoder.decodeObject(forKey: "directoryName") as? String
         distance = aDecoder.decodeObject(forKey: "distance") as? String
         latitude = aDecoder.decodeObject(forKey: "latitude") as? Double
         longitude = aDecoder.decodeObject(forKey: "longitude") as? Double
         pricing = aDecoder.decodeObject(forKey: "pricing") as? String
         serviceDirectorId = aDecoder.decodeObject(forKey: "serviceDirectorId") as? Int
         serviceDirectoryId = aDecoder.decodeObject(forKey: "serviceDirectoryId") as? Int
         serviceDirectoryLogo = aDecoder.decodeObject(forKey: "serviceDirectoryLogo") as? String
         websiteAppLink = aDecoder.decodeObject(forKey: "websiteAppLink") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if areaOfOperation != nil{
            aCoder.encode(areaOfOperation, forKey: "areaOfOperation")
        }
        if businessName != nil{
            aCoder.encode(businessName, forKey: "businessName")
        }
        if contactNumber != nil{
            aCoder.encode(contactNumber, forKey: "contactNumber")
        }
        if currentRating != nil{
            aCoder.encode(currentRating, forKey: "currentRating")
        }
        if directoryCategory != nil{
            aCoder.encode(directoryCategory, forKey: "directoryCategory")
        }
        if directoryName != nil{
            aCoder.encode(directoryName, forKey: "directoryName")
        }
        if distance != nil{
            aCoder.encode(distance, forKey: "distance")
        }
        if latitude != nil{
            aCoder.encode(latitude, forKey: "latitude")
        }
        if longitude != nil{
            aCoder.encode(longitude, forKey: "longitude")
        }
        if pricing != nil{
            aCoder.encode(pricing, forKey: "pricing")
        }
        if serviceDirectorId != nil{
            aCoder.encode(serviceDirectorId, forKey: "serviceDirectorId")
        }
        if serviceDirectoryId != nil{
            aCoder.encode(serviceDirectoryId, forKey: "serviceDirectoryId")
        }
        if serviceDirectoryLogo != nil{
            aCoder.encode(serviceDirectoryLogo, forKey: "serviceDirectoryLogo")
        }
        if websiteAppLink != nil{
            aCoder.encode(websiteAppLink, forKey: "websiteAppLink")
        }

    }

}
