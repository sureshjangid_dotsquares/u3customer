//
//  SaleItemModel.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 28/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation

class SaleItemModel : NSObject, NSCoding{

    var isForSell : Int!
    var itemCategoryName : String!
    var itemName : String!
    var itemStatus : Int!
    var listOfFiles : [ListOfFileModel]!
    var price : String!
    var sellItemId : Int!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        isForSell = dictionary["isForSell"] as? Int
        itemCategoryName = dictionary["itemCategoryName"] as? String
        itemName = dictionary["itemName"] as? String
        itemStatus = dictionary["itemStatus"] as? Int
        listOfFiles = [ListOfFileModel]()
        if let listOfFilesArray = dictionary["listOfFiles"] as? [[String:Any]]{
            for dic in listOfFilesArray{
                let value = ListOfFileModel(fromDictionary: dic)
                listOfFiles.append(value)
            }
        }
        price = dictionary["price"] as? String
        sellItemId = dictionary["sellItemId"] as? Int
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if isForSell != nil{
            dictionary["isForSell"] = isForSell
        }
        if itemCategoryName != nil{
            dictionary["itemCategoryName"] = itemCategoryName
        }
        if itemName != nil{
            dictionary["itemName"] = itemName
        }
        if itemStatus != nil{
            dictionary["itemStatus"] = itemStatus
        }
        if listOfFiles != nil{
            var dictionaryElements = [[String:Any]]()
            for listOfFilesElement in listOfFiles {
                dictionaryElements.append(listOfFilesElement.toDictionary())
            }
            dictionary["listOfFiles"] = dictionaryElements
        }
        if price != nil{
            dictionary["price"] = price
        }
        if sellItemId != nil{
            dictionary["sellItemId"] = sellItemId
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         isForSell = aDecoder.decodeObject(forKey: "isForSell") as? Int
         itemCategoryName = aDecoder.decodeObject(forKey: "itemCategoryName") as? String
         itemName = aDecoder.decodeObject(forKey: "itemName") as? String
         itemStatus = aDecoder.decodeObject(forKey: "itemStatus") as? Int
         listOfFiles = aDecoder.decodeObject(forKey :"listOfFiles") as? [ListOfFileModel]
         price = aDecoder.decodeObject(forKey: "price") as? String
         sellItemId = aDecoder.decodeObject(forKey: "sellItemId") as? Int

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if isForSell != nil{
            aCoder.encode(isForSell, forKey: "isForSell")
        }
        if itemCategoryName != nil{
            aCoder.encode(itemCategoryName, forKey: "itemCategoryName")
        }
        if itemName != nil{
            aCoder.encode(itemName, forKey: "itemName")
        }
        if itemStatus != nil{
            aCoder.encode(itemStatus, forKey: "itemStatus")
        }
        if listOfFiles != nil{
            aCoder.encode(listOfFiles, forKey: "listOfFiles")
        }
        if price != nil{
            aCoder.encode(price, forKey: "price")
        }
        if sellItemId != nil{
            aCoder.encode(sellItemId, forKey: "sellItemId")
        }

    }

}
