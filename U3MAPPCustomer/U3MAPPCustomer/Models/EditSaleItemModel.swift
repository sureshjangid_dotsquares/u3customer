//
//  EditSaleItemModel.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 31/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation

class EditSaleItemModel : NSObject, NSCoding{

    var descriptionField : String!
    var itemCategory : String!
    var itemCategoryID : Int!
    var itemName : String!
    var listOfFiles : [ListOfFileModel]!
    var locationAddress : String!
    var locationLatitude : Double!
    var locationLongitude : Double!
    var price : String!
    var sellItemId : Int!
    var sellOrGiveAway : Int!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        descriptionField = dictionary["description"] as? String
        itemCategory = dictionary["itemCategory"] as? String
        itemCategoryID = dictionary["itemCategoryID"] as? Int
        itemName = dictionary["itemName"] as? String
        listOfFiles = [ListOfFileModel]()
        if let listOfFilesArray = dictionary["listOfFiles"] as? [[String:Any]]{
            for dic in listOfFilesArray{
                let value = ListOfFileModel(fromDictionary: dic)
                listOfFiles.append(value)
            }
        }
        locationAddress = dictionary["locationAddress"] as? String
        locationLatitude = dictionary["locationLatitude"] as? Double
        locationLongitude = dictionary["locationLongitude"] as? Double
        price = dictionary["price"] as? String
        sellItemId = dictionary["sellItemId"] as? Int
        sellOrGiveAway = dictionary["sellOrGiveAway"] as? Int
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if itemCategory != nil{
            dictionary["itemCategory"] = itemCategory
        }
        if itemCategoryID != nil{
            dictionary["itemCategoryID"] = itemCategoryID
        }
        if itemName != nil{
            dictionary["itemName"] = itemName
        }
        if listOfFiles != nil{
            var dictionaryElements = [[String:Any]]()
            for listOfFilesElement in listOfFiles {
                dictionaryElements.append(listOfFilesElement.toDictionary())
            }
            dictionary["listOfFiles"] = dictionaryElements
        }
        if locationAddress != nil{
            dictionary["locationAddress"] = locationAddress
        }
        if locationLatitude != nil{
            dictionary["locationLatitude"] = locationLatitude
        }
        if locationLongitude != nil{
            dictionary["locationLongitude"] = locationLongitude
        }
        if price != nil{
            dictionary["price"] = price
        }
        if sellItemId != nil{
            dictionary["sellItemId"] = sellItemId
        }
        if sellOrGiveAway != nil{
            dictionary["sellOrGiveAway"] = sellOrGiveAway
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         itemCategory = aDecoder.decodeObject(forKey: "itemCategory") as? String
         itemCategoryID = aDecoder.decodeObject(forKey: "itemCategoryID") as? Int
         itemName = aDecoder.decodeObject(forKey: "itemName") as? String
         listOfFiles = aDecoder.decodeObject(forKey :"listOfFiles") as? [ListOfFileModel]
         locationAddress = aDecoder.decodeObject(forKey: "locationAddress") as? String
         locationLatitude = aDecoder.decodeObject(forKey: "locationLatitude") as? Double
         locationLongitude = aDecoder.decodeObject(forKey: "locationLongitude") as? Double
         price = aDecoder.decodeObject(forKey: "price") as? String
         sellItemId = aDecoder.decodeObject(forKey: "sellItemId") as? Int
         sellOrGiveAway = aDecoder.decodeObject(forKey: "sellOrGiveAway") as? Int

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if itemCategory != nil{
            aCoder.encode(itemCategory, forKey: "itemCategory")
        }
        if itemCategoryID != nil{
            aCoder.encode(itemCategoryID, forKey: "itemCategoryID")
        }
        if itemName != nil{
            aCoder.encode(itemName, forKey: "itemName")
        }
        if listOfFiles != nil{
            aCoder.encode(listOfFiles, forKey: "listOfFiles")
        }
        if locationAddress != nil{
            aCoder.encode(locationAddress, forKey: "locationAddress")
        }
        if locationLatitude != nil{
            aCoder.encode(locationLatitude, forKey: "locationLatitude")
        }
        if locationLongitude != nil{
            aCoder.encode(locationLongitude, forKey: "locationLongitude")
        }
        if price != nil{
            aCoder.encode(price, forKey: "price")
        }
        if sellItemId != nil{
            aCoder.encode(sellItemId, forKey: "sellItemId")
        }
        if sellOrGiveAway != nil{
            aCoder.encode(sellOrGiveAway, forKey: "sellOrGiveAway")
        }

    }

}
