//
//  ListOfFileModel.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 28/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation

class ListOfFileModel : NSObject, NSCoding{

    var fileName : String!
    var fileURL : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        fileName = dictionary["fileName"] as? String
        fileURL = dictionary["fileURL"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if fileName != nil{
            dictionary["fileName"] = fileName
        }
        if fileURL != nil{
            dictionary["fileURL"] = fileURL
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         fileName = aDecoder.decodeObject(forKey: "fileName") as? String
         fileURL = aDecoder.decodeObject(forKey: "fileURL") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if fileName != nil{
            aCoder.encode(fileName, forKey: "fileName")
        }
        if fileURL != nil{
            aCoder.encode(fileURL, forKey: "fileURL")
        }

    }

}
