//
//  ServiceDirectoryModel.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 18/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation

class ServiceDirectoryModel : NSObject, NSCoding {

    var currentRating : String = ""
    var directoryName : String!
    var distance : String!
    var serviceDirectorId : Int!
    var serviceDirectoryId : Int!
    var serviceDirectoryLogo : String!
    var latitude : Double!
    var longitude : Double!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        currentRating = dictionary["currentRating"] as? String ?? ""
        directoryName = dictionary["directoryName"] as? String
        distance = dictionary["distance"] as? String
        serviceDirectorId = dictionary["serviceDirectorId"] as? Int
        serviceDirectoryId = dictionary["serviceDirectoryId"] as? Int
        serviceDirectoryLogo = dictionary["serviceDirectoryLogo"] as? String
        latitude = dictionary["latitude"] as? Double
        longitude = dictionary["longitude"] as? Double
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if currentRating != nil{
            dictionary["currentRating"] = currentRating
        }
        if directoryName != nil{
            dictionary["directoryName"] = directoryName
        }
        if distance != nil{
            dictionary["distance"] = distance
        }
        if serviceDirectorId != nil{
            dictionary["serviceDirectorId"] = serviceDirectorId
        }
        if serviceDirectoryId != nil{
            dictionary["serviceDirectoryId"] = serviceDirectoryId
        }
        if serviceDirectoryLogo != nil{
            dictionary["serviceDirectoryLogo"] = serviceDirectoryLogo
        }
        if latitude != nil{
            dictionary["latitude"] = latitude
        }
        if longitude != nil{
            dictionary["longitude"] = longitude
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        currentRating = aDecoder.decodeObject(forKey: "currentRating") as? String ?? ""
         directoryName = aDecoder.decodeObject(forKey: "directoryName") as? String
         distance = aDecoder.decodeObject(forKey: "distance") as? String
         serviceDirectorId = aDecoder.decodeObject(forKey: "serviceDirectorId") as? Int
         serviceDirectoryId = aDecoder.decodeObject(forKey: "serviceDirectoryId") as? Int
         serviceDirectoryLogo = aDecoder.decodeObject(forKey: "serviceDirectoryLogo") as? String
        latitude = aDecoder.decodeObject(forKey: "latitude") as? Double
        longitude = aDecoder.decodeObject(forKey: "longitude") as? Double

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if currentRating != nil{
            aCoder.encode(currentRating, forKey: "currentRating")
        }
        if directoryName != nil{
            aCoder.encode(directoryName, forKey: "directoryName")
        }
        if distance != nil{
            aCoder.encode(distance, forKey: "distance")
        }
        if serviceDirectorId != nil{
            aCoder.encode(serviceDirectorId, forKey: "serviceDirectorId")
        }
        if serviceDirectoryId != nil{
            aCoder.encode(serviceDirectoryId, forKey: "serviceDirectoryId")
        }
        if serviceDirectoryLogo != nil{
            aCoder.encode(serviceDirectoryLogo, forKey: "serviceDirectoryLogo")
        }
        if latitude != nil{
            aCoder.encode(latitude, forKey: "latitude")
        }
        if longitude != nil{
            aCoder.encode(longitude, forKey: "longitude")
        }

    }

}
