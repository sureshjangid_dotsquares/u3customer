//
//  RootModel.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 26/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation

import UIKit

class RootModel: NSObject , NSCoding{
    
    var data : AnyObject!
    var apiName : String!
    var failureMsg : String!
    var responseCode : Int!
    var successMsg : String!
    var validationErrors : AnyObject!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        data = dictionary["data"] as AnyObject?
        apiName = dictionary["apiName"] as? String
        failureMsg = dictionary["failureMsg"] as? String
        responseCode = dictionary["responseCode"] as? Int
        successMsg = dictionary["successMsg"] as? String
        validationErrors = dictionary["validationErrors"] as AnyObject?

    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if data != nil{
            dictionary["data"] = data
        }
        if apiName != nil{
            dictionary["apiName"] = apiName
        }
        if failureMsg != nil{
            dictionary["failureMsg"] = failureMsg
        }
        if responseCode != nil{
            dictionary["responseCode"] = responseCode
        }
        if successMsg != nil{
            dictionary["successMsg"] = successMsg
        }
        if validationErrors != nil{
            dictionary["validationErrors"] = validationErrors
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        data = aDecoder.decodeObject(forKey: "data") as AnyObject?
        apiName = aDecoder.decodeObject(forKey: "apiName") as? String
        failureMsg = aDecoder.decodeObject(forKey: "failureMsg") as? String
        responseCode = aDecoder.decodeObject(forKey: "responseCode") as? Int
        successMsg = aDecoder.decodeObject(forKey: "successMsg") as? String
        validationErrors = aDecoder.decodeObject(forKey: "validationErrors") as AnyObject?

    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if data != nil{
            aCoder.encode(data, forKey: "data")
        }
        if apiName != nil{
            aCoder.encode(apiName, forKey: "apiName")
        }
        if failureMsg != nil{
            aCoder.encode(failureMsg, forKey: "failureMsg")
        }
        if responseCode != nil{
            aCoder.encode(responseCode, forKey: "responseCode")
        }
        if successMsg != nil{
            aCoder.encode(successMsg, forKey: "successMsg")
        }
        if validationErrors != nil{
            aCoder.encode(validationErrors, forKey: "validationErrors")
        }
    }
}

