//
//  BusinessCategoryModel.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 19/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation

class BusinessCategoryModel : NSObject{

    var categoryName : String!
    var id : Int!
    var category : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        categoryName = dictionary["categoryName"] as? String
        id = dictionary["id"] as? Int
        category = dictionary["category"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if categoryName != nil{
            dictionary["categoryName"] = categoryName
        }
        if id != nil{
            dictionary["id"] = id
        }
        if category != nil{
            dictionary["category"] = category
        }
        return dictionary
    }

}
