//
//  ThreeUAgentModel.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 03/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation

class ThreeUAgentModel : NSObject, NSCoding{

    var contact : String!
    var distance : String!
    var id : Int!
    var lat : Double!
    var lng : Double!
    var name : String!
    var profilePic : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        contact = dictionary["contact"] as? String
        distance = dictionary["distance"] as? String
        id = dictionary["id"] as? Int
        lat = dictionary["lat"] as? Double
        lng = dictionary["lng"] as? Double
        name = dictionary["name"] as? String
        profilePic = dictionary["profilePic"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if contact != nil{
            dictionary["contact"] = contact
        }
        if distance != nil{
            dictionary["distance"] = distance
        }
        if id != nil{
            dictionary["id"] = id
        }
        if lat != nil{
            dictionary["lat"] = lat
        }
        if lng != nil{
            dictionary["lng"] = lng
        }
        if name != nil{
            dictionary["name"] = name
        }
        if profilePic != nil{
            dictionary["profilePic"] = profilePic
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         contact = aDecoder.decodeObject(forKey: "contact") as? String
         distance = aDecoder.decodeObject(forKey: "distance") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         lat = aDecoder.decodeObject(forKey: "lat") as? Double
         lng = aDecoder.decodeObject(forKey: "lng") as? Double
         name = aDecoder.decodeObject(forKey: "name") as? String
         profilePic = aDecoder.decodeObject(forKey: "profilePic") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if contact != nil{
            aCoder.encode(contact, forKey: "contact")
        }
        if distance != nil{
            aCoder.encode(distance, forKey: "distance")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if lat != nil{
            aCoder.encode(lat, forKey: "lat")
        }
        if lng != nil{
            aCoder.encode(lng, forKey: "lng")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if profilePic != nil{
            aCoder.encode(profilePic, forKey: "profilePic")
        }

    }

}
