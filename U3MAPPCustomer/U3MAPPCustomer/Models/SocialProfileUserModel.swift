//
//  SocialProfileUserModel.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 14/04/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation

class SocialProfileUserModel : NSObject, NSCoding{
    
    var bio : String!
    var fullName : String!
    var id : String!
    var isBusiness : Bool!
    var profilePicture : String!
    var username : String!
    var website : String!
    let userId : String!             // For client-side use only!
    let firstName : String!
    let lastName : String!
    let email : String!
    let refreshToken : String!
    var emailVerified : Bool!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    override init() {
        bio = ""
        fullName = ""
        id = ""
        isBusiness = false
        profilePicture = ""
        username = ""
        website = ""
        userId = ""
        firstName = ""
        lastName = ""
        email = ""
        refreshToken = ""
        emailVerified = false
        
    }
    init(fromDictionary dictionary: [String:Any]){
        bio = dictionary["bio"] as? String ?? ""
        fullName = dictionary["full_name"] as? String ?? ""
        let arrNames = fullName.components(separatedBy: " ") as Array
        
        let first = arrNames[0]
        
        var last = ""
        
        if arrNames.count > 1 {
            last =  arrNames[1]
        }
        
        id = dictionary["id"] as? String ?? ""
        isBusiness = dictionary["is_business"] as? Bool ?? false
        profilePicture = dictionary["profile_picture"] as? String ?? ""
        username = dictionary["username"] as? String ?? ""
        website = dictionary["website"] as? String ?? ""
        userId = dictionary["user_id"] as? String ?? ""
        firstName = dictionary["first_name"] as? String ?? first
        lastName = dictionary["last_name"] as? String ?? last
        if fullName == ""{
            fullName = firstName + " " + lastName
        }
        email = dictionary["email"] as? String ?? ""
        refreshToken = dictionary["refresh_token"] as? String ?? ""
        emailVerified = dictionary["email_verified"] as? Bool ?? false
        
        if email != ""{
            emailVerified = true
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if bio != nil{
            dictionary["bio"] = bio
        }
        if fullName != nil{
            dictionary["full_name"] = fullName
        }
        if id != nil{
            dictionary["id"] = id
        }
        if isBusiness != nil{
            dictionary["is_business"] = isBusiness
        }
        if profilePicture != nil{
            dictionary["profile_picture"] = profilePicture
        }
        if username != nil{
            dictionary["username"] = username
        }
        if website != nil{
            dictionary["website"] = website
        }
        
        if userId != nil{
            dictionary["user_id"] = userId
        }
        if firstName != nil{
            dictionary["first_name"] = firstName
        }
        if lastName != nil{
            dictionary["last_name"] = lastName
        }
        if email != nil{
            dictionary["email"] = email
        }
        if refreshToken != nil{
            dictionary["refresh_token"] = refreshToken
        }
        if emailVerified != nil{
            dictionary["email_verified"] = emailVerified
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        bio = aDecoder.decodeObject(forKey: "bio") as? String
        fullName = aDecoder.decodeObject(forKey: "full_name") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        isBusiness = aDecoder.decodeObject(forKey: "is_business") as? Bool
        profilePicture = aDecoder.decodeObject(forKey: "profile_picture") as? String
        username = aDecoder.decodeObject(forKey: "username") as? String
        website = aDecoder.decodeObject(forKey: "website") as? String
        
        userId = aDecoder.decodeObject(forKey: "user_id") as? String
        firstName = aDecoder.decodeObject(forKey: "first_name") as? String
        lastName = aDecoder.decodeObject(forKey: "last_name") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        refreshToken = aDecoder.decodeObject(forKey: "refresh_token") as? String
        emailVerified = aDecoder.decodeObject(forKey: "email_verified") as? Bool
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if bio != nil{
            aCoder.encode(bio, forKey: "bio")
        }
        if fullName != nil{
            aCoder.encode(fullName, forKey: "full_name")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if isBusiness != nil{
            aCoder.encode(isBusiness, forKey: "is_business")
        }
        if profilePicture != nil{
            aCoder.encode(profilePicture, forKey: "profile_picture")
        }
        if username != nil{
            aCoder.encode(username, forKey: "username")
        }
        if website != nil{
            aCoder.encode(website, forKey: "website")
        }
        
        if userId != nil{
            aCoder.encode(website, forKey: "user_id")
        }
        if firstName != nil{
            aCoder.encode(website, forKey: "first_name")
        }
        if lastName != nil{
            aCoder.encode(website, forKey: "last_name")
        }
        if email != nil{
            aCoder.encode(website, forKey: "email")
        }
        if refreshToken != nil{
            aCoder.encode(website, forKey: "refresh_token")
        }
        if emailVerified != nil{
            aCoder.encode(emailVerified, forKey: "email_verified")
        }
        
    }
    
}
