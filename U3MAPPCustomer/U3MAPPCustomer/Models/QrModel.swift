//
//  QrModel.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 28/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation

class QrModel : NSObject, NSCoding{

    var alphanumericCode : String!
    var qrBase64Code : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        alphanumericCode = dictionary["alphanumericCode"] as? String
        qrBase64Code = dictionary["qrBase64Code"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if alphanumericCode != nil{
            dictionary["alphanumericCode"] = alphanumericCode
        }
        if qrBase64Code != nil{
            dictionary["qrBase64Code"] = qrBase64Code
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         alphanumericCode = aDecoder.decodeObject(forKey: "alphanumericCode") as? String
         qrBase64Code = aDecoder.decodeObject(forKey: "qrBase64Code") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if alphanumericCode != nil{
            aCoder.encode(alphanumericCode, forKey: "alphanumericCode")
        }
        if qrBase64Code != nil{
            aCoder.encode(qrBase64Code, forKey: "qrBase64Code")
        }

    }

}
