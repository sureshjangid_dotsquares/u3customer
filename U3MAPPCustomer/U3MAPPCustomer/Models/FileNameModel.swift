//
//  FileNameModel.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 25/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation

class FileNameModel : NSObject {

    var fileName : String!
    var fileURL : String!


    override init() {
        fileName = ""
        fileURL = ""
    }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        fileName = dictionary["fileName"] as? String
        fileURL = dictionary["fileURL"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if fileName != nil{
            dictionary["fileName"] = fileName
        }
        if fileURL != nil{
            dictionary["fileURL"] = fileURL
        }
        return dictionary
    }

}


class FileNameRequestModel : NSObject {

    var fileName : String!


    init(fileName: String) {
        self.fileName = fileName
    }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        fileName = dictionary["fileName"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if fileName != nil{
            dictionary["fileName"] = fileName
        }
        return dictionary
    }

}
