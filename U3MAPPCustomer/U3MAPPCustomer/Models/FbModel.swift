//
//  FbModel.swift
//
//  Created by Abhishek Jangid on 06/09/19.
//  Copyright © 2019 Abhishek Jangid. All rights reserved.
//

import UIKit

//MARK:- Social Model
class FbModel : NSObject, NSCoding {
    
    var email : String = ""
    var title : String = ""
    var message : String = ""
    var profileImage : String = ""
    var fbToken: String = ""
    var fbID: String = ""
    
    
    
    /**
     * Instantiate the instance using the passed parameter values to set the properties values
     */
    
    init(title:String,message:String,fbToken: String,fbID:String,email:String) {
        
        self.email = email
        self.title = title
        self.message = message
        self.profileImage = message
        self.fbToken = fbToken
        self.fbID = fbID
    }
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
        email = dictionary["email"] as? String ?? ""
        title = dictionary["full_name"] as? String ?? ""
        message = dictionary["message"] as? String ?? ""
        profileImage = dictionary["profile_image"] as? String ?? ""
        fbToken = dictionary["facebook_token"] as? String ?? ""
        fbID = dictionary["facebook_id"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        dictionary["email"] = email
        dictionary["full_name"] = title
        dictionary["message"] = message
        dictionary["profile_image"] = profileImage
        dictionary["facebook_token"] = fbToken
        dictionary["facebook_id"] = fbID
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        email = aDecoder.decodeObject(forKey: "email") as? String ?? ""
        title = aDecoder.decodeObject(forKey: "full_name") as? String ?? ""
        message = aDecoder.decodeObject(forKey: "message") as? String ?? ""
        profileImage = aDecoder.decodeObject(forKey: "profile_image") as? String ?? ""
        fbToken = aDecoder.decodeObject(forKey: "facebook_token") as? String ?? ""
        fbID = aDecoder.decodeObject(forKey: "facebook_id") as? String ?? ""
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        aCoder.encode(email, forKey: "email")
        aCoder.encode(title, forKey: "full_name")
        aCoder.encode(message, forKey: "message")
        aCoder.encode(profileImage, forKey: "profile_image")
        aCoder.encode(fbToken, forKey: "facebook_token")
        aCoder.encode(fbToken, forKey: "facebook_id")
    }
}
