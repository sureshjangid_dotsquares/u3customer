//
//  AdvertisementPopupModel.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 23/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation

class AdvertisementPopupModel : NSObject, NSCoding{

    var advertLink : String!
    var advertisementId : Int!
    var advertisementURL : String!

    override init(){
        
    }

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        advertLink = dictionary["advertLink"] as? String
        advertisementId = dictionary["advertisementId"] as? Int
        advertisementURL = dictionary["advertisementURL"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if advertLink != nil{
            dictionary["advertLink"] = advertLink
        }
        if advertisementId != nil{
            dictionary["advertisementId"] = advertisementId
        }
        if advertisementURL != nil{
            dictionary["advertisementURL"] = advertisementURL
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         advertLink = aDecoder.decodeObject(forKey: "advertLink") as? String
         advertisementId = aDecoder.decodeObject(forKey: "advertisementId") as? Int
         advertisementURL = aDecoder.decodeObject(forKey: "advertisementURL") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if advertLink != nil{
            aCoder.encode(advertLink, forKey: "advertLink")
        }
        if advertisementId != nil{
            aCoder.encode(advertisementId, forKey: "advertisementId")
        }
        if advertisementURL != nil{
            aCoder.encode(advertisementURL, forKey: "advertisementURL")
        }

    }

}
