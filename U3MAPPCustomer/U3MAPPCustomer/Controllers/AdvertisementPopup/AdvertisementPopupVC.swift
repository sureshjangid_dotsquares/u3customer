//
//  AdvertisementPopupVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 20/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import SafariServices

class AdvertisementPopupVC: UIViewController, SFSafariViewControllerDelegate {

    @IBOutlet var viewMain : UIView!
    @IBOutlet var imgAd : UIImageView!
    
    var objAdvertise: AdvertisementPopupModel!
    var callback: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewMain.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        viewMain.layer.shadowOffset = CGSize.zero
        viewMain.layer.shadowOpacity = 1.0
        viewMain.layer.shadowRadius = 5
        viewMain.layer.cornerRadius = 20
        viewMain.backgroundColor = .white
        viewMain.layer.masksToBounds = true
        
        //"https://i.picsum.photos/id/866/200/300.jpg"
        let url = URL(string: objAdvertise.advertisementURL)!
        
        imgAd.kf.indicatorType = .activity
        imgAd.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholderImage"),
            options: [
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
                self.presentAlertWith(message: "Error on loading data".localized) {
                    self.dismissView()
                }
            }
        }
    }
    
    func dismissView() {
        self.dismiss(animated: true) {
            self.callback?()
        }
    }
    

   @IBAction func didTapOnView(_ sender: Any) {
        dismissView()
    }
    
    @IBAction func didTapOnCrossbtn(_ sender: UIButton) {
        dismissView()
    }
    
    @IBAction func didTapOnFrontView(_ sender: Any) {
        guard objAdvertise.advertLink != nil else {self.presentAlertWith(message: "Currently we have no data on this.".localized);return}
        
        let strUrl : String = objAdvertise.advertLink.validURL()
        let url = URL(string: strUrl)!
        let controller = SFSafariViewController(url: url)
        self.present(controller, animated: true, completion: nil)
        controller.delegate = self
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
