//
//  LocationMapVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 03/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import GooglePlacePicker
import GrowingTextView

class LocationMapVC: U3BaseViewController,CLLocationManagerDelegate {
    
    @IBOutlet weak var viewMAP: UIView!
    @IBOutlet weak var txtLocation: GrowingTextView!
    
    var selectedPlace: GMSPlace!
    
    var placesClient: GMSPlacesClient!
    var nameLabel: UILabel!
    var addressLabel: UILabel!
    
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var mapView: GMSMapView!
    var zoomLevel: Float = 15.0
    
    var marker:GMSMarker!
    var centerMapCoordinate:CLLocationCoordinate2D!
    
    var lat = "",lang = ""
    var callback : ((_ lat : String, _ lang : String, _ address: String )->Void)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.backBarBtnPressed()
        //Location Manager code to fetch current location
        self.locationManager = CLLocationManager()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.distanceFilter = 50
        self.locationManager.startUpdatingLocation()
        self.locationManager.delegate = self
        
        updateUI()
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
            self.initilizeGoogleMap()
            
        }
        
    }
    
    fileprivate func updateUI() {
        self.title = "Select Location".localized
        if let nav = self.navigationController as? U3NavigationController {
            nav.hideNavBottomLine()
        }
        showHud()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
    }
    
    func initilizeGoogleMap() {
        
        guard let latt = PCLocationManager.shared()?.myLocation.latitude, let longg = PCLocationManager.shared()?.myLocation.longitude else {
            return
        }
        
        ////********************************* GOOGLE MAP VIEW WORK ************************/////////
        
        //==========Initilize MAPVIEW==============
        let camera = GMSCameraPosition.camera(withLatitude: latt,
                                              longitude: longg,
                                              zoom: zoomLevel)
        self.mapView = GMSMapView.map(withFrame: viewMAP.bounds, camera: camera)
        self.mapView.settings.myLocationButton = true
        self.mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.mapView.isMyLocationEnabled = true
        self.mapView.isBuildingsEnabled = false
        self.mapView.setMinZoom(0, maxZoom: 20.0)
        self.mapView.delegate = self
        
        
        
        // Add the map to the view, hide it until we've got a location update.
        self.viewMAP.addSubview(self.mapView)
        self.mapView.isHidden = false
        self.mapView.camera = camera
        self.mapView.animate(to: camera)
        
        self.view.layoutIfNeeded()
        
        
    }
    
    @IBAction func didTapChangeLocation(_ sender: UIButton) {
        self.pickPlace()
    }
    
    @IBAction func didTapDone(_ sender: UIButton) {
        callback(self.lat,self.lang,self.txtLocation.text!.trimString())
        self.navigationController?.popViewController(animated: true)
    }
        
}

extension LocationMapVC: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        print("willMove")
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        dissmissHud()
        print("idleAt")
        let latitude = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
        //        print("\(latitude)\(longitude)")
        //        centerMapCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        //        self.placeMarkerOnCenter(centerMapCoordinate:centerMapCoordinate)
        
        self.getAddressFromLatLon(lat: latitude, withLongitude: longitude)
    }
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        print("didDrag")
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        print("didEndDragging")
    }
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        print("didBeginDragging")
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
    }
    
}


extension LocationMapVC: GMSPlacePickerViewControllerDelegate {
    
    func getAddressFromLatLon(lat: CLLocationDegrees, withLongitude lon: CLLocationDegrees) {
        
        let ceo: CLGeocoder = CLGeocoder()
        let loc: CLLocation = CLLocation(latitude:lat, longitude: lon)
        self.lat = String(format:"%f",loc.coordinate.latitude)
        self.lang = String(format:"%f",loc.coordinate.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    /*print(pm.country)
                     print(pm.locality)
                     print(pm.subLocality)
                     print(pm.thoroughfare)
                     print(pm.postalCode)
                     print(pm.subThoroughfare)*/
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    
                    print(addressString)
                    
                    self.txtLocation.text = addressString
                }
        })
        
    }
    
    func pickPlace() {
            
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            // Specify the place data types to return.
            let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
                UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.formattedAddress.rawValue) | UInt(GMSPlaceField.coordinate.rawValue))!
            autocompleteController.placeFields = fields
            // Display the autocomplete view controller.
            present(autocompleteController, animated: true, completion: nil)
            
        }
       
        // To receive the results from the place picker 'self' will need to conform to
        // GMSPlacePickerViewControllerDelegate and implement this code.
        func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
            // Dismiss the place picker, as it cannot dismiss itself.
            viewController.dismiss(animated: true, completion: nil)
            
            print("Place name \(place.name)")
            print("Place address \(String(describing: place.formattedAddress))")
            print("Place attributions \(String(describing: place.attributions))")
            
            
            
            if place.formattedAddress == nil{
                Utility.ValidationError(errorMessage: "Please select correct location".localized, controller: self)
            }else{
                self.selectedPlace = place
                 self.txtLocation.placeholder = ""
            }
            
        }
        
        func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
            // Dismiss the place picker, as it cannot dismiss itself.
            viewController.dismiss(animated: true, completion: nil)
            
            print("No place selected")
        }
}

//MARK:- Google Place Sdk Delegates

extension LocationMapVC: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.selectedPlace = place
        self.txtLocation.placeholder = ""
        self.txtLocation.text = (place.name!) + ", " + ( place.formattedAddress?.components(separatedBy: ", ")
            .joined(separator: ", "))!
        dismiss(animated: true) {
            self.mapView.animate(to: GMSCameraPosition(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: self.zoomLevel))
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}


