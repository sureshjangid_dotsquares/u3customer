//
//  ContactUsVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 10/04/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class ContactUsVC: U3BaseViewController {

    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtEmailAddress: UITextField!
    @IBOutlet var txtContact: UITextField!
    @IBOutlet var txtQuery: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let nav = self.navigationController as? U3NavigationController {
            nav.hideNavBottomLine()
        }
        setupMenuBtn()
        self.title = "Contact Us".localized
    }
    
    @IBAction func didTapOnAdd(_ sender: Any) {
        if self.ValidateEntries() == false {
            return
        }
        
        self.wsContactUsQuery()
    }

}

extension ContactUsVC {
    
    //MARK: - Validation
    func ValidateEntries() -> Bool {
        if txtName.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter name".localized)
            return false
        }
        else if txtEmailAddress.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter email".localized)
            return false
        }
        else if txtEmailAddress.text?.trimString().isValidEmail() == false
        {
            Utility.ValidationError(errorMessage: "Please enter valid email".localized, controller: self)
            return false
        }
        else if txtContact.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter Phone number".localized)
            return false
        }
        else if txtContact.text!.trimString().count < 7 {
            self.presentAlertWith(message: "Phone number should be between 7 to 15 character".localized)
            return false
        }
        else if txtContact.text!.trimString().count > 15 {
            self.presentAlertWith(message: "Phone number should be between 7 to 15 character".localized)
            return false
        }
        else if txtContact.text?.trimString().isValidPhoneNumber() == false {
            self.presentAlertWith(message: "Please enter valid Phone number".localized)
            return false
        }
        else if txtQuery.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter your query here".localized)
            return false
        }
        
        return true
    }
}

//MARK:-API Calling
extension ContactUsVC{
    
    //MARK: Get Complete Profile
    func wsContactUsQuery(){
        let params : [String:Any] = [
            "Name": txtName.text?.trimString() as Any,
            "Contact": txtContact.text?.trimString() as Any,
            "Email": txtEmailAddress.text?.trimString() as Any,
            "Query": txtQuery.text?.trimString() as Any]
        
        showHud()
        WebServiceHelper.request(path: .contactUsQuery, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if (rootModel.data as? Bool) != nil{
                                self.presentAlertWith(message: rootModel.successMsg) {
                                    AppManager.shared.showRootView()
                                }
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
}
