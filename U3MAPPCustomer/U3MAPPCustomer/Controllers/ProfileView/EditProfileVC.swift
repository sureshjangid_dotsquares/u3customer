//
//  EditProfileVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 03/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import McPicker
import Kingfisher
import CropViewController

class EditProfileVC: U3BaseViewController {

    //MARK:-IBOutlets
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var signUpBtn:UIButton!
    @IBOutlet weak var emailTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var contactTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var addressTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var nameTxtFld:CustomTextField!
    @IBOutlet weak var ageTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var genderTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var buttonUserImage: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    
    lazy var strBase64Image : String = {
        let str = Utility.getStingFromImg(image :userImageView.image!)
        return str
    }()
    
    //MARK:-Variables
    var objProfile : ProfileModel! {
        didSet {
            emailTxtFld.text = objProfile.email ?? ""
            contactTxtFld.text = objProfile.contact ?? ""
            addressTxtFld.text = objProfile?.address ?? ""
            nameTxtFld.text = objProfile.customerName ?? ""
            if objProfile.age == 0 {
                ageTxtFld.text = ""
            }else {
                ageTxtFld.text = String(format:"%d",objProfile.age)
            }
            
            switch objProfile.gender {
            case gender.male.rawValue:
                genderTxtFld.text = "Male".localized
            case gender.female.rawValue:
                genderTxtFld.text = "Female".localized
            case gender.other.rawValue:
                genderTxtFld.text = "Other".localized
            default:
                genderTxtFld.text = ""
            }
            
            let url = URL(string: objProfile.logoProfilePic)
            userImageView.kf.setImage(with: url, placeholder: Images.profilePlaceholder.icon)
            self.userImageView.layer.cornerRadius =  self.userImageView.bounds.width/2
            
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        super.backBarBtnPressed()
        self.initSetup()
        self.wsGetCompleteProfileData()
        
        if let nav = self.navigationController as? U3NavigationController {
            nav.clearNavBarColor()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.view.setGradient(colors: [rFirstCgColor.cgColor,rSecondCgColor.cgColor,rThirdCgColor.cgColor], angle: 315)
        viewBg.setGradient(colors: [rFirstCgColor.cgColor,rSecondCgColor.cgColor,rThirdCgColor.cgColor], angle: 315)
        self.viewBg.roundCorners([.layerMinXMaxYCorner], radius: 50)
    }
    
    
    func initSetup(){
        
        self.title = "Profile".localized
        if let nav = self.navigationController as? U3NavigationController {
            nav.clearNavBarColor()
        }
        ageTxtFld.delegate = self
        genderTxtFld.delegate = self
        
        nameTxtFld.setRightViewImage(Images.edit.icon)
        ageTxtFld.setRightViewImage(Images.downArrow.icon)
        genderTxtFld.setRightViewImage(Images.downArrow.icon)
        
    }
    
    //MARK:-IBAction Methods
    @IBAction func didTapBackBtn(_ sender : UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func actionUserImagePick(_ sender: UIButton) {
        MediaUtilityClass.sharedInstanse().pickImage { (image, url, error) in
            if image != nil{
                
                let cropController = CropViewController(croppingStyle: .default, image: image!)
                cropController.delegate = self
                cropController.aspectRatioPreset = .presetSquare
                cropController.aspectRatioLockEnabled = true
                cropController.resetAspectRatioEnabled = false
                cropController.aspectRatioPickerButtonHidden = true
                cropController.rotateButtonsHidden = true
                cropController.rotateClockwiseButtonHidden = true
                cropController.doneButtonTitle = "Crop".localized
                cropController.cancelButtonTitle = "Cancel".localized
                self.present(cropController, animated: true, completion: nil)
                
            }
        }
    }
    
    @IBAction func didTapSignUpBtn(_ sender : UIButton){
        
        if ValidateEntries() == false {
            return
        }
        
        self.wsUpdateProfileData()
    }
    
    
    func setUserImage(image:UIImage?){
        self.userImageView.image = image
        self.userImageView.layer.cornerRadius =  self.userImageView.bounds.width/2
    }
    
}

extension EditProfileVC : CropViewControllerDelegate {
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        self.setUserImage(image: image)
        cropViewController.dismiss(animated: true, completion: nil)
        
    }
}


extension EditProfileVC:UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == ageTxtFld {
            self.view.endEditing(true)
            var ageArray = [Int]()
            ageArray += 18...100
            McPicker.show(data: [ageArray.map { String($0) }]) {(selections: [Int : String]) -> Void in
                textField.text = selections[0]
            }
            return false
        }else if textField == genderTxtFld {
            self.view.endEditing(true)
            McPicker.show(data: [["Male".localized, "Female".localized,"Other".localized]]) {(selections: [Int : String]) -> Void in
                textField.text = selections[0]
            }
            return false
        }
        return true
    }

}

extension EditProfileVC {
    
    //MARK: - Validation
    func ValidateEntries() -> Bool {
                
        if nameTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter name".localized)
            return false
        }
        else if emailTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter email".localized)
            return false
        }
        else if emailTxtFld.text?.trimString().isValidEmail() == false
        {
            Utility.ValidationError(errorMessage: "Please enter valid email".localized, controller: self)
            return false
        }
        else if contactTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter Phone number".localized)
            return false
        }
        else if contactTxtFld.text!.trimString().count < 7 {
            self.presentAlertWith(message: "Phone number should be between 7 to 15 character".localized)
            return false
        }
        else if contactTxtFld.text!.trimString().count > 15 {
            self.presentAlertWith(message: "Phone number should be between 7 to 15 character".localized)
            return false
        }
        else if contactTxtFld.text?.trimString().isValidPhoneNumber() == false {
            self.presentAlertWith(message: "Please enter valid Phone number".localized)
            return false
        }
        else if addressTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter address".localized)
            return false
        }
        else if ageTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please select age".localized)
            return false
        }
        else if genderTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please select gender".localized)
            return false
        }
        
        return true
    }
}


//MARK:-API Calling
extension EditProfileVC{
    
    //MARK: Get Complete Profile
    func wsGetCompleteProfileData(){
        let params : [String:Any] = [
            "UserId": AppManager.shared.userModel.id ?? 0,
            ]
        
        showHud()
        WebServiceHelper.request(path: .getCustomerProfile, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let dictResponse = rootModel.data as? [String:Any]{
                                self.objProfile = ProfileModel(fromDictionary: dictResponse)
                                
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    //MARK: Update Profile
    func wsUpdateProfileData(){
        
        let objParams = ProfileModel()
        objParams.email = emailTxtFld.text
        objParams.contact = contactTxtFld.text
        objParams.address = addressTxtFld.text
        objParams.customerName = nameTxtFld.text
        objParams.age = Int(ageTxtFld.text!)
        
        if genderTxtFld.text?.lowercased() == "male".localized {
            objParams.gender = gender.male.rawValue
        }else if genderTxtFld.text?.lowercased() == "female".localized {
            objParams.gender = gender.female.rawValue
        }else if genderTxtFld.text?.lowercased() == "other".localized {
            objParams.gender = gender.other.rawValue
        }else {
            objParams.gender = gender.unknown.rawValue
        }
        
        objParams.logoProfilePic = strBase64Image
        
        
        showHud()
        
        WebServiceHelper.request(path: .updateProfile, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: objParams.toDictionary()) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let dictResponse = rootModel.data as? [String:Any]{
                                AppManager.shared.userModel = UserModel(fromDictionary: dictResponse)
                                self.presentAlertWith(message: rootModel.successMsg) {
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    
}


