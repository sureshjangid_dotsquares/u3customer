//
//  ProfileVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 03/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import McPicker
import Kingfisher

class ProfileVC: U3BaseViewController {

    //MARK:-IBOutlets
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var emailTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var contactTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var addressTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var nameTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var ageTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var genderTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var userImageView: UIImageView!
    
    lazy var strBase64Image : String = {
        let str = Utility.getStingFromImg(image :userImageView.image!)
        return str
    }()
    
    //MARK:-Variables
    var objProfile : ProfileModel! {
        didSet {
            emailTxtFld.text = objProfile.email ?? ""
            contactTxtFld.text = objProfile.contact ?? ""
            addressTxtFld.text = objProfile?.address ?? ""
            nameTxtFld.text = objProfile.customerName ?? ""
            if objProfile.age == 0 {
                ageTxtFld.text = ""
            }else {
                ageTxtFld.text = String(format:"%d",objProfile.age)
            }
            
            switch objProfile.gender {
            case gender.male.rawValue:
                genderTxtFld.text = "Male".localized
            case gender.female.rawValue:
                genderTxtFld.text = "Female".localized
            case gender.other.rawValue:
                genderTxtFld.text = "Other".localized
            default:
                genderTxtFld.text = ""
            }
            let url = URL(string: objProfile.logoProfilePic)
            userImageView.kf.setImage(with: url, placeholder: Images.profilePlaceholder.icon)
            self.userImageView.layer.cornerRadius =  self.userImageView.bounds.width/2
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        super.setupMenuBtn()
        super.rightBarBtnPressed()
        self.initSetup()
        
        if let nav = self.navigationController as? U3NavigationController {
            nav.clearNavBarColor()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.wsGetCompleteProfileData()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.view.setGradient(colors: [rFirstCgColor.cgColor,rSecondCgColor.cgColor,rThirdCgColor.cgColor], angle: 315)
        viewBg.setGradient(colors: [rFirstCgColor.cgColor,rSecondCgColor.cgColor,rThirdCgColor.cgColor], angle: 315)
        self.viewBg.roundCorners([.layerMinXMaxYCorner], radius: 50)
    }
    
    
    func initSetup(){
        
        self.title = "Profile".localized
        if let nav = self.navigationController as? U3NavigationController {
            nav.clearNavBarColor()
        }
                
    }
    
    //MARK:-IBAction Methods
    
    @objc override func rightBtnAction(_ sender: UIBarButtonItem){
        if let vc = storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as? EditProfileVC{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didTapChangePassword(_ sender: Any) {
        if let vc = Storyboards.loginStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as? ChangePasswordVC{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}


//MARK:-API Calling
extension ProfileVC{
    
    //MARK: Get Complete Profile
    func wsGetCompleteProfileData(){
        let params : [String:Any] = [
            "UserId": AppManager.shared.userModel.id ?? 0,
            ]
        
        showHud()
        WebServiceHelper.request(path: .getCustomerProfile, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let dictResponse = rootModel.data as? [String:Any]{
                                self.objProfile = ProfileModel(fromDictionary: dictResponse)
                                
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    
}

