//
//  RedeemFreeBoxVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 28/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class RedeemFreeBoxVC: U3BaseViewController {

    //MARK:-IBOutlets
    @IBOutlet weak var imgQrCode:UIImageView!
    @IBOutlet weak var lblCode : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.setupMenuBtn()
        self.wsRedeemFreebox()
        
        self.title = "Redeem Free Boxes".localized

        
        if let nav = self.navigationController as? U3NavigationController {
            nav.clearNavBarColor()
        }
        
    }
    
    //MARK:-Variables
    var objQr : QrModel! {
        didSet {
            if let decodedData = Data(base64Encoded: objQr.qrBase64Code, options: .ignoreUnknownCharacters) {
                imgQrCode.image = UIImage(data: decodedData)
            }
            lblCode.text = objQr.alphanumericCode
            
        }
    }

}


//MARK:-API Calling
extension RedeemFreeBoxVC{
    
    func wsRedeemFreebox(){
        let params : [String:Any] = ["": ""]
        
        showHud()
        WebServiceHelper.request(path: .redeemFreebox, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let dictResponse = rootModel.data as? [String:Any]{
                                self.objQr = QrModel(fromDictionary: dictResponse)
                                
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
}
