//
//  AddSaleItemCell.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 25/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class AddSaleItemCell: UICollectionViewCell {
    
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var btnCross: UIButton!
    
    var callbackDeleteImg: (()->())?
    
    var obj: FileNameModel! {
        didSet {
            let url = URL(string: obj.fileURL)
            imgView.kf.setImage(with: url, placeholder: Images.itemPlaceholder.icon)
        }
    }
    
    @IBAction func didTapCrossBtn(_ sender: Any) {
        callbackDeleteImg?()
    }
    
}

class PlusBtnCell: UICollectionViewCell {
    
    override func draw(_ rect: CGRect) {
        self.contentView.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        self.contentView.layer.shadowOffset = CGSize(width:0, height:5)
        self.contentView.layer.shadowOpacity = 1.0
        self.contentView.layer.shadowRadius = 5
        self.contentView.layer.cornerRadius = 10
        self.contentView.backgroundColor = .white
    }
}
