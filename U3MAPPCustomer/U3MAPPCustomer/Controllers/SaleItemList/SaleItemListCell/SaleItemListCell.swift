//
//  SaleItemListCell.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 27/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import CHIPageControl

class SaleItemListCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var coltnCard: UICollectionView!
    @IBOutlet weak var verticalPageControl: CHIPageControlJaloro!
    
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblItemCatName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnStatus: UIButton?
    @IBOutlet weak var btnEdit: UIButton?
    @IBOutlet weak var btnDelete: UIButton?
    @IBOutlet weak var stackActions: UIStackView?
    
    var callbackStatus: (()->())?
    var callbackDelete: (()->())?
    var callbackEdit: (()->())?
    var callbackColtnTap: (()->())?
    
    var obj: SaleItemModel! {
        didSet {
            lblItemName.text = obj.itemName
            lblItemCatName.text = obj.itemCategoryName
            
            if obj.isForSell == 1 {
                lblPrice.text = obj.price
            }else {
                lblPrice.text = "Free".localized
            }
            
            switch obj.itemStatus {
            case 1:
                btnStatus?.setTitle("Mark Occupied".localized, for: .normal)
            case 2:
                btnStatus?.setTitle("Occupied".localized, for: .normal)
            default:
                btnStatus?.setTitle("Removed".localized, for: .normal)
            }
            
            if obj.itemStatus == 2 {
                stackActions?.isHidden = true
            }else {
                stackActions?.isHidden = false
            }
            
            self.verticalPageControl.numberOfPages = obj.listOfFiles.count

            
            coltnCard.dataSource = self
            coltnCard.delegate = self
            coltnCard.reloadData()
            coltnCard.isPagingEnabled = true
            coltnCard.showsHorizontalScrollIndicator = false
            coltnCard.showsVerticalScrollIndicator = false
            
            //you can activate touch through code
//            self.verticalPageControl.enableTouchEvents = true
//            self.verticalPageControl.delegate = self
        }
    }
    
    @IBAction func didTapBtnStatus(_ sender: Any) {
        guard obj.itemStatus == 1 else {return}
        callbackStatus?()
    }
    
    @IBAction func didTapEditBtn(_ sender: Any) {
        callbackEdit?()
    }
    
    @IBAction func didTapCrossBtn(_ sender: Any) {
        self.topMostController()?.presentAlertWith(message: "Are you sure you want to delete this Item?".localized, oKTitle: "Ok".localized, okaction: {
            self.callbackDelete?()
        }, cancelAction: {})
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.backgroundColor = UIColor.white
        containerView.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.05).cgColor
        containerView.layer.shadowOffset = CGSize(width:0, height:9)
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowRadius = 10
        containerView.layer.cornerRadius = 5.0
        containerView.layer.masksToBounds = false
        containerView.clipsToBounds = false
        
        btnStatus?.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        btnStatus?.layer.shadowOffset = CGSize(width:0, height:5)
        btnStatus?.layer.shadowOpacity = 1.0
        btnStatus?.layer.shadowRadius = 5
        btnStatus?.layer.cornerRadius = 15
        btnStatus?.backgroundColor = .white
        btnStatus?.layer.masksToBounds = false
        btnStatus?.clipsToBounds = false

        btnEdit?.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        btnEdit?.layer.shadowOffset = CGSize(width:0, height:5)
        btnEdit?.layer.shadowOpacity = 1.0
        btnEdit?.layer.shadowRadius = 5
        btnEdit?.layer.cornerRadius = 15
        btnEdit?.backgroundColor = .white
        btnEdit?.layer.masksToBounds = false
        btnEdit?.clipsToBounds = false

        btnDelete?.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        btnDelete?.layer.shadowOffset = CGSize(width:0, height:5)
        btnDelete?.layer.shadowOpacity = 1.0
        btnDelete?.layer.shadowRadius = 5
        btnDelete?.layer.cornerRadius = 15
        btnDelete?.backgroundColor = .white
        btnDelete?.layer.masksToBounds = false
        btnDelete?.clipsToBounds = false
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


extension SaleItemListCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCollectionViewCell", for: indexPath) as! CardCollectionViewCell
        let url = URL(string: self.obj.listOfFiles[indexPath.item].fileURL)
        cell.imgBanner.kf.setImage(with: url, placeholder: Images.itemPlaceholder.icon)
        cell.imgBanner.contentMode = .scaleAspectFit
        cell.imgBanner.layer.masksToBounds = true
        cell.backgroundColor = Utility.random()
                
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return obj.listOfFiles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        callbackColtnTap?()
    }
        
}

extension SaleItemListCell: CHIBasePageControlDelegate {
    func didTouch(pager: CHIBasePageControl, index: Int) {
        print(pager, index)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let midX:CGFloat = scrollView.bounds.midX
        let midY:CGFloat = scrollView.bounds.midY
        let point:CGPoint = CGPoint(x:midX, y:midY)
        
        guard
            
            let indexPath:IndexPath = coltnCard.indexPathForItem(at:point)
            
            else
        {
            return
        }
        
        let currentPage:Int = indexPath.item
         self.verticalPageControl.progress = Double(currentPage)
    }
}
