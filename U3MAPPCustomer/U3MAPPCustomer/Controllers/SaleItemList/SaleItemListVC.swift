//
//  SaleItemListVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 27/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


class SaleItemListVC: U3BaseViewController {

    @IBOutlet var tblList: UITableView!
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet weak var lblNoData: UILabel!
    
    var arrItem = [SaleItemModel]()
    var objSaleItem : EditSaleItemModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.wsMySellItemList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
    }
    
    func updateUI() {
        
        if let nav = self.navigationController as? U3NavigationController {
            nav.hideNavBottomLine()
        }
        
        self.title = "Upload".localized

        setupMenuBtn()
        txtSearch.returnKeyType = .done
        rightBarBtnPressed(img: Images.plus.icon)
        
    }
    
    @objc override func rightBtnAction(_ sender: UIBarButtonItem){
        if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "AddSaleItemVC") as? AddSaleItemVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func showNoDataText(){
        if arrItem.count > 0{
            lblNoData.isHidden = true
            tblList.isHidden = false
        }
        else{
            lblNoData.isHidden = false
            tblList.isHidden = true
        }
    }

}

extension SaleItemListVC: UITableViewMethods  {
    //MARK: UITableViewDelegate UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SaleItemListCell", for: indexPath) as? SaleItemListCell  else {
            return UITableViewCell()
        }
        cell.obj = self.arrItem[indexPath.row]
        cell.callbackStatus = {
            self.wsMarkSellItemOccupied(itemID: self.arrItem[indexPath.row].sellItemId) { (status) in
                if status {
                    self.arrItem[indexPath.row].itemStatus = 2
                    self.tblList.reloadData()
                }
            }
        }
        cell.callbackDelete = {
            self.wsDeleteSellItem(itemID: self.arrItem[indexPath.row].sellItemId) { (status) in
                if status {
                    self.arrItem.remove(at: indexPath.row)
                    self.tblList.reloadData()
                }
            }
        }
        cell.callbackEdit = {
            
            self.wsGetMySellItemList(SellItemId: self.arrItem[indexPath.row].sellItemId) { (status) in
                if status {
                    if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "EditSaleItemList") as? EditSaleItemList {
                        vc.objSaleItem = self.objSaleItem
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
        cell.callbackColtnTap = {
            if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "SaleItemDetailVC") as? SaleItemDetailVC {
                vc.itemId = self.arrItem[indexPath.row].sellItemId
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "SaleItemDetailVC") as? SaleItemDetailVC {
            vc.itemId = self.arrItem[indexPath.row].sellItemId
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension SaleItemListVC : UITextFieldDelegate {
    // MARK: - UITextField Delegate
    
    /*
     * textFieldShouldBeginEditing method is a textfield delegate method invoke if editing should begin in the specified text field.
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.wsMySellItemList(searchString: textField.text!.trimString())
        return true
    }
    
}


//MARK: Webservice CALL
extension SaleItemListVC {
    
    private func wsMySellItemList(searchString : String = "", completion: ((_ status: Bool) -> Void)? = nil){

        let params : [String:Any] =
            
            ["SearchItemName":searchString as Any]
       
        showHud()
        WebServiceHelper.request(path: .mySellItemList, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let arrResponse = rootModel.data as? [[String:Any]] {
                                self.arrItem.removeAll()
                                self.arrItem = arrResponse.map{return SaleItemModel(fromDictionary: $0)}
                                self.showNoDataText()
                                if self.arrItem.count > 0 {
                                    self.tblList.reloadData()
                                }
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                            completion?(false)
                        }
                    }
                }else{
                    completion?(false)
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    
    private func wsGetMySellItemList(SellItemId : Int, completion: ((_ status: Bool) -> Void)? = nil){

        let params : [String:Any] =
                        
            ["SellItemId":SellItemId as Any]
       
        showHud()
        WebServiceHelper.request(path: .getMySellItem, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let dictResponse = rootModel.data as? [String:Any] {
                                self.objSaleItem = EditSaleItemModel(fromDictionary: dictResponse)
                                completion?(true)
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                            completion?(false)
                        }
                    }
                }else{
                    completion?(false)
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    
    private func wsMarkSellItemOccupied(itemID : Int = 0, completion: ((_ status: Bool) -> Void)? = nil) {

        let params : [String:Any] =
                        
            ["SellItemId":itemID as Any]
       
        showHud()
        WebServiceHelper.request(path: .markSellItemOccupied, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if (rootModel.data as? Bool) != nil {
                                self.presentAlertWith(message: rootModel.successMsg) {
                                    completion?(true)
                                }
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                            completion?(false)
                        }
                    }
                }else{
                    completion?(false)
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    private func wsDeleteSellItem(itemID : Int = 0, completion: ((_ status: Bool) -> Void)? = nil) {

        let params : [String:Any] =
                        
            ["SellItemId":itemID as Any]
       
        showHud()
        WebServiceHelper.request(path: .deleteSellItem, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if (rootModel.data as? Bool) != nil {
                                self.presentAlertWith(message: rootModel.successMsg) {
                                    completion?(true)
                                }
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                            completion?(false)
                        }
                    }
                }else{
                    completion?(false)
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
}
