//
//  EditSaleItemList.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 31/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

import UIKit
import McPicker
import CropViewController

class EditSaleItemList: U3BaseViewController {

    @IBOutlet var colItem: UICollectionView!
    @IBOutlet weak var txtItemName: CustomTextFieldAnimated!
    @IBOutlet weak var txtCategory:CustomTextFieldAnimated!
    @IBOutlet weak var txtSellGiveAway: CustomTextFieldAnimated!
    @IBOutlet weak var txtLocation:CustomTextFieldAnimated!
    @IBOutlet weak var txtPrice: CustomTextFieldAnimated!
    @IBOutlet weak var txtDescription: CustomTextFieldAnimated!
    
    var arrUploadFiles = [FileNameModel]()
    var arrCategoryItem = [BusinessCategoryModel]()
    var selectedCategoryId = 0
    var selectedImg = UIImage()
    var latitude = PCLocationManager.shared().myLocation.latitude , longitude = PCLocationManager.shared().myLocation.longitude
    lazy var strBase64Image = ""
    var objSaleItem : EditSaleItemModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let nav = self.navigationController as? U3NavigationController {
            nav.hideNavBottomLine()
        }
        self.title = "Add Item"
    }
    
    func updateUI() {
        
        backBarBtnPressed()
        
        txtLocation.setRightViewImage(Images.location.icon)
        txtCategory.setRightViewImage(Images.downArrow.icon)
        txtSellGiveAway.setRightViewImage(Images.downArrow.icon)
        
        txtItemName.text = objSaleItem.itemName
        txtCategory.text = objSaleItem.itemCategory
        selectedCategoryId = objSaleItem.itemCategoryID
        
        if objSaleItem.sellOrGiveAway == 0 {
            self.txtPrice.text = "Free"
            self.txtPrice.isUserInteractionEnabled = false
        }else {
            self.txtPrice.text = objSaleItem.price
            self.txtPrice.isUserInteractionEnabled = true
        }
        txtLocation.text = objSaleItem.locationAddress
        latitude = objSaleItem.locationLatitude
        longitude = objSaleItem.locationLongitude
        txtDescription.text = objSaleItem.descriptionField
        
        for item in objSaleItem.listOfFiles {
            let obj = FileNameModel(fromDictionary: item.toDictionary())
            arrUploadFiles.append(obj)
        }
        
        if objSaleItem.sellOrGiveAway == 1 {
            txtSellGiveAway.text = "For Sell"
        }else {
            txtSellGiveAway.text = "For Give Away"
        }
        

    }
    //MARK: - IBActions -
    
    @IBAction func didTapOnPlus(_ sender: Any) {
        MediaUtilityClass.sharedInstanse().pickImage { (image, url, error) in
            if image != nil{
                
                let cropController = CropViewController(croppingStyle: .default, image: image!)
                cropController.delegate = self
                cropController.customAspectRatio = CGSize(width: 200,height: 100)
                cropController.aspectRatioLockEnabled = true
                cropController.resetAspectRatioEnabled = false
                cropController.aspectRatioPickerButtonHidden = true
                cropController.rotateButtonsHidden = true
                cropController.rotateClockwiseButtonHidden = true
                cropController.doneButtonTitle = "Crop".localized
                cropController.cancelButtonTitle = "Cancel".localized
                self.present(cropController, animated: true, completion: nil)
                
            }
        }
    }
    
    @IBAction func didTapOnSearch(_ sender: Any) {
        if ValidateEntries() == false {
            return
        }
        self.wsUpdateSellItem()
    }
    
}

extension EditSaleItemList : CropViewControllerDelegate {
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        self.selectedImg = image
        self.strBase64Image = Utility.getStingFromImg(image :self.selectedImg)
        self.wsUploadImage(base64: self.strBase64Image)
        cropViewController.dismiss(animated: true, completion: nil)
        
    }
}

extension EditSaleItemList: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    // MARK:  CollectionViewDelegate Methods
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 180, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrUploadFiles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddSaleItemCell", for: indexPath) as! AddSaleItemCell
        itemCell.obj = arrUploadFiles[indexPath.item]
        itemCell.callbackDeleteImg = {
            self.arrUploadFiles.remove(at: indexPath.item)
            self.colItem.reloadData()
        }
        return itemCell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.item == 0 {
            
        }
    }
    
}


extension EditSaleItemList:UITextFieldDelegate {
    // MARK: - UITextField Delegate
    
    /*
     * textFieldShouldBeginEditing method is a textfield delegate method invoke if editing should begin in the specified text field.
     */
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        if textField == txtLocation{
            self.view.endEditing(true)
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            guard let objVC = storyBoard.instantiateViewController(withIdentifier: "LocationMapVC") as? LocationMapVC else {return false}
            objVC.callback = { (lat,lang,address) in
                self.latitude = Double(lat)!;self.longitude = Double(lang)!
                self.txtLocation.text = address
            }
            self.navigationController?.pushViewController(objVC, animated: true)
            return false
        }else if textField == txtCategory {
            self.view.endEditing(true)
            if self.arrCategoryItem.count > 0 {
                McPicker.show(data: [self.arrCategoryItem.map { String($0.category) }]) {(selections: [Int : String]) -> Void in
                    self.txtCategory.text = selections[0]
                }
            }else {
                self.wsSellItemCategoryList { (status: Bool) in
                    if status {
                        McPicker.show(data: [self.arrCategoryItem.map { String($0.category) }]) {(selections: [Int : String]) -> Void in
                            self.txtCategory.text = selections[0]
                            let arr = self.arrCategoryItem.filter({$0.category == selections[0]})
                            self.selectedCategoryId = arr[0].id
                        }
                    }
                }
            }
            
            return false
        }
        else if textField == txtSellGiveAway {
            self.view.endEditing(true)
            McPicker.show(data: [["For Sell", "For Give Away"]]) {(selections: [Int : String]) -> Void in
                textField.text = selections[0]
                if selections[0] == "For Give Away" {
                    self.txtPrice.text = "Free"
                    self.txtPrice.isUserInteractionEnabled = false
                }else {
                    self.txtPrice.text = ""
                    self.txtPrice.isUserInteractionEnabled = true
                }
            }
            return false
        }
        return true
    }
}


extension EditSaleItemList {
    
    //MARK: - Validation
    func ValidateEntries() -> Bool {
        
        if self.arrUploadFiles.count == 0 {
            self.presentAlertWith(message: "Please add atleast an item image first")
            return false
        }
        else if txtItemName.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter item name")
            return false
        }
        else if txtCategory.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please select item category")
            return false
        }
        else if txtSellGiveAway.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please select any option")
            return false
        }
        else if txtLocation.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please select your location")
            return false
        }
        else if txtPrice.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter item price")
            return false
        }
        else if txtDescription.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter item description")
            return false
        }
        
        return true
    }
}

//MARK: Webservice CALL
extension EditSaleItemList {
    private func wsUploadImage(base64:String){
        
        let params : [String:Any] = [
            "Base64Image": base64 ]
        
        showHud()
        WebServiceHelper.request(path: .uploadImage, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let dictResponse = rootModel.data as? [String:Any] {
                                let obj = FileNameModel(fromDictionary: dictResponse)
                                DispatchQueue.main.async {
                                    self.arrUploadFiles.append(obj)
                                    self.colItem.reloadData()
                                    self.strBase64Image = ""
                                    self.selectedImg = UIImage()
                                }
                                
                            }
                        }else if rootModel.responseCode == ErrorCode.validationFailed.rawValue{
                            
                            if let validationArray = rootModel.validationErrors as? [String] {
                                self.presentAlertWith(message: validationArray[0])
                            }
                            
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg) {
                                
                            }
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized) {
                            
                        }
                    }
                }
                
            }
        }
    }
    
    private func wsSellItemCategoryList( completion: ((_ status: Bool) -> Void)? = nil){
        let params : [String:Any] = ["":""]
        
        showHud()
        WebServiceHelper.request(path: .sellItemCategory, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let arrResponse = rootModel.data as? [[String:Any]]{
                                self.arrCategoryItem.removeAll()
                                self.arrCategoryItem = arrResponse.map{return BusinessCategoryModel(fromDictionary: $0)}
                                completion?(self.arrCategoryItem.count == 0 ? false : true)
                                
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                            completion?(false)
                        }
                    }
                }else{
                    completion?(false)
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    
    private func wsUpdateSellItem( completion: ((_ status: Bool) -> Void)? = nil){

        
        let tempArr = self.arrUploadFiles.filter{$0.fileName != ""}
        
        var filterArr = [[String:Any]]()
        for item in tempArr {
            let obj = FileNameRequestModel(fileName: item.fileName)
            filterArr.append(obj.toDictionary())
        }

        var price = 0.0
        if txtPrice.text!.trimString() != "Free" {
            price = Double(txtPrice.text!.trimString())!
        }
        
        let params : [String:Any] =
                        
            ["SellItemId":objSaleItem.sellItemId! as Any,
             "ItemName":txtItemName.text!.trimString() as Any,
             "ItemCategory":selectedCategoryId,
             "IsForSell":txtSellGiveAway.text!.trimString() == "For Sell" ? 1 : 0,
             "LocationAddress":txtLocation.text!.trimString() as Any,
             "LocationLatitude":latitude,
             "LocationLongitude":longitude,
             "Price":price as Any,
             "Description":txtDescription.text!.trimString() as Any,
             "ListOfFiles":filterArr as Any]
        
        
        showHud()
        WebServiceHelper.request(path: .updateSellItem, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if (rootModel.data as? Bool) != nil{
                                self.presentAlertWith(message: rootModel.successMsg) {
                                    self.navigationController?.popViewController(animated: true)
                                }
                                
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                            completion?(false)
                        }
                    }
                }else{
                    completion?(false)
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
}
