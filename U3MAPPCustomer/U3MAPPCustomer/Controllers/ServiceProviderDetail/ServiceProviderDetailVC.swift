//
//  ServiceProviderDetailVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 17/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import FloatRatingView
import CoreLocation
import MapKit
import SafariServices

class ServiceProviderDetailVC: U3BaseViewController,SFSafariViewControllerDelegate {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var rateView: FloatRatingView!
    @IBOutlet weak var txtServiceName: CustomTextFieldAnimated!
    @IBOutlet weak var txtServiceCategory: CustomTextFieldAnimated!
    @IBOutlet weak var txtArea: CustomTextFieldAnimated!
    @IBOutlet weak var txtPricing: CustomTextFieldAnimated!
    @IBOutlet weak var txtContact: CustomTextFieldAnimated!
    @IBOutlet weak var txtAddress: CustomTextFieldAnimated!
    @IBOutlet weak var txtDistance: CustomTextFieldAnimated!
    
    
    @IBOutlet weak var btnGetDirection: UIButton!
    @IBOutlet weak var btnMoreDetail: UIButton!
    @IBOutlet weak var btnRateReview: UIButton!
    
    
    var directoryId: Int!
    var latitude = PCLocationManager.shared().myLocation.latitude , longitude = PCLocationManager.shared().myLocation.longitude
    var objDirectory: ServiceDirectoryDetailModel!{
        didSet {
            let url = URL(string: self.objDirectory.serviceDirectoryLogo)
            imgProfile.kf.setImage(with: url, placeholder: Images.itemPlaceholder.icon)
            txtUserName.text = objDirectory.businessName
            txtServiceName.text = objDirectory.directoryName
            txtServiceCategory.text = objDirectory.directoryCategory
            txtArea.text = objDirectory.areaOfOperation
            txtPricing.text = objDirectory.pricing
            txtContact.text = objDirectory.contactNumber
            txtAddress.text = objDirectory.address
            txtDistance.text = String(format:"%@ %@",objDirectory.distance, "miles".localized)
            
            rateView.rating = Double(objDirectory.currentRating) ?? 0.00
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.backBarBtnPressed()

        setupUI()
    }
    
    fileprivate func setupUI() {
        
        self.wsServiceDirectoryDetail()
        
        btnGetDirection.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        btnGetDirection.layer.shadowOffset = CGSize(width:0, height:5)
        btnGetDirection.layer.shadowOpacity = 1.0
        btnGetDirection.layer.shadowRadius = 5
        btnGetDirection.layer.cornerRadius = btnGetDirection.bounds.height/2
        btnGetDirection.backgroundColor = .white
        
        btnMoreDetail.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        btnMoreDetail.layer.shadowOffset = CGSize(width:0, height:5)
        btnMoreDetail.layer.shadowOpacity = 1.0
        btnMoreDetail.layer.shadowRadius = 5
        btnMoreDetail.layer.cornerRadius = 5
        btnMoreDetail.backgroundColor = .white
        
        btnRateReview.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        btnRateReview.layer.shadowOffset = CGSize(width:0, height:5)
        btnRateReview.layer.shadowOpacity = 1.0
        btnRateReview.layer.shadowRadius = 5
        btnRateReview.layer.cornerRadius = 5
    }
    
    //MARK:- IBActions
    
    @IBAction func didTapOnRateReview(_ sender: Any) {
        
        if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "RateReviewVC") as? RateReviewVC{
            vc.directoryId = self.directoryId
            vc.objDirectory = self.objDirectory
            vc.callback = { obj in
                self.objDirectory = obj
            }
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    @IBAction func didTapGetDirection(_ sender: Any) {
        
        guard objDirectory != nil else {return}
            
            if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                
                UIApplication.shared.open(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(String(describing: (objDirectory?.latitude)!)),\(String(describing: (objDirectory?.longitude)!))&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
               
                
            } else {
    //            print("You haven't google maps application, please download.")
                
                let coordinate = CLLocationCoordinate2DMake((objDirectory?.latitude)!,(objDirectory?.longitude)!)
                let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
                mapItem.name = "Target location".localized
                mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
                
                
            }
            
        }
    
    @IBAction func didTapMoreDetail(_ sender: Any) {
        guard objDirectory.websiteAppLink != nil else {self.presentAlertWith(message: "Currently we have no data into this.".localized);return}
        let url = URL(string: objDirectory.websiteAppLink.validURL())!
        let controller = SFSafariViewController(url: url)
        self.present(controller, animated: true, completion: nil)
        controller.delegate = self
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}


//MARK:-API Calling
extension ServiceProviderDetailVC{
    
    func wsServiceDirectoryDetail( completion: ((_ status: Bool) -> Void)? = nil){
        let params : [String:Any] =
            ["DirectoryId":directoryId! as Any,
            "Latitude":self.latitude,
            "Longitude":self.longitude]
        
        showHud()
        WebServiceHelper.request(path: .serviceDetails, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let dictResponse = rootModel.data as? [String:Any]{
                                self.objDirectory = ServiceDirectoryDetailModel(fromDictionary: dictResponse)
                                completion?(true)
                                
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                            completion?(false)
                        }
                    }
                }else{
                    completion?(false)
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
}
