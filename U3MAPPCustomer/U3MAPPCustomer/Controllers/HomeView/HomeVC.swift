//
//  HomeVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 29/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import Kingfisher
import SafariServices
import MKMagneticProgress

class HomeVC: U3BaseViewController,SFSafariViewControllerDelegate {

    //MARK:- IBOutltes
    @IBOutlet weak var viewCircle:MKMagneticProgress!
    @IBOutlet weak var viewFirst: UIView!
    @IBOutlet weak var imgAdvertise: UIImageView!
    
    var objAdvertise = AdvertisementPopupModel()
        
    //MARK:- ViewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        super.setupMenuBtn()
        self.title = "Home".localized
        if let nav = self.navigationController as? U3NavigationController {
            nav.clearNavBarColor()
        }
        
        
        self.wsGetPromotionalAdvertisementStrip { (status, objAdvertiseModel) in
            if status {
                guard objAdvertiseModel.advertisementURL != nil else {return}
                let url = URL(string: objAdvertiseModel.advertisementURL)
                self.imgAdvertise.kf.setImage(with: url, placeholder: Images.demoAdvertise.icon)
                self.imgAdvertise.contentMode = .scaleToFill
                
                self.objAdvertise = objAdvertiseModel
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+2.0) {
            self.promotionalAdvertise()
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.view.setGradient(colors: [rFirstCgColor.cgColor,rSecondCgColor.cgColor,rThirdCgColor.cgColor], angle: -315)
        viewFirst.setGradient(colors: [rFirstCgColor.cgColor,rSecondCgColor.cgColor,rThirdCgColor.cgColor], angle: -315)
        self.viewFirst.roundCorners([.layerMinXMaxYCorner], radius: 50)
        
        if let percentage = AppManager.shared.userModel.checklistCompletionPercentage {
            let data = Float(percentage)/100
            viewCircle.setProgress(progress: CGFloat(data), animated: true)
        }

    }
    
    fileprivate func redeemFreeBoxPopUp() {
        if AppManager.shared.hasRedeemedFreeBox == false {
            if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "CongratulationVC") as? CongratulationVC {
                vc.callback = { status in
                    AppManager.shared.hasRedeemedFreeBox = true
                    if status {
                        self.open3UAgent()
                    }
                }
                self.present(vc, animated: true, completion: nil)
            }
        }
    }

    fileprivate func promotionalAdvertise() {
        if AppManager.shared.isAdOpenedAlready == false {
            self.wsGetPromotionalAdvertisementPopup { (status, objAdvertiseModel) in
                if status {
                    if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "AdvertisementPopupVC") as? AdvertisementPopupVC{
                        vc.objAdvertise = objAdvertiseModel
                        vc.callback = {
                            AppManager.shared.isAdOpenedAlready = true
                            self.redeemFreeBoxPopUp()
                        }
                        self.present(vc, animated: false, completion: nil)
                    }
                }
            }
        }else {
            self.redeemFreeBoxPopUp()
        }
        
    }
    
    fileprivate func open3UAgent() {
        //Move to 3UAgent Screen
        if let secondViewController = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "Search3UAgentVC") as? Search3UAgentVC {
            let navController = U3NavigationController(rootViewController: secondViewController)
            navController.setViewControllers([secondViewController], animated:true)
            self.revealViewController().setFront(navController, animated: true)
        }
    }
    
   
    
    //MARK:- IBActions
    @IBAction func didTapOnCheckList(_ sender: Any) {
        if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "CheckListViewController") as? CheckListViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func didTapOn3UMarket(_ sender: Any) {
        if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "UThreeMarketVC") as? UThreeMarketVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didTapOnBroweItemBtn(_ sender: UIButton) {
        if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "UThreeMarketVC") as? UThreeMarketVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didTapOnSaleAndGiveBtn(_ sender: UIButton) {
        if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "SaleItemListVC") as? SaleItemListVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didTapOnAdvertise(_ sender: Any) {
        print("Advertise Pressed")
        guard self.objAdvertise.advertLink != nil else {
            self.presentAlertWith(message: "More Data not available".localized)
            return
        }
        let strUrl : String = self.objAdvertise.advertLink.validURL()
        let url = URL(string: strUrl)!
        let controller = SFSafariViewController(url: url)
        self.present(controller, animated: true, completion: nil)
        controller.delegate = self
        
        
    }
    
    @IBAction func didTapOnServiceDirectory(_ sender: Any) {
        if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "SearchDirectoryVC") as? SearchDirectoryVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didTapOnQRCode(_ sender: Any) {
        if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "ScanQRVC") as? ScanQRVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didTapOnBoxesPackaging(_ sender: Any) {
        if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "SearchDirectoryVC") as? SearchDirectoryVC{
            vc.selectedCategoryId = 209
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didTapOnUtilities(_ sender: Any) {
        if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "SearchDirectoryVC") as? SearchDirectoryVC{
            vc.selectedCategoryId = 210
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}


//MARK:-API Calling
extension HomeVC{
    
    func wsGetPromotionalAdvertisementPopup( completion: ((_ status: Bool,_ model: AdvertisementPopupModel) -> Void)? = nil){
        let params : [String:Any] =
            ["":""]
        
        showHud()
        WebServiceHelper.request(path: .getPromotionalAdvertisementPopup, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let dictResponse = rootModel.data as? [String:Any] {
                                let obj = AdvertisementPopupModel(fromDictionary: dictResponse)
                                completion?(true, obj)
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                            completion?(false, AdvertisementPopupModel())
                        }
                    }
                }else{
                    completion?(false, AdvertisementPopupModel())
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    func wsGetPromotionalAdvertisementStrip( completion: ((_ status: Bool,_ model: AdvertisementPopupModel) -> Void)? = nil){
        let params : [String:Any] =
            ["":""]
        
        showHud()
        WebServiceHelper.request(path: .getPromotionalAdvertisementStrip, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let dictResponse = rootModel.data as? [String:Any] {
                                let obj = AdvertisementPopupModel(fromDictionary: dictResponse)
                                completion?(true, obj)
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                            completion?(false, AdvertisementPopupModel())
                        }
                    }
                }else{
                    completion?(false, AdvertisementPopupModel())
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
}
