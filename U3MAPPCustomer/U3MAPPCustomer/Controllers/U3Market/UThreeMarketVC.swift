//
//  UThreeMarketVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 07/04/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import McPicker

class UThreeMarketVC: U3BaseViewController {

    @IBOutlet var tblList: UITableView!
    @IBOutlet weak var txtCategory:CustomTextFieldAnimated!
    @IBOutlet weak var txtSellGiveAway: CustomTextFieldAnimated!
    @IBOutlet weak var btnSearch:UIButton!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var constraintSearchViewHeight: NSLayoutConstraint!
    
    var arrCategoryItem = [BusinessCategoryModel]()
    var arrItem = [SaleItemModel]()
    var objSaleItem : EditSaleItemModel!
    var selectedCategoryId = 0
    var selectedType = saleType.all
    var isShowSearchView = false {
        didSet {
            self.showSearchView(status: isShowSearchView)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.wsSellItemCategoryList { (status: Bool) in
            self.wsBrowseSellItem()
        }
    }
    
    func updateUI() {
        
        if let nav = self.navigationController as? U3NavigationController {
            nav.hideNavBottomLine()
        }
        
        self.title = "3U Market".localized
        
        txtCategory.setRightViewImage(Images.downArrow.icon)
        txtSellGiveAway.setRightViewImage(Images.downArrow.icon)
        
        txtCategory.delegate = self
        txtSellGiveAway.delegate = self
        
        setupMenuBtn()
        
    }
    
    func showNoDataText(){
        if arrItem.count > 0{
            lblNoData.isHidden = true
            tblList.isHidden = false
        }
        else{
            lblNoData.isHidden = false
            tblList.isHidden = true
        }
    }
    
    //MARK:- IBActions
    
    func showSearchView(status: Bool) {
        constraintSearchViewHeight.constant = status ? 280 : 44
        if constraintSearchViewHeight.constant == 280 {
            self.btnSearch.isHidden = false
            self.txtCategory.isHidden = false
            self.txtSellGiveAway.isHidden = false
        }else {
            self.btnSearch.isHidden = true
            self.txtCategory.isHidden = true
            self.txtSellGiveAway.isHidden = true
        }
        UIView.animate(withDuration: 0.3){
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func didTapOnSearchFilter(_ sender: Any) {
        if ((sender as? UIControl) != nil) {
            isShowSearchView = !isShowSearchView
        }
        
    }
    
    @IBAction func didTapOnSearchBtn(_ sender: UIButton) {
        self.wsBrowseSellItem { (status) in
            if status {
                self.isShowSearchView = true
            }else {
                self.isShowSearchView = false
            }
        }
    }

}


extension UThreeMarketVC: UITableViewMethods  {
    //MARK: UITableViewDelegate UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SaleItemListCell", for: indexPath) as? SaleItemListCell  else {
            return UITableViewCell()
        }
        cell.obj = self.arrItem[indexPath.row]
        cell.callbackColtnTap = {
            if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "UThreeMarketDetailVC") as? UThreeMarketDetailVC {
                vc.itemId = self.arrItem[indexPath.row].sellItemId
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "UThreeMarketDetailVC") as? UThreeMarketDetailVC {
            vc.itemId = self.arrItem[indexPath.row].sellItemId
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension UThreeMarketVC : UITextFieldDelegate {
    // MARK: - UITextField Delegate
    
    /*
     * textFieldShouldBeginEditing method is a textfield delegate method invoke if editing should begin in the specified text field.
     */
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtSellGiveAway {
            self.view.endEditing(true)
            McPicker.show(data: [saleType.allCases.map{$0.rawValue.localized}]) {(selections: [Int : String]) -> Void in
                textField.text = selections[0]
                let arr = saleType.allCases.filter({$0.rawValue.localized == selections[0]})
                self.selectedType = arr[0]
            }
            return false
        }else {
            self.view.endEditing(true)
            if self.arrCategoryItem.count > 0 {
                McPicker.show(data: [self.arrCategoryItem.map { String($0.category) }]) {(selections: [Int : String]) -> Void in
                    self.txtCategory.text = selections[0]
                    let arr = self.arrCategoryItem.filter({$0.category == selections[0]})
                    self.selectedCategoryId = arr[0].id
                }
            }else {
                self.wsSellItemCategoryList { (status: Bool) in
                    if status {
                        McPicker.show(data: [self.arrCategoryItem.map { String($0.category) }]) {(selections: [Int : String]) -> Void in
                            self.txtCategory.text = selections[0]
                            let arr = self.arrCategoryItem.filter({$0.category == selections[0]})
                            self.selectedCategoryId = arr[0].id
                        }
                    }
                }
            }
            return false
        }
        
    }
    
}


//MARK: Webservice CALL
extension UThreeMarketVC {
    
    private func wsBrowseSellItem(completion: ((_ status: Bool) -> Void)? = nil){

        let params : [String:Any] =
                        
            ["ItemCategoryId":selectedCategoryId as Any,
             "IsForSell":selectedType.getTypeId() as Any]
       
        showHud()
        WebServiceHelper.request(path: .browseSellItem, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let arrResponse = rootModel.data as? [[String:Any]] {
                                self.arrItem.removeAll()
                                self.arrItem = arrResponse.map{return SaleItemModel(fromDictionary: $0)}
                                self.showNoDataText()
                                if self.arrItem.count > 0 {
                                    self.tblList.reloadData()
                                }
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                            completion?(false)
                        }
                    }
                }else{
                    completion?(false)
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    private func wsSellItemCategoryList( completion: ((_ status: Bool) -> Void)? = nil){
        let params : [String:Any] = ["":""]
        
        showHud()
        WebServiceHelper.request(path: .sellItemCategory, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let arrResponse = rootModel.data as? [[String:Any]]{
                                self.arrCategoryItem.removeAll()
                                self.arrCategoryItem = arrResponse.map{return BusinessCategoryModel(fromDictionary: $0)}
                                completion?(self.arrCategoryItem.count == 0 ? false : true)
                                
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                            completion?(false)
                        }
                    }
                }else{
                    completion?(false)
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
}
