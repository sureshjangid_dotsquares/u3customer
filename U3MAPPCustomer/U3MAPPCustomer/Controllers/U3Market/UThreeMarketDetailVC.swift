//
//  UThreeMarketDetailVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 08/04/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import CHIPageControl
import MapKit

class UThreeMarketDetailVC: U3BaseViewController {

    @IBOutlet var coltnCard: UICollectionView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblCategory: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet weak var verticalPageControl: CHIPageControlJaloro!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var lblPhone: UILabel!
    @IBOutlet weak var viewUser: UIView!
    
    var itemId:Int!
    var objDetail : SaleItemDetailModel! {
        didSet {
            lblTitle.text = objDetail.itemName
            lblCategory.text = objDetail.itemCategory
            
            if objDetail.sellOrGiveAway == 1 {
                lblPrice.text = objDetail.price
            }else {
                lblPrice.text = "Free".localized
            }
            
            lblAddress.text = objDetail.locationAddress
            lblDescription.text = objDetail.descriptionField
            let url = URL(string: self.objDetail.customerProfilePic)
            imgProfile.kf.setImage(with: url, placeholder: Images.itemPlaceholder.icon)
            lblName.text = objDetail.customerName
            lblEmail.text = objDetail.customerEmail
            lblPhone.text = objDetail.customerContact
            
            self.verticalPageControl.numberOfPages = self.objDetail.listOfFiles.count
            self.coltnCard.dataSource = self
            self.coltnCard.delegate = self
            self.coltnCard.reloadData()
            self.coltnCard.isPagingEnabled = true
            self.coltnCard.showsHorizontalScrollIndicator = false
            self.coltnCard.showsVerticalScrollIndicator = false
                        
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        updateUI()
        
        self.wsSaleItemDetail { (apiStatus) in
            if apiStatus == false {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        
    }
    
    func updateUI() {
        self.title = "Item Detail".localized
        backBarBtnPressed()
        
        viewUser?.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        viewUser?.layer.shadowOffset = CGSize(width:0, height:5)
        viewUser?.layer.shadowOpacity = 1.0
        viewUser?.layer.shadowRadius = 5
        viewUser?.layer.cornerRadius = 15
        viewUser?.backgroundColor = .white
        viewUser?.layer.masksToBounds = false
        viewUser?.clipsToBounds = false
    }
    
    @IBAction func didTapLocation(_ sender: Any) {
        self.openMap()
    }
    
    func openMap() {
        
        guard objDetail != nil else {return}
            
            if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                
                UIApplication.shared.open(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(String(describing: (objDetail?.locationLatitude)!)),\(String(describing: (objDetail?.locationLongitude)!))&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
               
                
            } else {
    //            print("You haven't google maps application, please download.")
                
                let coordinate = CLLocationCoordinate2DMake((objDetail?.locationLatitude)!,(objDetail?.locationLongitude)!)
                let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
                mapItem.name = "Target location".localized
                mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
                
                
            }
            
        }
    

}

extension UThreeMarketDetailVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCollectionViewCell", for: indexPath) as! CardCollectionViewCell
        let url = URL(string: self.objDetail.listOfFiles[indexPath.item].fileURL)
        cell.imgBanner.kf.setImage(with: url, placeholder: Images.itemPlaceholder.icon)
        cell.imgBanner.contentMode = .scaleAspectFit
        cell.imgBanner.layer.masksToBounds = true
        cell.backgroundColor = Utility.random()
                
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objDetail.listOfFiles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
        
}

extension UThreeMarketDetailVC: CHIBasePageControlDelegate {
    func didTouch(pager: CHIBasePageControl, index: Int) {
        print(pager, index)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let midX:CGFloat = scrollView.bounds.midX
        let midY:CGFloat = scrollView.bounds.midY
        let point:CGPoint = CGPoint(x:midX, y:midY)
        
        guard
            
            let indexPath:IndexPath = coltnCard.indexPathForItem(at:point)
            
            else
        {
            return
        }
        
        let currentPage:Int = indexPath.item
         self.verticalPageControl.progress = Double(currentPage)
    }
}



//MARK: Webservice CALL
extension UThreeMarketDetailVC {
    
    private func wsSaleItemDetail(completion: ((_ status: Bool) -> Void)? = nil){

        let params : [String:Any] =
            
            ["SellItemId":itemId as Any]
       
        showHud()
        WebServiceHelper.request(path: .saleItemDetail, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let dictResponse = rootModel.data as? [String:Any] {
                                self.objDetail = SaleItemDetailModel(fromDictionary: dictResponse)
                                
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                            completion?(false)
                        }
                    }
                }else{
                    completion?(false)
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
}

