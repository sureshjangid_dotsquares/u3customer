//
//  LoginViewController.swift
//  U3MAPPCustomer
//
//  Created by Dheeraj Kumar on 09/01/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import TwitterKit

class LoginViewController: U3BaseViewController {

    //MARK:- IBOutlets declaration
    @IBOutlet weak var emailTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var passwordTxtFld:CustomTextFieldAnimated!
    
    //MARK:- View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setGradientColor()
        
        if let nav = self.navigationController as? U3NavigationController {
            nav.clearNavBarColor()
        }
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        clearInformation()
    }
    
    func setupUI(){
        self.passwordTxtFld .setRightViewImageWithTapEvent(textFieldImg: #imageLiteral(resourceName: "password_show")) { (sender, imageview) in
            if self.passwordTxtFld.text != "" {
                sender.isSelected = !sender.isSelected
                if sender.isSelected {
                    imageview?.image = #imageLiteral(resourceName: "password_icon")
                    self.passwordTxtFld.isSecureTextEntry = false
                } else {
                    imageview?.image = #imageLiteral(resourceName: "password_show")
                    self.passwordTxtFld.isSecureTextEntry = true
                }
            }
        }
    }
    
    func clearInformation() {
        passwordTxtFld.text = ""
    }
    
    //MARK:- IBAction
    //Login Btn Press
    @IBAction func didTapLoginBtn(_ sender : UIButton){
        if self.ValidateEntries() == false {
            return
        }
        
        wsLoginUser()
    }
    
    // MARK:  Fb login Pressed
    @IBAction func didTapFbBtn(_ sender: UIButton) {
        
        FacebookRequest.fbRedirectToFetchData(nil, isShowIndicator: true, inViewController: self) { (isSuccess, response) in

            if(isSuccess) {
                self.wsSocialLoginUser(fbID: response?.fbID ?? "", email: response?.email ?? "", name: response?.title ?? "", profilePic: response?.profileImage ?? "")
            }else {
                if(response != nil) {
                    self.presentAlertWith(message: response?.message ?? "")
                }
            }
        }
        
    }
    
    // MARK:  Google login Pressed
    @IBAction func didTapGogoleBtn(_ sender: UIButton) {
        loginWithGoogle()
    }
    
    /**
    * Below method is called when app received data from google. This notification is posted from appdelegate.
    */
    @objc func googleDataReceived(notification:Notification)
    {
        if let dict = notification.userInfo as? [String:Any]
        {
            if let model=dict["model"] as? SocialModel
            {
                self.wsSocialLoginUser(gID: model.userId ?? "", email: model.email ?? "", name: model.fullName ?? "", profilePic: model.imageURL ?? "")
            }
        }
        
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK:  Linkedin Pressed
    @IBAction func didTapLinkedinBtn(_ sender: UIButton) {
        CreateProfileWithSocialMedia.shared.loginWithSocialMedia(type: SocialType.linkedin) { (result) in
            let model = SocialProfileUserModel(fromDictionary: result)
            
            self.wsSocialLoginUser(lID: model.id, email: model.email, name: model.fullName, profilePic: model.profilePicture)
            
        }

    }
    
    // MARK:  Twitter login Pressed
    @IBAction func didTapTwitterBtn(_ sender: UIButton) {
        self.loginWithTwitter()
    }
    
    //Don't Sign Up Btn Press
    @IBAction func didTapDontSignBtn(_ sender : UIButton){
        if let vc = storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as? SignupViewController{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didTapForgotBtn(_ sender : UIButton){
        if let vc = storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as? ForgotPasswordVC{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

}


extension LoginViewController {
    
    //MARK: - Validation
    func ValidateEntries() -> Bool {
        if emailTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter email".localized)
            return false
        }
        else if emailTxtFld.text?.trimString().isValidEmail() == false
        {
            Utility.ValidationError(errorMessage: "Please enter valid email".localized, controller: self)
            return false
        }
        else if passwordTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter password".localized)
            return false
        }
        
        return true
    }
}


//MARK:-API Calling
extension LoginViewController{
    func wsLoginUser(){
        let params : [String:Any] = [
            "Email":emailTxtFld.text?.trimString() ?? "",
            "Password":passwordTxtFld.text?.trimString() ?? "",
            "DeviceToken":AppManager.shared.deviceToken,
            ]
        
        showHud()
        WebServiceHelper.request(path: .customerLogin, method: .post, token: nil, headers: AppManager.shared.getHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                self.clearInformation()
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let dictResponse = rootModel.data as? [String:Any]{
                                self.handleResponse(dict: dictResponse)
                            }
                        }else if rootModel.responseCode == ErrorCode.validationFailed.rawValue{
                            
                            if let validationArray = rootModel.validationErrors as? [String] {
                                self.presentAlertWith(message: validationArray[0])
                            }
                            
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    
    
    func wsSocialLoginUser(gID : String = "", fbID : String = "", twID : String = "", lID : String = "", email : String = "", name : String = "", profilePic : String = ""){
        let params : [String:Any] = [
            "DeviceToken":AppManager.shared.deviceToken,
            "GoogleId": gID,
            "FacebookId":fbID,
            "TwitterId":twID,
            "LinkedinId":lID,
            "CustomerName":name,
            "email":email,
            "LogoProfilePic":profilePic
        ]
        
        showHud()
        WebServiceHelper.request(path: .socialLogin, method: .post, token: nil, headers: AppManager.shared.getHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                self.clearInformation()
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let dictResponse = rootModel.data as? [String:Any]{
                                self.handleResponse(dict: dictResponse)
                            }
                        }else if rootModel.responseCode == ErrorCode.validationFailed.rawValue{
                            
                            if let validationArray = rootModel.validationErrors as? [String] {
                                self.presentAlertWith(message: validationArray[0])
                            }
                            
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    func handleResponse(dict: [String:Any]) {
        let userModel = UserModel.init(fromDictionary: dict)
        AppManager.shared.userModel = userModel
        AppManager.shared.isLoggedIn = true
        AppManager.shared.hasRedeemedFreeBox = userModel.hasRedeemedFreeBox
        AppManager.shared.isRememberMe = true
        AppManager.shared.email = self.emailTxtFld.text?.trimString() ?? ""
        AppManager.shared.password = self.passwordTxtFld.text?.trimString() ?? ""
        
        AppManager.shared.isProfileCompleted = userModel.isProfileCompeled.boolValue
        
        if AppManager.shared.isProfileCompleted {
            APPDELEGATE.showHomeWindow()
        }else {
            AppManager.shared.redirectionOnCompleteProfile(viewController: self)
        }
    }
    
    
}

extension LoginViewController{
    
    func loginWithTwitter(){
        
        showHud()
        TWTRTwitter.sharedInstance().logIn(with: self) { (session, error) in
            dissmissHud()
            if (session != nil) {
                let username = session?.userName ?? "User name not found"
                print("signed in as \(username)");
                 let client = TWTRAPIClient.withCurrentUser()
                client.requestEmail { email, error in
                    if (email != nil) {
                        let model=SocialModel(email: email ?? "", userId: session?.userID ?? "0", idToken: "", fullName: session?.userName ?? "", givenName: "",imageURL:"",firstName:"",lastName:"")
                        
                        self.wsSocialLoginUser(twID: model.userId,email: model.email, name: model.fullName)
                        
                    } else {
                        print("error: \(error?.localizedDescription ??  "Unable to get login Email from twitter")");
                    }
                }
                
            } else {
                print("error: \(error?.localizedDescription ?? "Unable to login")");
//                self.presentAlertWith(message: error?.localizedDescription ?? "Unable to login")
            }
            
        }
        

       }
}
