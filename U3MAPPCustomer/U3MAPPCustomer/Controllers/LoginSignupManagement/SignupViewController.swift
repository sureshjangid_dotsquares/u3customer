//
//  SignupViewController.swift
//  U3MAPPCustomer
//
//  Created by Dheeraj Kumar on 09/01/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import SafariServices

class SignupViewController: U3BaseViewController,SFSafariViewControllerDelegate {

    //MARK:-IBOutlets declaration
    @IBOutlet weak var emailTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var passwordTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var confirmPasswordTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var nameTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var termsBtn:UIButton!
    
    //MARK:-View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        super.setGradientColor (angle: 155)
        super.backBarBtnPressed()
        if let nav = self.navigationController as? U3NavigationController {
            nav.clearNavBarColor()
        }
        
        self.passwordTxtFld .setRightViewImageWithTapEvent(textFieldImg: #imageLiteral(resourceName: "password_show")) { (sender, imageview) in
            if self.passwordTxtFld.text != "" {
                sender.isSelected = !sender.isSelected
                if sender.isSelected {
                    imageview?.image = #imageLiteral(resourceName: "password_icon")
                    self.passwordTxtFld.isSecureTextEntry = false
                } else {
                    imageview?.image = #imageLiteral(resourceName: "password_show")
                    self.passwordTxtFld.isSecureTextEntry = true
                }
            }
        }
        
        self.confirmPasswordTxtFld .setRightViewImageWithTapEvent(textFieldImg: #imageLiteral(resourceName: "password_show")) { (sender, imageview) in
            if self.confirmPasswordTxtFld.text != "" {
                sender.isSelected = !sender.isSelected
                if sender.isSelected {
                    imageview?.image = #imageLiteral(resourceName: "password_icon")
                    self.confirmPasswordTxtFld.isSecureTextEntry = false
                } else {
                    imageview?.image = #imageLiteral(resourceName: "password_show")
                    self.confirmPasswordTxtFld.isSecureTextEntry = true
                }
            }
        }
    }
    
    
    
    //MARK:- IBAction Methods
    @IBAction func didTapBackBtn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapTermsBtn(_ sender : UIButton){
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func didTapTermLink(_ sender : UIButton){
        let url = URL(string: termsURL)!
        let controller = SFSafariViewController(url: url)
        self.present(controller, animated: true, completion: nil)
        controller.delegate = self
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapSignUpBtn(_ sender : UIButton){
        if self.ValidateEntries() == false {
            return
        }
        self.wsSignupUser()
        
    }
    
    //Don't Sign In  Btn Press
    @IBAction func didTapDontSignBtn(_ sender : UIButton){
        if let vc = Storyboards.loginStoryboard.instantiateViewController(withIdentifier: "BusinessPopupVC") as? BusinessPopupVC {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
}


extension SignupViewController {
    
    //MARK: - Validation
    func ValidateEntries() -> Bool {
        if nameTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter name")
            return false
        }
        else if emailTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter email")
            return false
        }
        else if emailTxtFld.text?.trimString().isValidEmail() == false
        {
            Utility.ValidationError(errorMessage: "Please enter valid email", controller: self)
            return false
        }
        else if passwordTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter password")
            return false
        }
        else if passwordTxtFld.text!.trimString().count < 6{
            self.presentAlertWith(message: "Password should be minimum 6 characters")
            return false
        }
        else if passwordTxtFld?.text?.trimString() !=  confirmPasswordTxtFld?.text?.trimString()
        {
            Utility.ValidationError(errorMessage: "Confirm Password should be same as Password", controller: self)
            return false
        }
        else if termsBtn.isSelected == false
        {
            Utility.ValidationError(errorMessage: "Please select Terms of Use" , controller: self)
            
            return false
        }
        
        return true
    }
}



//MARK:-API Calling
extension SignupViewController{
    func wsSignupUser(){
        
        let params : [String:Any] = [
            "Email":emailTxtFld.text?.trimString() ?? "",
            "Password":passwordTxtFld.text?.trimString() ?? "",
            "CustomerName" :nameTxtFld.text!.trimString() as Any
        ]
        
        showHud()
        WebServiceHelper.request(path: .signup, method: .post, token: nil, headers: AppManager.shared.getHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            
                            self.presentAlertWith(message: rootModel.successMsg) {
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                            
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    
}
