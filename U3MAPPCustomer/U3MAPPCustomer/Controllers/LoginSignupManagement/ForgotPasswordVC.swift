//
//  ForgotPasswordVC.swift
//  U3MAPPCustomer
//
//  Created by Dheeraj Kumar on 10/01/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class ForgotPasswordVC: U3BaseViewController {

    //MARK:-IBOutlets declaration
    @IBOutlet weak var emailTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var btnSend:UIButton!
    
    //MARK:-View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backBarBtnPressed()
        
        if let nav = self.navigationController as? U3NavigationController {
            nav.clearNavBarColor()
        }

        setupUI()
    }
    
    // MARK:  setup UI
    func setupUI()
    {
        emailTxtFld.title.text = "Email".localized
        emailTxtFld.placeholder = "Enter email address".localized
        
        btnSend.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        btnSend.layer.shadowOffset = CGSize(width:0, height:5)
        btnSend.layer.shadowOpacity = 1.0
        btnSend.layer.shadowRadius = 5
        btnSend.layer.cornerRadius = 5
    }
    
    //MARK:-IBAction Methods
    @IBAction func didTapOnSend(_ sender: Any) {
        if self.ValidateEntries() == false {
            return
        }
        
        self.wsForgotPassword()
    }
  
}


extension ForgotPasswordVC {
    
    //MARK: - Validation
    func ValidateEntries() -> Bool {
        if emailTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter email".localized)
            return false
        }
        else if emailTxtFld.text?.trimString().isValidEmail() == false
        {
            Utility.ValidationError(errorMessage: "Please enter valid email".localized, controller: self)
            return false
        }
        
        return true
    }
}


//MARK:-API Calling
extension ForgotPasswordVC{
    func wsForgotPassword(){
        let params : [String:Any] = [
            "email":emailTxtFld.text?.trimString() ?? "" ]
        
        showHud()
        WebServiceHelper.request(path: .forgotPassword, method: .post, token: nil, headers: AppManager.shared.getHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if (rootModel.data as? Bool) != nil{
                                self.presentAlertWith(message: rootModel.successMsg) {
                                    self.navigationController?.popViewController(animated: true)
                                }
                               
                            }
                        }else if rootModel.responseCode == ErrorCode.validationFailed.rawValue{
                            
                            if let validationArray = rootModel.validationErrors as? [String] {
                                self.presentAlertWith(message: validationArray[0])
                            }
                            
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    
}
