//
//  ChangePasswordVC.swift
//  U3MAPPCustomer
//
//  Created by Dheeraj Kumar on 10/01/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class ChangePasswordVC: U3BaseViewController {

    //MARK:-IBOutlets declaration
    @IBOutlet weak var passwordTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var newPasswordTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var confirmNewPasswordTxtFld:CustomTextFieldAnimated!
    @IBOutlet weak var btnSend:UIButton!
    
    var isVisibleOldPassword:Bool = false
    var isVisibleNewPassword:Bool = false
    var isVisibleNewConfirmPassword:Bool = false
    
    //MARK:-View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backBarBtnPressed()
        
        setupUI()
        
    }
    
    func setupUI(){
        
        self.title = "Change Password".localized
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        self.passwordTxtFld .setRightViewImageWithTapEvent(textFieldImg: #imageLiteral(resourceName: "password_show")) { (sender, imageview) in
            if self.passwordTxtFld.text != "" {
                sender.isSelected = !sender.isSelected
                if sender.isSelected {
                    imageview?.image = #imageLiteral(resourceName: "password_icon")
                    self.passwordTxtFld.isSecureTextEntry = false
                } else {
                    imageview?.image = #imageLiteral(resourceName: "password_show")
                    self.passwordTxtFld.isSecureTextEntry = true
                }
            }
        }
        
        self.newPasswordTxtFld .setRightViewImageWithTapEvent(textFieldImg: #imageLiteral(resourceName: "password_show")) { (sender, imageview) in
            if self.newPasswordTxtFld.text != "" {
                sender.isSelected = !sender.isSelected
                if sender.isSelected {
                    imageview?.image = #imageLiteral(resourceName: "password_icon")
                    self.newPasswordTxtFld.isSecureTextEntry = false
                } else {
                    imageview?.image = #imageLiteral(resourceName: "password_show")
                    self.newPasswordTxtFld.isSecureTextEntry = true
                }
            }
        }
        
        self.confirmNewPasswordTxtFld .setRightViewImageWithTapEvent(textFieldImg: #imageLiteral(resourceName: "password_show")) { (sender, imageview) in
            if self.confirmNewPasswordTxtFld.text != "" {
                sender.isSelected = !sender.isSelected
                if sender.isSelected {
                    imageview?.image = #imageLiteral(resourceName: "password_icon")
                    self.confirmNewPasswordTxtFld.isSecureTextEntry = false
                } else {
                    imageview?.image = #imageLiteral(resourceName: "password_show")
                    self.confirmNewPasswordTxtFld.isSecureTextEntry = true
                }
            }
        }
        
        
        btnSend.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        btnSend.layer.shadowOffset = CGSize(width:0, height:5)
        btnSend.layer.shadowOpacity = 1.0
        btnSend.layer.shadowRadius = 5
        btnSend.layer.cornerRadius = 5
    }
    
    //MARK:-IBAction Methods 
    @IBAction func didTapSendBtn(_ sender : UIButton){
        if self.ValidateEntries() == false {
            return
        }
        self.wsChangePasword()
    }
}


extension ChangePasswordVC {
    
    //MARK: - Validation
    func ValidateEntries() -> Bool {
        if passwordTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter old password".localized)
            return false
        }
        else if newPasswordTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter new password".localized)
            return false
        }
        else if newPasswordTxtFld.text!.trimString().count < 6{
            self.presentAlertWith(message: "Password should be minimum 6 characters".localized)
            return false
        }
        else if confirmNewPasswordTxtFld.text?.trimString().isEmpty == true{
            self.presentAlertWith(message: "Please enter confirm new password".localized)
            return false
        }
        else if confirmNewPasswordTxtFld.text!.trimString().count < 6{
            self.presentAlertWith(message: "Confirm Password should be minimum 6 characters".localized)
            return false
        }
        else if newPasswordTxtFld?.text?.trimString() != confirmNewPasswordTxtFld?.text?.trimString()
        {
            Utility.ValidationError(errorMessage: "Confirm Password should be same as New Password".localized, controller: self)
            return false
        }
        
        return true
    }
}


//MARK:-API Calling
extension ChangePasswordVC{
    
    //MARK: Get Complete Profile
    func wsChangePasword(){
        let params : [String:Any] = [
            "OldPassword": passwordTxtFld.text?.trimString() as Any,
            "NewPassword": newPasswordTxtFld.text?.trimString() as Any,
            "ConfirmPassword": confirmNewPasswordTxtFld.text?.trimString() as Any]
        showHud()
        WebServiceHelper.request(path: .changePassword, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if (rootModel.data as? Bool) != nil{
                                self.presentAlertWith(message: rootModel.successMsg) {
                                    AppManager.shared.showRootView()
                                }
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
}
