//
//  BusinessPopupVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 24/04/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import SafariServices

class BusinessPopupVC: UIViewController,UITextViewDelegate,SFSafariViewControllerDelegate {

    var linkUrl = "http://3usystem.projectstatus.in/"
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func didTapClose(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func didTapOnLink(_ sender: Any) {
        let strUrl : String = self.linkUrl.validURL()
        let url = NSURL(string: strUrl)! as URL
        let controller = SFSafariViewController(url: url)
        self.present(controller, animated: true, completion: nil)
        controller.delegate = self
    }

}
