     //
     //  SideBarVC.swift
     //  Inside Play
     //
     //  Created by Ganesh on 26/02/20.
     //  Copyright © 2020 ds. All rights reserved.
     //
     
     import UIKit
     import SWRevealViewController
     import Kingfisher
     
     class SideBarVC: UIViewController {
        @IBOutlet weak var consTop: NSLayoutConstraint!
        @IBOutlet var tblVW : UITableView!
        @IBOutlet var vwHeader : UIView!
        @IBOutlet var imgBG : UIImageView!
        @IBOutlet var imgProfile : UIImageView!
        @IBOutlet var lblVersionNo : UILabel!
        var selectedIndexpath : NSMutableArray!
        var arrOfSideBarContent : NSMutableArray!
        var seletedIndex = -1
        
        override func viewDidLoad() {
            super.viewDidLoad()
            selectedIndexpath = NSMutableArray()
            if let path = Bundle.main.path(forResource: "Sidemenu", ofType: "plist") {
                self.arrOfSideBarContent = NSMutableArray(contentsOfFile: path)
            }
            
            lblVersionNo.text = AppManager.shared.userModel.customerName
            
            self.revealViewController().rearViewRevealWidth = self.view.frame.width-55
            self.consTop.constant =  self.view.height > 750 ? -44:-20
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.revealViewController().view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.revealViewController().tapGestureRecognizer()
            self.revealViewController().frontViewController.view.isUserInteractionEnabled = false
            
            let url = URL(string: AppManager.shared.userModel.logoProfilePic)
            imgProfile.kf.setImage(with: url, placeholder: Images.profilePlaceholder.icon)
            
            tblVW.reloadData()
           // drawRectangle()
        }
        
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.isUserInteractionEnabled = true
        }
        
        func drawRectangle() {
            let renderer = UIGraphicsImageRenderer(size: CGSize(width: imgBG.width, height: imgBG.height))

            let img = renderer.image { ctx in
                let rectangle = CGRect(x: 10, y: 10, width: imgBG.width, height: imgBG.height)

                // 4
                ctx.cgContext.setFillColor(UIColor.yellow.cgColor)
               // ctx.cgContext.setStrokeColor(UIColor.gray.cgColor)
               // ctx.cgContext.setLineWidth(20)

                // 5
                ctx.cgContext.addRect(rectangle)
                ctx.cgContext.drawPath(using: .fillStroke)
            }

            imgBG.image = img
        }
     }
     
    
     
     
     extension SideBarVC: UITableViewMethods {
        // MARK: - TableView Delegate
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.arrOfSideBarContent?.count ?? 0
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            let dict = arrOfSideBarContent?.object(at: indexPath.row) as! NSDictionary
            let title = dict.object(forKey: "Title") as? String ?? ""
            let cell = tableView.dequeueReusableCell(withIdentifier: "SidBarCell", for: indexPath) as! SidBarCell
            cell.lblMenuname.text = title
            cell.imgIcon.image = UIImage.init(named: dict.object(forKey: "ImgTitle") as? String ?? "")
            if indexPath.row ==  0{
                // cell.imgIcon.image = Utility.getSyncNotication ?  #imageLiteral(resourceName: "sync"):#imageLiteral(resourceName: "Sync_data")
            }
            return cell
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            return  812*0.064 //self.view.frame.height ==
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
            if indexPath.row == arrOfSideBarContent.count-1{
                self.logoutPressed()
            }
            else{
                
                self.seletedIndex = indexPath.row
                guard  let dic = arrOfSideBarContent?.object(at: indexPath.row) as? NSDictionary else{return}
                guard  let sugue = dic["Segue"] as? String else{return}
                
                if self.canPerformSegue(withIdentifier: sugue) {
                    self.performSegue(withIdentifier: sugue, sender: self)
                }else {
                    print("segue error")
                }
            }
            
        }
        
     }
