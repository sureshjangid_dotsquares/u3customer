//
//  SidBarCell.swift
//  Steel Cap
//
//  Created by Ganesh on 26/02/20.
//  Copyright © 2020 ds. All rights reserved.
 
 

import UIKit

class SidBarCell: UITableViewCell {

    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblMenuname: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
