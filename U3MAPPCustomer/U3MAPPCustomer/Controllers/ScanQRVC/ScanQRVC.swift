//
//  ScanQRVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 24/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import AVFoundation
import SafariServices

class ScanQRVC: U3BaseViewController, SFSafariViewControllerDelegate {

    
    //MARK:-Variables
    var latitude = PCLocationManager.shared().myLocation.latitude , longitude = PCLocationManager.shared().myLocation.longitude
    private var captureSession: AVCaptureSession!
    private var previewLayer: AVCaptureVideoPreviewLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Scan QR Code".localized
        setupMenuBtn()
        captureSession = AVCaptureSession()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if AVCaptureDevice.authorizationStatus(for: .video) == AVAuthorizationStatus.authorized {
            
        }else {
            AVCaptureDevice.requestAccess(for: .video) { (granted:Bool) in
                if granted == false {
                    // User rejected
                    self.presentAlertWith(message: "Please allow camera access from settings.".localized, oKTitle: "Ok".localized, okaction: {
                        
                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                            return
                        }
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                    print("Settings opened: \(success)") // Prints true
                                })
                            } else {
                                // Fallback on earlier versions
                            }
                        }
                        
                    }) {}
                    
                }
            }
        }
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        // Initialize the captureSession object.
        captureSession = AVCaptureSession()
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = self.view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        previewLayer.connection?.videoOrientation = .portrait
        self.view.layer.addSublayer(previewLayer)
        
        // Start video capture.
        captureSession?.startRunning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopScanning()
    }
    
    private func startScanning(){
        DispatchQueue.main.async {
            guard let _captureSession = self.captureSession else {return}
            _captureSession.startRunning()
        }
        
    }
    
    private func stopScanning() {
        DispatchQueue.main.async {
            guard let _captureSession = self.captureSession else {return}
            _captureSession.stopRunning()
        }
        
    }
    
    private func failed() {
            //        let ac = UIAlertController(title: "Scanning not supported", message: Constant.ValidationMessages.qrFailed, preferredStyle: .alert)
            //        ac.addAction(UIAlertAction(title: "OK", style: .default))
            //        present(ac, animated: true)
            captureSession = nil
        }
        
        func addQrCodeImage() {
            let image = UIImage(named: "qrCode")?.cgImage
            let myLayer = CALayer()
            myLayer.frame = CGRect(origin:CGPoint.zero, size: CGSize(width: 100, height: 100))
            myLayer.contents = image
            myLayer.position = view.center
            
            self.view.layer.insertSublayer(myLayer, above: previewLayer)
            self.view.layer.addSublayer(myLayer)
        }
        
        public func ValidateString(stringValue: String) -> (Bool, String) {
    //        let str = "3UAPP_AC4D5T_2_95"
            let array = stringValue.components(separatedBy: "_")
            
            guard array.count > 0 else {
                return (false, "0")
                
            }
            guard array[0] == "3UAPP" && array[1] == "AC4D5T" else {
                return (false, "0")
                
            }
            if array[2] == "1" {
                //Customer
                return (false, array[3])

            }else if array[2] == "2" {
                //Advertiser
                print("advertise here")
                
                print("userid\(array[3])")
                
                return (true, array[3])
                
            }
            return (false, "0")
        }

}


extension ScanQRVC : AVCaptureMetadataOutputObjectsDelegate{
    //MARK: - AVCaptureMetadataOutputObjectsDelegate -
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if metadataObjects.count == 0 {
            return
        }
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            stopScanning()
            
            let StringData = self.ValidateString(stringValue: stringValue)
            
            if StringData.0 {
                self.validateQR(userId: Int(StringData.1)!, code: "")
            }else {
                self.presentAlertWith(message: "QR Code Validation Error".localized) {
                    self.dismiss(animated: true)
                    self.startScanning()

                }
            }
        }
        
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}


//MARK: Webservice CALL
extension ScanQRVC {
    private func validateQR(userId:Int,code:String){
        
        let params : [String:Any] = [
            "BoxAdvertisementId": userId,
            "Latitude":self.latitude,
            "Longitude":self.longitude,
        ]
        
        showHud()
        WebServiceHelper.request(path: .scanAdvertisementQRCode, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            DispatchQueue.main.async {
                
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let dictResponse = rootModel.data as? [String:Any] {
                                if let weblink = dictResponse["websiteAppLink"] as? String {
                                    let strUrl : String = weblink
                                    let url = URL(string: strUrl.validURL())!
                                    let controller = SFSafariViewController(url: url)
                                    self.present(controller, animated: true, completion: nil)
                                    controller.delegate = self
                                }
                            }
                        }else if rootModel.responseCode == ErrorCode.validationFailed.rawValue{
                            
                            if let validationArray = rootModel.validationErrors as? [String] {
                                self.presentAlertWith(message: validationArray[0])
                            }
                            
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg) {
                                self.startScanning()
                            }
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized) {
                            self.startScanning()
                        }
                    }
                }
                
            }
        }
    }
}
