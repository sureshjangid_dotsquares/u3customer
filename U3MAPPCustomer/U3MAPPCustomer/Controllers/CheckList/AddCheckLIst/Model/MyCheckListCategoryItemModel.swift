//
//	CheckListCategoryItem.swift
//
//	Create by Abhishek Jangid on 2/3/2020
//	Copyright © 2020. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class MyCheckListCategoryItemModel : NSObject, NSCoding{

	var checkListLogo : String!
	var checkListLogoId : Int!
	var isChecked : Bool!
	var itemID : Int!
	var itemName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		checkListLogo = dictionary["checkListLogo"] as? String ?? ""
		checkListLogoId = dictionary["checkListLogoId"] as? Int ?? 0
		isChecked = dictionary["isChecked"] as? Bool ?? false
		itemID = dictionary["itemID"] as? Int ?? 0
		itemName = dictionary["itemName"] as? String ?? ""
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if checkListLogo != nil{
			dictionary["checkListLogo"] = checkListLogo
		}
		if checkListLogoId != nil{
			dictionary["checkListLogoId"] = checkListLogoId
		}
		if isChecked != nil{
			dictionary["isChecked"] = isChecked
		}
		if itemID != nil{
			dictionary["itemID"] = itemID
		}
		if itemName != nil{
			dictionary["itemName"] = itemName
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         checkListLogo = aDecoder.decodeObject(forKey: "checkListLogo") as? String
         checkListLogoId = aDecoder.decodeObject(forKey: "checkListLogoId") as? Int
         isChecked = aDecoder.decodeObject(forKey: "isChecked") as? Bool
         itemID = aDecoder.decodeObject(forKey: "itemID") as? Int
         itemName = aDecoder.decodeObject(forKey: "itemName") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if checkListLogo != nil{
			aCoder.encode(checkListLogo, forKey: "checkListLogo")
		}
		if checkListLogoId != nil{
			aCoder.encode(checkListLogoId, forKey: "checkListLogoId")
		}
		if isChecked != nil{
			aCoder.encode(isChecked, forKey: "isChecked")
		}
		if itemID != nil{
			aCoder.encode(itemID, forKey: "itemID")
		}
		if itemName != nil{
			aCoder.encode(itemName, forKey: "itemName")
		}

	}

}
