//
//	CustomerResponseToDo.swift
//
//	Create by Abhishek Jangid on 2/3/2020
//	Copyright © 2020. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class MyCheckListCustomerResponseToDoModel : NSObject, NSCoding{

	var item : String!
	var itemId : Int!
    var isChecked : Bool!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		item = dictionary["item"] as? String ?? ""
		itemId = dictionary["itemId"] as? Int ?? 0
        isChecked = dictionary["isChecked"] as? Bool ?? false
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if item != nil{
			dictionary["item"] = item
		}
		if itemId != nil{
			dictionary["itemId"] = itemId
		}
        if isChecked != nil{
            dictionary["isChecked"] = isChecked
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        item = aDecoder.decodeObject(forKey: "item") as? String
        itemId = aDecoder.decodeObject(forKey: "itemId") as? Int
        isChecked = aDecoder.decodeObject(forKey: "isChecked") as? Bool
        
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if item != nil{
			aCoder.encode(item, forKey: "item")
		}
		if itemId != nil{
			aCoder.encode(itemId, forKey: "itemId")
		}
        if isChecked != nil{
            aCoder.encode(isChecked, forKey: "isChecked")
        }

	}

}
