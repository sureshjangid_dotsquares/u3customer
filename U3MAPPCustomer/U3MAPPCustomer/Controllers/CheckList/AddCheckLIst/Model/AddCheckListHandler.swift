//
//  AddCheckListHandler.swift
//  U3MAPPCustomer
//
//  Created by Jagdish Jangir on 29/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation


class AddCheckListHandler {
    
    private var checkListData:AddCheckList?
    private(set) var allCategories:[MyCheckListCategoryModel] = []
    
    
    
    init() {
        self.checkListData = AddCheckList()
    }
    
    init(data:[String:Any]) {
        self.checkListData = AddCheckList.init(data:data)
         for move in self.checkListData!.data.masterChecklistItems{
            self.allCategories.append(contentsOf:move.checkListCategory)
        }
    }
    
    
    var toDoList:[MyCheckListCustomerResponseToDoModel] {
        return self.checkListData!.data.customerResponseToDo
    }
    
    
    
    
   
    
    
  func moveNameOfCategory(category:MyCheckListCategoryModel)->String{
    for move in self.checkListData!.data.masterChecklistItems{
               for cat in move.checkListCategory {
                   if cat === category {
                       return move.typeofMove
                   }
               }
           }
           return ""
       }
    
  
}
