//
//	CheckListCategory.swift
//
//	Create by Abhishek Jangid on 2/3/2020
//	Copyright © 2020. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class MyCheckListCategoryModel : NSObject, NSCoding{
    
    var checkListCategoryItem : [MyCheckListCategoryItemModel]!
    var checkListCategoryName : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        checkListCategoryItem = [MyCheckListCategoryItemModel]()
        if let checkListCategoryItemArray = dictionary["checkListCategoryItem"] as? [[String:Any]]{
            for dic in checkListCategoryItemArray{
                let value = MyCheckListCategoryItemModel(fromDictionary: dic)
                checkListCategoryItem.append(value)
            }
        }
        checkListCategoryName = dictionary["checkListCategoryName"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if checkListCategoryItem != nil{
            var dictionaryElements = [[String:Any]]()
            for checkListCategoryItemElement in checkListCategoryItem {
                dictionaryElements.append(checkListCategoryItemElement.toDictionary())
            }
            dictionary["checkListCategoryItem"] = dictionaryElements
        }
        if checkListCategoryName != nil{
            dictionary["checkListCategoryName"] = checkListCategoryName
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        checkListCategoryItem = aDecoder.decodeObject(forKey :"checkListCategoryItem") as? [MyCheckListCategoryItemModel]
        checkListCategoryName = aDecoder.decodeObject(forKey: "checkListCategoryName") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if checkListCategoryItem != nil{
            aCoder.encode(checkListCategoryItem, forKey: "checkListCategoryItem")
        }
        if checkListCategoryName != nil{
            aCoder.encode(checkListCategoryName, forKey: "checkListCategoryName")
        }
        
    }
    
}
