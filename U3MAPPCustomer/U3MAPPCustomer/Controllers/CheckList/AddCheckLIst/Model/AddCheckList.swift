//
//  AddCheckList.swift
//  U3MAPPCustomer
//
//  Created by Jagdish Jangir on 03/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation

class AddCheckList:NSObject {
    
    var data:MyCheckListModel
    
    init(data:[String:Any]) {
        self.data = MyCheckListModel.init(fromDictionary:data)
    }
    
    override init() {
        self.data = MyCheckListModel.init(fromDictionary:[:])
    }
    
}
