//
//	Data.swift
//
//	Create by Abhishek Jangid on 2/3/2020
//	Copyright © 2020. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class MyCheckListModel : NSObject, NSCoding{

	var customerResponseToDo : [MyCheckListCustomerResponseToDoModel]!
	var masterChecklistItems : [MyCheckListMasterChecklistItem]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		customerResponseToDo = [MyCheckListCustomerResponseToDoModel]()
		if let customerResponseToDoArray = dictionary["customerResponseToDo"] as? [[String:Any]]{
			for dic in customerResponseToDoArray{
				let value = MyCheckListCustomerResponseToDoModel(fromDictionary: dic)
				customerResponseToDo.append(value)
			}
		}
		masterChecklistItems = [MyCheckListMasterChecklistItem]()
		if let masterChecklistItemsArray = dictionary["masterChecklistItems"] as? [[String:Any]]{
			for dic in masterChecklistItemsArray{
				let value = MyCheckListMasterChecklistItem(fromDictionary: dic)
				masterChecklistItems.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if customerResponseToDo != nil{
			var dictionaryElements = [[String:Any]]()
			for customerResponseToDoElement in customerResponseToDo {
				dictionaryElements.append(customerResponseToDoElement.toDictionary())
			}
			dictionary["customerResponseToDo"] = dictionaryElements
		}
		if masterChecklistItems != nil{
			var dictionaryElements = [[String:Any]]()
			for masterChecklistItemsElement in masterChecklistItems {
				dictionaryElements.append(masterChecklistItemsElement.toDictionary())
			}
			dictionary["masterChecklistItems"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         customerResponseToDo = aDecoder.decodeObject(forKey :"customerResponseToDo") as? [MyCheckListCustomerResponseToDoModel]
          masterChecklistItems = aDecoder.decodeObject(forKey :"masterChecklistItems") as? [MyCheckListMasterChecklistItem]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if customerResponseToDo != nil{
			aCoder.encode(customerResponseToDo, forKey: "customerResponseToDo")
		}
		if masterChecklistItems != nil{
			aCoder.encode(masterChecklistItems, forKey: "masterChecklistItems")
		}

	}

}
