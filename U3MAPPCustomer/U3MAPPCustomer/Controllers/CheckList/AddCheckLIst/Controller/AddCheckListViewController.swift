//
//  CheckListViewController.swift
//  U3MAPPCustomer
//
//  Created by Jagdish Jangir on 28/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class AddCheckListViewController: U3BaseViewController {
    
    @IBOutlet weak var tableView_checkList: UITableView!
    @IBOutlet weak var typeOfMove: UILabel!
    @IBOutlet weak var buttonAdd: UIButton!
    
    
    private var checkListHander:CheckListHandler?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        checkListHander = CheckListHandler()
        
        self.title = "Add Checklist".localized
        if let nav = self.navigationController as? U3NavigationController {
            nav.hideNavBottomLine()
        }
        
        super.backBarBtnPressed()
        
        getMasterCheckListData()
        
        typeOfMove.text = ""
        
        tableView_checkList.separatorStyle = .none
        tableView_checkList.showsVerticalScrollIndicator = false
        tableView_checkList.sectionHeaderHeight = UITableView.automaticDimension
        tableView_checkList.estimatedSectionHeaderHeight = 100
        tableView_checkList.estimatedRowHeight = 100
        tableView_checkList.rowHeight = UITableView.automaticDimension
        
        buttonAdd.isHidden = true
        tableView_checkList.dataSource = self
        tableView_checkList.delegate = self
        
        
        tableView_checkList.register(CheckListHeaderXib.nib, forHeaderFooterViewReuseIdentifier: CheckListHeaderXib.headerIdentifier)
        
        
        buttonAdd.setTitle("ADD".localized, for:.normal)
        buttonAdd.setTitleColor(UIColor.white, for:.normal)
        buttonAdd.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        buttonAdd.layer.shadowOffset = CGSize(width:0, height:5)
        buttonAdd.layer.shadowOpacity = 1.0
        buttonAdd.layer.shadowRadius = 5
        buttonAdd.layer.cornerRadius = 5.0
        buttonAdd.backgroundColor = UIColor(hexString:"#2DCBEB")
        
    }
    
    
    func getMasterCheckListData(){
        
        showHud()
        let headers = AppManager.shared.getLoggedInHeaders()
        //        headers["AuthorizationToken"] = "YZX2VYHHPM"
        //        headers["UserId"] = "10"
        
        WebServiceHelper.request(path: .masterCheckList, method: .post, token: nil, headers: headers, parameters: [:]) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            
                            if let dataList = rootModel.data as? [[String:Any]] {
                                self.checkListHander = CheckListHandler.init(data: dataList)
                                self.tableView_checkList.reloadData()
                                self.buttonAdd.isHidden = false
                                self.updateCategoryName()
                            }
                            
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    
    
    
    
    @IBAction func actionButtonAdd(_ sender: UIButton) {
        showHud()
        let headers = AppManager.shared.getLoggedInHeaders()
        
        WebServiceHelper.request(path: .addMyCheckList, method: .post, token: nil, headers: headers, parameters: self.checkListHander!.toDictionary()) { (response, error, isSuccess) in
            dissmissHud()
            DispatchQueue.main.async {
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let percentage = rootModel.data as? Int {
                                AppManager.shared.checkListPercentage = percentage
                            }
                            self.presentAlertWith(message:rootModel.successMsg) {
                                self.navigationController?.popViewController(animated:true)
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
}


extension AddCheckListViewController:UIScrollViewDelegate {
    
    
    func updateCategoryName(){
        if let topSection = self.tableView_checkList.indexPathsForVisibleRows?.first?.section {
            if checkListHander!.allCategories.count > topSection{
                typeOfMove.text = checkListHander!.moveNameOfCategory(category:checkListHander!.allCategories[topSection])
            }else{
                typeOfMove.text = ""
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateCategoryName()
    }
}

extension AddCheckListViewController:UITableViewMethods {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if checkListHander!.allCategories.count > 0  {
            return checkListHander!.allCategories.count + 1
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == checkListHander!.allCategories.count {
            return 1 + checkListHander!.toDoList.count
        }
        
        return checkListHander!.allCategories[section].checkListCategoryItem.count
    }
    
    
    func showAddNewTaskPopup(){
        if let vc = self.storyboard?.instantiateViewController(withIdentifier:"AddNewTaskVC") as? AddNewTaskVC{
            vc.taskText = {input in
                if !input.isEmpty {
                    let newItem = MyCheckListCustomerResponseToDoModel.init(fromDictionary:["item":input])
                    self.checkListHander!.toDoList.append(newItem)
                    self.tableView_checkList.reloadData()
                    let lastSection = self.tableView_checkList.numberOfSections-1
                    let lastRow = self.tableView_checkList.numberOfRows(inSection:lastSection)-1
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        self.tableView_checkList.scrollToRow(at:IndexPath.init(row:lastRow, section:lastSection), at: .bottom, animated:true)
                    }
                }
            }
            self.present(vc, animated: true)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == checkListHander!.allCategories.count {
            if indexPath.row == 0 {
                let cellIdentifier = CheckListAddToDoItem.cellIdentifier
                let checkCell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier, for: indexPath) as! CheckListAddToDoItem
                checkCell.addNewTime = {
                    self.showAddNewTaskPopup()
                }
                return checkCell
            }
            let cellIdentifier = CheckListToDoItemTableViewCell.cellIdentifier
            let checkCell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier, for: indexPath) as! CheckListToDoItemTableViewCell
            checkCell.cellData = checkListHander!.toDoList[indexPath.row-1]
            checkCell.checkButton.isHidden = true
            checkCell.deleteAction = {item in
                self.checkListHander!.deleteToDoItem(item:item)
                tableView.reloadData()
            }
            return checkCell
            
        }
        
        let cellIdentifier = AddCheckListTableViewCell.cellIdentifier
        let checkCell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier, for: indexPath) as! AddCheckListTableViewCell
        checkCell.cellData = checkListHander!.allCategories[indexPath.section].checkListCategoryItem[indexPath.row]
        return checkCell
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == checkListHander!.allCategories.count {
            return 0
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == checkListHander!.allCategories.count{
            return nil
        }
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CheckListHeaderXib.headerIdentifier) as? CheckListHeaderXib else {
            assertionFailure("Failed to load - CheckListHeaderXib")
            return nil
        }
        headerView.contentView.backgroundColor = UIColor.white
        headerView.headerText.numberOfLines = 0
        headerView.headerText.text = checkListHander!.allCategories[section].checkListCategoryName
        return headerView
    }
    
}
