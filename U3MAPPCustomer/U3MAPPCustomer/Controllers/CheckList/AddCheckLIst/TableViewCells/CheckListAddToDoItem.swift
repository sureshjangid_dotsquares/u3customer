//
//  CheckListAddToDoItem.swift
//  U3MAPPCustomer
//
//  Created by Jagdish Jangir on 28/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class CheckListAddToDoItem: UITableViewCell {
    
    static let cellIdentifier = "checkListAddToDoItem"
    
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var buttonAdd: UIButton!
    
    @IBOutlet weak var titleText: UILabel!
    
    
    var addNewTime:(()->())?
    
    @IBAction func actionAddToDoItem(_ sender: UIButton){
        addNewTime?()
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        containerView.backgroundColor = UIColor.white
        titleText.text = "Add To-Do items".localized
        
        buttonAdd.setTitle("+ Add".localized, for:.normal)
        
        buttonAdd.setTitleColor(UIColor.white, for:.normal)
        buttonAdd.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        buttonAdd.layer.shadowOffset = CGSize(width:0, height:5)
        buttonAdd.layer.shadowOpacity = 1.0
        buttonAdd.layer.shadowRadius = 5
        buttonAdd.layer.cornerRadius = buttonAdd.bounds.height/2
        buttonAdd.backgroundColor = UIColor(hexString:"#2DCBEB")
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
}
