//
//  CheckListToDoItemTableViewCell.swift
//  U3MAPPCustomer
//
//  Created by Jagdish Jangir on 28/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class CheckListToDoItemTableViewCell: UITableViewCell {
    
    static let cellIdentifier = "checkListToDoItem"
    
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var buttonDelete: UIButton!
    @IBOutlet weak var checkButton: UIButton!
    
    @IBOutlet weak var titleText: UILabel!
    
    var itemSelectionChanged:((MyCheckListCustomerResponseToDoModel)->())?
    var deleteAction: ((MyCheckListCustomerResponseToDoModel)->())?
    
    @IBAction func actionDeleteToDoItem(_ sender: UIButton){
        guard let _cellData = cellData else {return}
        deleteAction?(_cellData)
    }
    
    var cellData:MyCheckListCustomerResponseToDoModel? {
        didSet{
            updateContent()
        }
    }
    
    func toggleSelection(){
         guard let _cellData = cellData else {return}
        _cellData.isChecked = !_cellData.isChecked
        updateContent()
    }
    
    
    func updateContent(){
//        titleText.text = cellData?.item
        if cellData!.isChecked {
            self.titleText.attributedText = self.strikeThroughText(text:cellData!.item)
            checkButton.setImage(UIImage.init(named:"checklist_icon"), for:.normal)
        }else{
            self.titleText.attributedText = NSAttributedString.init(string: cellData!.item)
            checkButton.setImage(UIImage.init(named:"checklist_unicon"), for:.normal)
        }
    }
    
    private func strikeThroughText(text:String?)->NSAttributedString?{
        guard let _text = text else {return nil}
        let attributedString = NSMutableAttributedString(string:_text)
        attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributedString.length))
        return attributedString
    }
    
    @IBAction func actionCheck(_ sender: UIButton){
            
            guard let _cellData = cellData else {return}
            itemSelectionChanged?(_cellData)
    //        cellData?.isChecked = !cellData!.isChecked
    //        self.updateContent()
        }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        containerView.backgroundColor = UIColor.white
        containerView.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.05).cgColor
        containerView.layer.shadowOffset = CGSize(width:0, height:9)
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowRadius = 10
        containerView.layer.cornerRadius = 5.0
        
        titleText.numberOfLines = 0
        titleText.text = ""
        buttonDelete.tintColor = UIColor.black
        buttonDelete.setTitle(nil, for:.normal)
        buttonDelete.setImage(UIImage.init(named:"cross"), for:.normal)
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
}
