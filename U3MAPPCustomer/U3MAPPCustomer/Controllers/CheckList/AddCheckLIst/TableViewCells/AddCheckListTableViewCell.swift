//
//  AddCheckListTableViewCell.swift
//  U3MAPPCustomer
//
//  Created by Jagdish Jangir on 28/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class AddCheckListTableViewCell: UITableViewCell {
    
    static let cellIdentifier = "AddCheckListTableViewCell"
    
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var checkDescription: UILabel!
    
    @IBOutlet weak var bottomBar: UIView!
    
    var cellData:CheckListCategoryItemModel? {
        didSet {
            updateContent()
        }
    }
    
    
    private func updateContent(){
        guard let _cellData = cellData else {return}
        self.checkDescription.attributedText = NSAttributedString.init(string: _cellData.itemName)
        if _cellData.isChecked {
           // self.checkDescription.attributedText = self.strikeThroughText(text:_cellData.itemName)
            checkButton.setImage(UIImage.init(named:"checklist_icon"), for:.normal)
        }else{
          //  self.checkDescription.attributedText = NSAttributedString.init(string: _cellData.itemName)
            checkButton.setImage(UIImage.init(named:"checklist_unicon"), for:.normal)
        }
    }
    
    private func strikeThroughText(text:String?)->NSAttributedString?{
        guard let _text = text else {return nil}
        let attributedString = NSMutableAttributedString(string:_text)
        attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributedString.length))
        return attributedString
    }
    
    
    @IBAction func actionCheck(_ sender: UIButton){
        cellData!.isChecked =  !cellData!.isChecked
        self.updateContent()
    }
    
    @IBAction func actionLogo(_ sender: UIButton) {
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.backgroundColor = UIColor.white
        self.bottomBar.backgroundColor = UIColor(hexString:"#ECECEC")
        
        actionButton.setImage(UIImage.init(named:"logo_icon"), for:.normal)
        actionButton.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        actionButton.layer.shadowOffset = CGSize(width:0, height:5)
        actionButton.layer.shadowOpacity = 1.0
        actionButton.layer.shadowRadius = 5
        actionButton.layer.cornerRadius = 4.0
        actionButton.backgroundColor = UIColor.white
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
}
