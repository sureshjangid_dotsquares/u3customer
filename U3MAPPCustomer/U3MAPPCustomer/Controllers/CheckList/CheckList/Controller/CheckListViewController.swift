//
//  CheckListViewController.swift
//  U3MAPPCustomer
//
//  Created by Jagdish Jangir on 27/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class CheckListViewController: U3BaseViewController {
    
    @IBOutlet weak var tableView_checkList: UITableView!
    
    
    @IBOutlet weak var typeOfMove: UILabel!
    
    @IBOutlet weak var placeHolderView: UIView!
    
    @IBOutlet weak var typeOfMoveHolder: UIView!
    
    private var checkListHander:AddCheckListHandler?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        super.setupMenuBtn()
        self.title = "Checklist".localized
        
        if let nav = self.navigationController as? U3NavigationController {
            nav.hideNavBottomLine()
        }
        
        checkListHander = AddCheckListHandler()
        
        
        
        
        typeOfMove.text = ""
        
        
        tableView_checkList.separatorStyle = .none
        tableView_checkList.showsVerticalScrollIndicator = false
        tableView_checkList.sectionHeaderHeight = UITableView.automaticDimension
        tableView_checkList.estimatedSectionHeaderHeight = 100
        tableView_checkList.estimatedRowHeight = 100
        tableView_checkList.rowHeight = UITableView.automaticDimension
        tableView_checkList.dataSource = self
        tableView_checkList.delegate = self
        
        tableView_checkList.register(CheckListHeaderXib.nib, forHeaderFooterViewReuseIdentifier: CheckListHeaderXib.headerIdentifier)
        addCreateNewRightBarButton()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getMyCheckListData()
    }
    
    func addCreateNewRightBarButton() {
        let rightBarBtn : UIBarButtonItem = UIBarButtonItem(image: Images.plus.icon, style: .plain, target: self, action: #selector(self.rightBtnAction(_:)))
        rightBarBtn.tintColor = UIColor.black
        self.navigationItem.rightBarButtonItem = rightBarBtn
    }
    
    override func rightBtnAction(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier:"addChecklistSegueIdentifier", sender:nil)
    }
    
    @IBAction func actionCreateNew(_ sender: UIButton) {
        self.performSegue(withIdentifier:"addChecklistSegueIdentifier", sender:nil)
    }
    
    
    
    private func showHidePlaceholder(show:Bool){
        self.placeHolderView.isHidden = !show
        self.tableView_checkList.isHidden = show
        self.typeOfMoveHolder.isHidden = show
    }
    
    
    
    func invertItemSelection(item:MyCheckListCategoryItemModel,done:@escaping ()->()){
        
        showHud()
        let headers = AppManager.shared.getLoggedInHeaders()
        //        headers["AuthorizationToken"] = "YZX2VYHHPM"
        //        headers["UserId"] = "10"
        
        var param:[String:Any] = [:]
        param["ItemId"] = item.itemID
        param["IsChecked"] = !item.isChecked
        
        WebServiceHelper.request(path: .changeChecklistItemStatus, method: .post, token: nil, headers: headers, parameters: param) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let percentage = rootModel.data as? Int {
                                AppManager.shared.checkListPercentage = percentage
                            }
                            if rootModel.successMsg.isEmpty {
                                done()
                            }else {
                                self.presentAlertWith(message: rootModel.successMsg)
                                done()
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    func invertToDoItemSelection(item:MyCheckListCustomerResponseToDoModel,done:@escaping ()->()){
        
        showHud()
        let headers = AppManager.shared.getLoggedInHeaders()
        //        headers["AuthorizationToken"] = "YZX2VYHHPM"
        //        headers["UserId"] = "10"
        
        var param:[String:Any] = [:]
        param["ItemId"] = item.itemId
        param["IsChecked"] = !item.isChecked
        
        WebServiceHelper.request(path: .changeToDoChecklistItemStatus, method: .post, token: nil, headers: headers, parameters: param) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let percentage = rootModel.data as? Int {
                                AppManager.shared.checkListPercentage = percentage
                            }
                            if rootModel.successMsg.isEmpty {
                                done()
                            }else {
                                self.presentAlertWith(message: rootModel.successMsg)
                                done()
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    func getMyCheckListData(){
        
        showHud()
        let headers = AppManager.shared.getLoggedInHeaders()
        //        headers["AuthorizationToken"] = "YZX2VYHHPM"
        //        headers["UserId"] = "10"
        
        WebServiceHelper.request(path: .myCheckList, method: .post, token: nil, headers: headers, parameters: [:]) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            
                            if let dataList = rootModel.data as? [String:Any] {
                                self.checkListHander = AddCheckListHandler.init(data:dataList)
                                self.tableView_checkList.reloadData()
                                let showPlaceHolder = (self.checkListHander!.allCategories.isEmpty &&
                                    self.checkListHander!.toDoList.isEmpty)
                                
                                self.showHidePlaceholder(show:showPlaceHolder)
                                self.updateCategoryName()
                            }
                            
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                        }
                    }
                }else{
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    
    
    
}


extension CheckListViewController:UIScrollViewDelegate {
    
    func updateCategoryName(){
        if let topSection = self.tableView_checkList.indexPathsForVisibleRows?.first?.section {
            if checkListHander!.allCategories.count > topSection{
                typeOfMove.text = checkListHander!.moveNameOfCategory(category:checkListHander!.allCategories[topSection])
            }else{
                typeOfMove.text = ""
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateCategoryName()
    }
    
}

extension CheckListViewController:UITableViewMethods {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return checkListHander!.allCategories.count + 1
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == checkListHander!.allCategories.count {
            return checkListHander!.toDoList.count
        }
        
        return checkListHander!.allCategories[section].checkListCategoryItem.count
    }
    
    //    func numberOfSections(in tableView: UITableView) -> Int {
    //        return checkListHander!.allCategories.count
    //    }
    //
    //    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //        return checkListHander!.allCategories[section].checkListCategoryItem.count
    //    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == checkListHander!.allCategories.count {
            let cellIdentifier = CheckListToDoItemTableViewCell.cellIdentifier
            let checkCell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier, for: indexPath) as! CheckListToDoItemTableViewCell
            checkCell.cellData = checkListHander!.toDoList[indexPath.row]
            checkCell.buttonDelete.isHidden = true
            checkCell.itemSelectionChanged = { (changedItem) in
                self.invertToDoItemSelection(item:changedItem) {
                    checkCell.toggleSelection()
                }
            }
            return checkCell
            
        }
        
        let cellIdentifier = CheckListTableViewCell.cellIdentifier
        let checkCell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier, for: indexPath) as! CheckListTableViewCell
        let data = checkListHander!.allCategories[indexPath.section].checkListCategoryItem[indexPath.row]
        checkCell.cellData = data
        checkCell.itemSelectionChanged = { (changedItem) in
            self.invertItemSelection(item:changedItem) {
                checkCell.toggleSelection()
            }
        }
        checkCell.didTapLogo = {
            if data.checkListLogoId == 0 {
                if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "SearchDirectoryVC") as? SearchDirectoryVC{
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }else {
                if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "ServiceProviderDetailVC") as? ServiceProviderDetailVC{
                    vc.directoryId = data.checkListLogoId
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        return checkCell
    }
    
    //    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //        let cellIdentifier = CheckListTableViewCell.cellIdentifier
    //        let checkCell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier, for: indexPath) as! CheckListTableViewCell
    //        checkCell.cellData = checkListHander!.allCategories[indexPath.section].checkListCategoryItem[indexPath.row]
    //        return checkCell
    //    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == checkListHander!.allCategories.count {
            return 0
        }
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == checkListHander!.allCategories.count{
            return nil
        }
        
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CheckListHeaderXib.headerIdentifier) as? CheckListHeaderXib else {
            assertionFailure("Failed to load - CheckListHeaderXib")
            return nil
        }
        headerView.headerText.numberOfLines = 0
        headerView.contentView.backgroundColor = UIColor.white
        headerView.headerText.text = checkListHander!.allCategories[section].checkListCategoryName
        headerView.backgroundView = UIView()
        return headerView
    }
    
}
