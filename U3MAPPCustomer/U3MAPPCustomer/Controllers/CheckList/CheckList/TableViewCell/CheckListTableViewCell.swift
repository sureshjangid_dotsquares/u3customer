//
//  CheckListTableViewCell.swift
//  U3MAPPCustomer
//
//  Created by Jagdish Jangir on 27/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class CheckListTableViewCell: UITableViewCell {
    
    static let cellIdentifier = "CheckListTableViewCell"
    
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var checkDescription: UILabel!
    
    
    var itemSelectionChanged:((MyCheckListCategoryItemModel)->())?
    var didTapLogo:(()->())?
    
    var cellData:MyCheckListCategoryItemModel? {
        didSet {
            updateContent()
        }
    }
    
    
    func toggleSelection(){
         guard let _cellData = cellData else {return}
        _cellData.isChecked = !_cellData.isChecked
        updateContent()
    }
    
    private func updateContent(){
        guard let _cellData = cellData else {return}
        if _cellData.isChecked {
            self.checkDescription.attributedText = self.strikeThroughText(text:_cellData.itemName)
            checkButton.setImage(UIImage.init(named:"checklist_icon"), for:.normal)
        }else{
            self.checkDescription.attributedText = NSAttributedString.init(string: _cellData.itemName)
            checkButton.setImage(UIImage.init(named:"checklist_unicon"), for:.normal)
        }
        
        if _cellData.checkListLogoId == 0 {
            actionButton.setImage(UIImage.init(named:"logo_icon"), for:.normal)
        }else {
            
            let url = URL(string: _cellData.checkListLogo)

            
            let img = UIImageView()
            img.kf.setImage(with: url) { result in
                // `result` is either a `.success(RetrieveImageResult)` or a `.failure(KingfisherError)`
                switch result {
                case .success(let value):
                    // The image was set to image view:
                    print(value.image)
                    self.actionButton.setImage(value.image, for:.normal)
                    // From where the image was retrieved:
                    // - .none - Just downloaded.
                    // - .memory - Got from memory cache.
                    // - .disk - Got from disk cache.
                    print(value.cacheType)

                    // The source object which contains information like `url`.
                    print(value.source)

                case .failure(let error):
                    print(error) // The error happens
                }
            }
        }
    }
    
    private func strikeThroughText(text:String?)->NSAttributedString?{
        guard let _text = text else {return nil}
        let attributedString = NSMutableAttributedString(string:_text)
        attributedString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributedString.length))
        return attributedString
    }
    
    
    @IBAction func actionCheck(_ sender: UIButton){
        
        guard let _cellData = cellData else {return}
        itemSelectionChanged?(_cellData)
//        cellData?.isChecked = !cellData!.isChecked
//        self.updateContent()
    }
    
    @IBAction func actionLogo(_ sender: UIButton) {
        didTapLogo?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.backgroundColor = UIColor.white
        containerView.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.05).cgColor
        containerView.layer.shadowOffset = CGSize(width:0, height:9)
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowRadius = 10
        containerView.layer.cornerRadius = 5.0
        
        actionButton.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        actionButton.layer.shadowOffset = CGSize(width:0, height:5)
        actionButton.layer.shadowOpacity = 1.0
        actionButton.layer.shadowRadius = 5
        actionButton.layer.cornerRadius = 4.0
        actionButton.backgroundColor = UIColor.white
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}


