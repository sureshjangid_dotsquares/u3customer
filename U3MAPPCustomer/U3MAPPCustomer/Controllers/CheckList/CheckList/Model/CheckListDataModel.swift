//
//  CheckListDataModel.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 02/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation

class CheckListDataModel : NSObject, NSCoding{

    var checkListCategory : [CheckListCategoryModel]!
    var typeofMove : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        checkListCategory = [CheckListCategoryModel]()
        if let checkListCategoryArray = dictionary["checkListCategory"] as? [[String:Any]]{
            for dic in checkListCategoryArray{
                let value = CheckListCategoryModel(fromDictionary: dic)
                checkListCategory.append(value)
            }
        }
        typeofMove = dictionary["typeofMove"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if checkListCategory != nil{
            var dictionaryElements = [[String:Any]]()
            for checkListCategoryElement in checkListCategory {
                dictionaryElements.append(checkListCategoryElement.toDictionary())
            }
            dictionary["checkListCategory"] = dictionaryElements
        }
        if typeofMove != nil{
            dictionary["typeofMove"] = typeofMove
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         checkListCategory = aDecoder.decodeObject(forKey :"checkListCategory") as? [CheckListCategoryModel]
         typeofMove = aDecoder.decodeObject(forKey: "typeofMove") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if checkListCategory != nil{
            aCoder.encode(checkListCategory, forKey: "checkListCategory")
        }
        if typeofMove != nil{
            aCoder.encode(typeofMove, forKey: "typeofMove")
        }

    }

}
