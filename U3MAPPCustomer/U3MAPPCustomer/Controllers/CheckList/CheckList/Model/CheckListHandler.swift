//
//  CheckListHandler.swift
//  U3MAPPCustomer
//
//  Created by Jagdish Jangir on 29/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation





class CheckListHandler {
    
    private var checkListData:CheckList?
    private(set) var allCategories:[CheckListCategoryModel] = []
    
    var toDoList:[MyCheckListCustomerResponseToDoModel]
    
    init() {
        self.checkListData = CheckList()
        self.toDoList = []
    }
    
    init(data:[[String:Any]]) {
        self.checkListData = CheckList.init(data: data)
        self.toDoList = []
        for move in self.checkListData!.data{
            self.allCategories.append(contentsOf:move.checkListCategory)
        }
    }
    
    func deleteToDoItem(item:MyCheckListCustomerResponseToDoModel){
        if let index = self.toDoList.firstIndex(of:item){
            self.toDoList.remove(at:index)
        }
    }
    
    func selectedCategoryItems()-> Any{
        let moves = self.checkListData!.data
        var moveArray:[Any] = []
        for item in moves {
            var selectedCategories:[Any]  = []
            if let categories = item.checkListCategory,categories.count > 0{
                for cat in categories {
                    if let categoriesItems = cat.checkListCategoryItem,categoriesItems.count > 0 {
                        var selectedCatItems:[Any]  = []
                        for categoriesItem in categoriesItems where categoriesItem.isChecked == true{
                            selectedCatItems.append(categoriesItem.toDictionary())
                        }
                        if selectedCatItems.count > 0 {
                            var allData = cat.toDictionary()
                            allData["checkListCategoryItem"] = selectedCatItems
                            selectedCategories.append(allData)
                        }
                    }
                }
            }
            if selectedCategories.count > 0 {
            var allData = item.toDictionary()
            allData["checkListCategory"] = selectedCategories
            moveArray.append(allData)
            }
        }
        return moveArray
    }
    
    func toDictionary()->[String:Any]{
        var dic:[String:Any] = [:]
        let selectedCategories = selectedCategoryItems()
        dic["masterChecklistItems"] = selectedCategories
        
        var newToDoItem:[Any] = []
        for toDoItem in self.toDoList {
            if toDoItem.itemId == 0 {
                newToDoItem.append(toDoItem.toDictionary())
            }
        }
        dic["customerRequestToDo"] = newToDoItem
        return dic
    }
    
    
    func moveNameOfCategory(category:CheckListCategoryModel)->String{
        for move in self.checkListData!.data{
            for cat in move.checkListCategory {
                if cat === category {
                    return move.typeofMove
                }
            }
        }
        return ""
    }
    
}
