//
//  CheckList.swift
//  U3MAPPCustomer
//
//  Created by Jagdish Jangir on 29/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation


class CheckList:NSObject {
    
    var data:[CheckListDataModel]
    
    init(data:[[String:Any]]) {
        self.data = []
       
        for dic in data {
            self.data.append(CheckListDataModel.init(fromDictionary: dic))
        }
    }
    
    override init() {
        self.data = []
       
    }
    
}
