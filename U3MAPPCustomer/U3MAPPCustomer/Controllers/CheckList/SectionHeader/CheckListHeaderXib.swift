//
//  CheckListHeaderXib.swift
//  U3MAPPCustomer
//
//  Created by Jagdish Jangir on 27/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class CheckListHeaderXib: UITableViewHeaderFooterView {
    
    static let headerIdentifier = "CheckListHeaderXib"
    
    @IBOutlet weak var headerText: UILabel!
    
    static var nib:UINib {
        return UINib.init(nibName:"CheckListHeaderXib", bundle:Bundle.main)
    }
    
    
    
  
    
    
    
    
    
}
