//
//  CongratulationVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 29/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class CongratulationVC: UIViewController {

    //MARK:- IBOutltes
    @IBOutlet weak var viewMain: UIView!
    
    var callback: ((_ status: Bool)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewMain.roundCorners([.layerMinXMinYCorner, .layerMaxXMaxYCorner], radius: 25)
    }
    @IBAction func didTapOnSearch(_ sender: Any) {
        self.dismiss(animated: false) {
            self.callback!(true)
        }
    }
    
    @IBAction func didTapClose(_ sender: Any) {
        self.dismiss(animated: false ) {
            self.callback!(false)
        }
    }
    
    
}
