//
//  3UAgentCell.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 03/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import MapKit

class ThreeUAgentCell: UITableViewCell {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var btnDirection: UIButton!

    var mylatitude = PCLocationManager.shared().myLocation.latitude , mylongitude = PCLocationManager.shared().myLocation.longitude
    var obj : ThreeUAgentModel! {
        didSet {
            let url = URL(string: obj.profilePic)
            imgProfile.kf.setImage(with: url, placeholder: Images.rectPlaceholder.icon)
            lblName.text = obj.name
            lblDistance.text = String(format:"%@ %@",obj.distance,"Miles".localized)
            lblContact.text = obj.contact
        }
    }
    
    @IBAction func didTapDirection(_ sender: UIButton) {
        self.openMap()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func openMap() {
        
        guard obj != nil else {return}
            
            if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                
                UIApplication.shared.open(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(String(describing: (obj?.lat)!)),\(String(describing: (obj?.lng)!))&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
               
                
            } else {
    //            print("You haven't google maps application, please download.")
                
                let coordinate = CLLocationCoordinate2DMake((obj?.lat)!,(obj?.lng)!)
                let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
                mapItem.name = "Target location".localized
                mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
                
                
            }
            
        }
    
}
