//
//  Search3UAgentVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 03/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class Search3UAgentVC: U3BaseViewController {

    //MARK:- IBOutlets declaration
    @IBOutlet weak var txtSearch:CustomTextFieldAnimated!
    @IBOutlet weak var txtLocation:CustomTextFieldAnimated!
    @IBOutlet weak var btnSearch:UIButton!
    @IBOutlet weak var tblAgent:UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var constraintSearchViewHeight: NSLayoutConstraint!
    
    var arrItem = [ThreeUAgentModel]()
    var latitude = PCLocationManager.shared().myLocation.latitude , longitude = PCLocationManager.shared().myLocation.longitude
    var isShowSearchView = false {
        didSet {
            self.showSearchView(status: isShowSearchView)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isShowSearchView = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    // MARK:  setup UI
    func setupUI()
    {
        if let nav = self.navigationController as? U3NavigationController {
            nav.hideNavBottomLine()
        }
        self.title = "Search 3U Agent".localized
        super.setupMenuBtn()
        
        self.wsSearchNearbyDistributor { (status) in
            if status {
                self.isShowSearchView = true
            }else {
                self.isShowSearchView = false
            }
        }
        
        txtLocation.setRightViewImage(Images.location.icon)
        
        txtSearch.title.text = "Search".localized
        txtSearch.placeholder = "Enter Search Distributor".localized
        
    }
    
    func showNoDataText(){
        if arrItem.count > 0{
            lblNoData.isHidden = true
            tblAgent.isHidden = false
        }
        else{
            lblNoData.isHidden = false
            tblAgent.isHidden = true
        }
    }
    
    //MARK:- IBActions
    
    func showSearchView(status: Bool) {
        constraintSearchViewHeight.constant = status ? 280 : 44
        if constraintSearchViewHeight.constant == 280 {
            
            self.txtSearch.isHidden = false
            self.txtLocation.isHidden = false
            self.btnSearch.isHidden = false
        }else {
            self.txtSearch.isHidden = true
            self.txtLocation.isHidden = true
            self.btnSearch.isHidden = true
        }
        UIView.animate(withDuration: 0.3){
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func didTapOnSearchFilter(_ sender: Any) {
        if ((sender as? UIControl) != nil) {
            isShowSearchView = !isShowSearchView
        }
        
    }
    
    @IBAction func didTapOnSearchBtn(_ sender: UIButton) {
        self.wsSearchNearbyDistributor { (status) in
            if status {
                self.isShowSearchView = true
            }else {
                self.isShowSearchView = false
            }
        }
    }
    

}


extension Search3UAgentVC: UITableViewMethods {
    //MARK: - Tableview methods -
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrItem.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ThreeUAgentCell = tableView.dequeueReusableCell(withIdentifier: "ThreeUAgentCell", for: indexPath) as! ThreeUAgentCell
        cell.obj = arrItem[indexPath.row]
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }

}

extension Search3UAgentVC:UITextFieldDelegate {
    // MARK: - UITextField Delegate
    
    /*
     * textFieldShouldBeginEditing method is a textfield delegate method invoke if editing should begin in the specified text field.
     */
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        if textField == txtLocation{
            //self.pickPlace()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            guard let objVC = storyBoard.instantiateViewController(withIdentifier: "LocationMapVC") as? LocationMapVC else {return false}
            objVC.callback = { (lat,lang,address) in
                self.latitude = Double(lat)!;self.longitude = Double(lang)!
                self.txtLocation.text = address
            }
            self.navigationController?.pushViewController(objVC, animated: true)
            return false
        }
        return true
    }
}


//MARK:-API Calling
extension Search3UAgentVC{
    
    func wsSearchNearbyDistributor( completion: ((_ status: Bool) -> Void)? = nil){
        let params : [String:Any] =
            ["TextSearch": txtSearch.text!.trimString() as Any,
             "Lat":self.latitude,//26.8869,
            "Lng":self.longitude]//75.8341 ]
        
        showHud()
        WebServiceHelper.request(path: .searchNearbyDistributor, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let arrResponse = rootModel.data as? [[String:Any]]{
                                self.arrItem.removeAll()
                                self.arrItem = arrResponse.map{return ThreeUAgentModel(fromDictionary: $0)}
                                
                                self.tblAgent.reloadData()
                                self.showNoDataText()
                                completion?(self.arrItem.count == 0 ? false : true)
                                
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                            completion?(false)
                        }
                    }
                }else{
                    completion?(false)
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
}
