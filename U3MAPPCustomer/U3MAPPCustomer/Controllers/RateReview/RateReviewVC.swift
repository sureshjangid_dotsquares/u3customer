//
//  RateReviewVC.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 17/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import GrowingTextView
import FloatRatingView

class RateReviewVC: UIViewController,UITextViewDelegate {
    
    //MARK:- IBOutltes
    @IBOutlet weak var viewMain: UIView!
    
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var RatingView: FloatRatingView!
    @IBOutlet weak var textView: GrowingTextView!
    
    var directoryId: Int!
    var objDirectory: ServiceDirectoryDetailModel!
    var callback: ((ServiceDirectoryDetailModel)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewMain.roundCorners([.layerMinXMinYCorner, .layerMaxXMaxYCorner], radius: 25)
                
        
    }
    
    @IBAction func didTapOnView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func actionSubmit(_ sender: UIButton) {
        
        if RatingView.rating == 0 {
           self.presentAlertWith(message: "Please rate service provider".localized)
            return
        }
        else if textView.text.trimString().isEmpty {
            self.presentAlertWith(message: "Please enter text here".localized)
            return
        }
        
        wsServiceDirectoryDetail { (status) in
            self.dismiss(animated: false) {
                self.callback!(self.objDirectory)
            }
            
        }
        
    }
    
    
}


//MARK:-API Calling
extension RateReviewVC{
    
    func wsServiceDirectoryDetail( completion: ((_ status: Bool) -> Void)? = nil){
        let params : [String:Any] =
            ["DirectoryId":directoryId! as Any,
             "Rate":RatingView.rating,
             "Comment":textView.text.trimString()]
        
        showHud()
        WebServiceHelper.request(path: .rateReviewDirectory, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let dictResponse = rootModel.data as? [String:Any]{
                                if let avgRating = dictResponse["averageRating"] as? Int {
                                    self.presentAlertWith(message: rootModel.successMsg) {
                                        self.objDirectory.currentRating = String(format:"%d",avgRating)
                                        completion?(true)
                                    }
                                }else {
                                    completion?(false)
                                }
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg) {
                                completion?(false)
                            }
                        }
                    }
                }else{
                    completion?(false)
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
}
