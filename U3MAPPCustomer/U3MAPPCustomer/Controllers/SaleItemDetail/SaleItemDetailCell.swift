//
//  SaleItemDetailCell.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 06/04/20.
//  Copyright © 2020 ds. All rights reserved.
//

import Foundation
import CHIPageControl

class SaleItemDetailCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var coltnCard: UICollectionView!
    @IBOutlet weak var verticalPageControl: CHIPageControlJaloro!
    
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblItemCatName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    

    var obj: SaleItemModel! {
        didSet {
            lblItemName.text = obj.itemName
            lblItemCatName.text = obj.itemCategoryName
            
            if obj.isForSell == 1 {
                lblPrice.text = obj.price
            }else {
                lblPrice.text = "Free".localized
            }
            
            self.verticalPageControl.numberOfPages = obj.listOfFiles.count

            coltnCard.dataSource = self
            coltnCard.delegate = self
            coltnCard.reloadData()
            coltnCard.isPagingEnabled = true
            coltnCard.showsHorizontalScrollIndicator = false
            coltnCard.showsVerticalScrollIndicator = false
            
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


extension SaleItemDetailCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCollectionViewCell", for: indexPath) as! CardCollectionViewCell
        let url = URL(string: self.obj.listOfFiles[indexPath.item].fileURL)
        cell.imgBanner.kf.setImage(with: url, placeholder: Images.itemPlaceholder.icon)
        cell.imgBanner.contentMode = .scaleAspectFit
        cell.imgBanner.layer.masksToBounds = true
        cell.backgroundColor = Utility.random()
                
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return obj.listOfFiles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
        
}

extension SaleItemDetailCell: CHIBasePageControlDelegate {
    func didTouch(pager: CHIBasePageControl, index: Int) {
        print(pager, index)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let midX:CGFloat = scrollView.bounds.midX
        let midY:CGFloat = scrollView.bounds.midY
        let point:CGPoint = CGPoint(x:midX, y:midY)
        
        guard
            
            let indexPath:IndexPath = coltnCard.indexPathForItem(at:point)
            
            else
        {
            return
        }
        
        let currentPage:Int = indexPath.item
         self.verticalPageControl.progress = Double(currentPage)
    }
}

