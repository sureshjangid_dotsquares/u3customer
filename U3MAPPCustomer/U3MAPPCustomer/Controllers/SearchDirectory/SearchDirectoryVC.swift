//
//  SearchDirectoryVCViewController.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 17/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import McPicker

class SearchDirectoryVC: U3BaseViewController {

    //MARK:- IBOutlets declaration
    @IBOutlet weak var txtServiceName:CustomTextFieldAnimated!
    @IBOutlet weak var txtBusinessCategory:CustomTextFieldAnimated!
    @IBOutlet weak var txtLocation:CustomTextFieldAnimated!
    @IBOutlet weak var btnSearch:UIButton!
    @IBOutlet weak var tblAgent:UITableView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var constraintSearchViewHeight: NSLayoutConstraint!
    
    var selectedCategoryId = 0
    var arrCategoryItem = [BusinessCategoryModel]()
    let PAGESIZE = 20
    var pageNumber = 1
    var isLoadMore : Bool = false
    var arrItem = [ServiceDirectoryModel]()
    var latitude = PCLocationManager.shared().myLocation.latitude , longitude = PCLocationManager.shared().myLocation.longitude
    var isShowSearchView = false {
        didSet {
            self.showSearchView(status: isShowSearchView)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isShowSearchView = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
    }
    
    // MARK:  setup UI
    func setupUI()
    {
        if let nav = self.navigationController as? U3NavigationController {
            nav.hideNavBottomLine()
        }
        self.title = "Service Directory".localized
        
        if selectedCategoryId == 0 {
            self.setupMenuBtn()
        }else {
            self.backBarBtnPressed()
        }
                
        txtLocation.setRightViewImage(Images.location.icon)
        txtBusinessCategory.setRightViewImage(Images.downArrow.icon)
        
        txtServiceName.title.text = "Service Directory Name".localized
        txtServiceName.placeholder = "Enter Service Directory Name".localized
        
        txtBusinessCategory.title.text = "Business Category".localized
        txtBusinessCategory.placeholder = "Select Category".localized
        
        if selectedCategoryId != 0 {
            self.wsBusinessCategoryList { (status: Bool) in
                if status {
                    let arr = self.arrCategoryItem.filter({$0.id == self.selectedCategoryId})
                    if arr.count > 0 {
                        self.selectedCategoryId = arr[0].id
                        self.txtBusinessCategory.text = arr[0].categoryName
                        self.serviceDirectoryApi()
                    }else {
                        self.selectedCategoryId = 0
                        self.serviceDirectoryApi()
                    }

                }else {
                    self.selectedCategoryId = 0
                    self.serviceDirectoryApi()
                }
            }
        }else {
            selectedCategoryId = 0
            self.serviceDirectoryApi()
        }
    }
    
    func serviceDirectoryApi() {
        self.wsSearchServiceDirectory { (status) in
            if status {
                self.isShowSearchView = true
            }else {
                self.isShowSearchView = false
            }
        }
    }
    
    func showNoDataText(){
        if arrItem.count > 0{
            lblNoData.isHidden = true
            tblAgent.isHidden = false
        }
        else{
            lblNoData.isHidden = false
            tblAgent.isHidden = true
        }
    }
    
    //MARK:- IBActions
    
    func showSearchView(status: Bool) {
        constraintSearchViewHeight.constant = status ? 350 : 44
        if constraintSearchViewHeight.constant == 350 {
            
            self.txtServiceName.isHidden = false
            self.txtLocation.isHidden = false
            self.btnSearch.isHidden = false
        }else {
            self.txtServiceName.isHidden = true
            self.txtLocation.isHidden = true
            self.btnSearch.isHidden = true
        }
        UIView.animate(withDuration: 0.3){
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func didTapOnSearchFilter(_ sender: Any) {
        if ((sender as? UIControl) != nil) {
            isShowSearchView = !isShowSearchView
        }
        
    }
    
    @IBAction func didTapOnSearchBtn(_ sender: UIButton) {
        self.pageNumber = 1
        self.wsSearchServiceDirectory { (status) in
            if status {
                self.isShowSearchView = true
            }else {
                self.isShowSearchView = false
            }
        }
    }
    

}


extension SearchDirectoryVC: UITableViewMethods {
    //MARK: - Tableview methods -
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrItem.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ServiceDirectoryCell = tableView.dequeueReusableCell(withIdentifier: "ServiceDirectoryCell", for: indexPath) as! ServiceDirectoryCell
        cell.obj = arrItem[indexPath.row]
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = Storyboards.homeStoryboard.instantiateViewController(withIdentifier: "ServiceProviderDetailVC") as? ServiceProviderDetailVC{
            vc.directoryId = arrItem[indexPath.row].serviceDirectoryId
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

}

extension SearchDirectoryVC:UITextFieldDelegate {
    // MARK: - UITextField Delegate
    
    /*
     * textFieldShouldBeginEditing method is a textfield delegate method invoke if editing should begin in the specified text field.
     */
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        if textField == txtLocation{
            self.view.endEditing(true)
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            guard let objVC = storyBoard.instantiateViewController(withIdentifier: "LocationMapVC") as? LocationMapVC else {return false}
            objVC.callback = { (lat,lang,address) in
                self.latitude = Double(lat)!;self.longitude = Double(lang)!
                self.txtLocation.text = address
            }
            self.navigationController?.pushViewController(objVC, animated: true)
            return false
        }else if textField == txtBusinessCategory {
            self.view.endEditing(true)
            if self.arrCategoryItem.count > 0 {
                McPicker.show(data: [self.arrCategoryItem.map { String($0.categoryName) }]) {(selections: [Int : String]) -> Void in
                    self.txtBusinessCategory.text = selections[0]
                    let arr = self.arrCategoryItem.filter({$0.categoryName == selections[0]})
                    self.selectedCategoryId = arr[0].id
                }
            }else {
                self.wsBusinessCategoryList { (status: Bool) in
                    if status {
                        McPicker.show(data: [self.arrCategoryItem.map { String($0.categoryName) }]) {(selections: [Int : String]) -> Void in
                            self.txtBusinessCategory.text = selections[0]
                            let arr = self.arrCategoryItem.filter({$0.categoryName == selections[0]})
                            self.selectedCategoryId = arr[0].id
                        }
                    }
                }
            }
            
            return false
        }
        return true
    }
}


//MARK:-API Calling
extension SearchDirectoryVC{
    
    func wsSearchServiceDirectory( completion: ((_ status: Bool) -> Void)? = nil){
        let params : [String:Any] =
            ["DirectoryName":txtServiceName.text!.trimString() as Any,
            "BusinessCategoryId":selectedCategoryId,
            "Latitude":self.latitude,
            "Longitude":self.longitude,
            "Address":"",
            "PageSize":PAGESIZE,
            "PageNumber":pageNumber]
        
        showHud()
        WebServiceHelper.request(path: .searchServiceDirectory, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if self.arrItem.count == 0 || self.pageNumber == 1 {
                    self.arrItem.removeAll()
                }
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let arrResponse = rootModel.data as? [[String:Any]]{
                                let tempArray = arrResponse.map{return ServiceDirectoryModel(fromDictionary: $0)}
                                self.arrItem.append(contentsOf: tempArray)
                                self.isLoadMore = tempArray.count >= self.PAGESIZE ? true : false
                                self.tblAgent.reloadData()
                                self.showNoDataText()
                                completion?(self.arrItem.count == 0 ? false : true)
                                
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                            completion?(false)
                        }
                    }
                }else{
                    completion?(false)
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
    func wsBusinessCategoryList( completion: ((_ status: Bool) -> Void)? = nil){
        let params : [String:Any] = ["":""]
        
        showHud()
        WebServiceHelper.request(path: .businessCategoryList, method: .post, token: nil, headers: AppManager.shared.getLoggedInHeaders(), parameters: params) { (response, error, isSuccess) in
            dissmissHud()
            
            DispatchQueue.main.async {
                
                if isSuccess{
                    if let dictRoot = response as? [String:Any]{
                        let rootModel = RootModel.init(fromDictionary: dictRoot)
                        if rootModel.responseCode == ErrorCode.resultSuccess.rawValue{
                            if let arrResponse = rootModel.data as? [[String:Any]]{
                                self.arrCategoryItem.removeAll()
                                self.arrCategoryItem = arrResponse.map{return BusinessCategoryModel(fromDictionary: $0)}
                                completion?(self.arrCategoryItem.count == 0 ? false : true)
                                
                            }
                        }else{
                            self.presentAlertWith(message: rootModel.failureMsg)
                            completion?(false)
                        }
                    }
                }else{
                    completion?(false)
                    if let dict = response as? [String:Any]{
                        print(dict)
                        self.presentAlertWith(message: "Something went wrong.".localized)
                    }
                }
                
            }
        }
    }
    
}

extension SearchDirectoryVC: UIScrollViewDelegate {
    // MARK:  ScrollView Delegate
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        // UITableView only moves in one direction, y axis
        
        let currentOffset: CGFloat = scrollView.contentOffset.y
        let maximumOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height
        //NSInteger result = maximumOffset - currentOffset;
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            if isLoadMore {
                pageNumber += 1
                self.wsSearchServiceDirectory()
            }
        }
    }
}
