//
//  ServiceDirectoryCell.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 17/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import FloatRatingView
import MapKit

class ServiceDirectoryCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var btnDirection: UIButton!
    @IBOutlet var rateView: FloatRatingView!

    var mylatitude = PCLocationManager.shared().myLocation.latitude , mylongitude = PCLocationManager.shared().myLocation.longitude
    var obj : ServiceDirectoryModel! {
        didSet {
            let url = URL(string: obj.serviceDirectoryLogo)
            imgProfile.kf.setImage(with: url, placeholder: Images.rectPlaceholder.icon)
            lblName.text = obj.directoryName
            lblDistance.text = String(format:"%@ %@",obj.distance, "Miles".localized)
            self.rateView.rating = Double(obj.currentRating) ?? 0.00
            self.rateView.emptyImage = UIImage(named: "star_un_fill")
            self.rateView.fullImage = UIImage(named: "star_fill_icon")
        }
    }
    
    @IBAction func didTapDirection(_ sender: UIButton) {
        self.openMap()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.backgroundColor = UIColor.white
        containerView.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.05).cgColor
        containerView.layer.shadowOffset = CGSize(width:0, height:9)
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowRadius = 10
        containerView.layer.cornerRadius = 5.0
        
        btnDirection.layer.shadowColor = UIColor.init(red:0, green:0, blue:0, alpha:0.08).cgColor
        btnDirection.layer.shadowOffset = CGSize(width:0, height:5)
        btnDirection.layer.shadowOpacity = 1.0
        btnDirection.layer.shadowRadius = 5
        btnDirection.layer.cornerRadius = 4.0
        btnDirection.backgroundColor = UIColor.white
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func openMap() {
        
        guard obj != nil else {return}
            
            if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                
                UIApplication.shared.open(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(String(describing: (obj?.latitude)!)),\(String(describing: (obj?.longitude)!))&directionsmode=driving")! as URL, options: [:], completionHandler: nil)
               
                
            } else {
    //            print("You haven't google maps application, please download.")
                
                let coordinate = CLLocationCoordinate2DMake((obj?.latitude)!,(obj?.longitude)!)
                let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
                mapItem.name = "Target location".localized
                mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
                
                
            }
            
        }
    
}
