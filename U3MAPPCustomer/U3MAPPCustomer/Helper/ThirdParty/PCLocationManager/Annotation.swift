//
//  CustomAnnotation.swift
//  KimiKonnect
//
//  Created by Mani on 18/04/17.
//  Copyright © 2017 Dheeraj Kumar. All rights reserved.
//

import UIKit
import MapKit

class Annotation: NSObject, MKAnnotation {

    var coordinate: CLLocationCoordinate2D
    var title: String?
    var address: String?
    var lblHours: String?
    var lblDistance : String?
    var groupTag: String?
    var annotationTag: Int?
    
   
    
    init(coordinate: CLLocationCoordinate2D, title: NSString, address: NSString, lblHours : String, lblDistance : String, groupTag: String) {
        self.coordinate = coordinate
        self.title = title as String
        self.address = address as String
        
//        self.phoneNumber = phoneNumber
//        self.imageName = imageName
        self.groupTag = groupTag
    }
    
}
