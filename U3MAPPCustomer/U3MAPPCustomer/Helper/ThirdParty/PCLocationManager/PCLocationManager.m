//
//  PCLocationManager.m
//  PopCab
//
//  Created by Vasim Akram on 28/07/15.
//  Copyright (c) 2015 Dheeraj. All rights reserved.
//

#import "PCLocationManager.h"

@implementation PCLocationManager
{

}

@synthesize locationMngr, myLocation;

+ (PCLocationManager*) sharedLocationManager {
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

-(id)init{
    self = [super init];
    
    if(self){
        self.locationMngr = [[CLLocationManager alloc] init];
        self.locationMngr.delegate = self;
        self.locationMngr.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationMngr.distanceFilter = kCLLocationAccuracyHundredMeters ;
        self.myLocation = CLLocationCoordinate2DMake(0, 0);
        
    }
    return self;
}

-(void)startUpdateingLocation{
    
    //    if(isAlreadyUpdatingLocation){
    //        return;
    //    }
    //    else{
    
  //  [self.locationMngr requestAlwaysAuthorization];
    [self.locationMngr requestWhenInUseAuthorization];
    
    [self.locationMngr startUpdatingLocation];
    isAlreadyUpdatingLocation = YES;
    //    }
}

-(void)stopUpdationgLocation{
    [self.locationMngr stopUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    if(manager.location.coordinate.latitude == self.myLocation.latitude && manager.location.coordinate.longitude == self.myLocation.longitude){
        return;
    }
    NSLog(@":::::::::location did update::::::::::::");
    self.myLocation = manager.location.coordinate;

}



@end
