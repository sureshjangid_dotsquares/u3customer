//
//  HudShowHide.swift
//  Generic
//
//  Created by Vinod Kumar on 28/09/17.
//  Copyright © 2017 Vinod Kumar. All rights reserved.
//

import UIKit
import SVProgressHUD


   // hud show function
   func showHud(){
    DispatchQueue.main.async {
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.clear)
    }
  }


    // hud hide function
    func dissmissHud(){
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }

    }

func showHudWithMessage(message: String){
    DispatchQueue.main.async {
        SVProgressHUD.show(withStatus: message)
        SVProgressHUD.setDefaultMaskType(.gradient)
    }
}


