//
//  UITextField.swift
//  U3MAPPCustomer
//
//  Created by Dheeraj Kumar on 09/01/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

import UIKit

private var kAssociationKeyMaxLength: Int = 0

extension UITextField {
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
   // MARK:- Left view tint color.
    @IBInspectable public var leftViewTintColor: UIColor? {
        get {
            guard let iconView = leftView as? UIImageView else { return nil }
            return iconView.tintColor
        }
        set {
            guard let iconView = leftView as? UIImageView else { return }
            iconView.image = iconView.image?.withRenderingMode(.alwaysTemplate)
            iconView.tintColor = newValue
        }
    }
    
    // MARK:- Right view tint color.
    @IBInspectable public var rightViewTintColor: UIColor? {
        get {
            guard let iconView = rightView as? UIImageView else { return nil }
            return iconView.tintColor
        }
        set {
            guard let iconView = rightView as? UIImageView else { return }
            iconView.image = iconView.image?.withRenderingMode(.alwaysTemplate)
            iconView.tintColor = newValue
        }
    }
    
    @IBInspectable
    var leftImage: UIImage? {
        get{
            return (self.leftView as? UIImageView)?.image
        }
        set {
            guard let image=newValue else {return}
            self.setLeftImage(image: image)
        }
    }
    @IBInspectable
    var rightImage: UIImage? {
        get{
            return (self.rightView as? UIImageView)?.image
        }
        set {
            guard let image=newValue else {return}
            self.setRightImage(image: image)
        }
    }
    
    
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
    
    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }
    
    @objc func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }
        
        let selection = selectedTextRange
        let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        text = String(prospectiveText[..<maxCharIndex])
        selectedTextRange = selection
    }
    
    // MARK:- Clear text.
    public func clear() {
        text = ""
        attributedText = NSAttributedString(string: "")
    }
    
    // MARK:- Set placeholder text color.
    ///
    /// - Parameter color: placeholder text color.
    public func setPlaceHolderTextColor(_ color: UIColor) {
        guard let holder = placeholder, !holder.isEmpty else { return }
        self.attributedPlaceholder = NSAttributedString(string: holder, attributes: [NSAttributedString.Key.foregroundColor: color])
    }
    
    // MARK:- Add padding to the left of the textfield rect.
    ///
    /// - Parameter padding: amount of padding to apply to the left of the textfield rect.
    public func addPaddingLeft(_ padding: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: padding, height: frame.height))
        leftView = paddingView
        leftViewMode = .always
    }
    
    // MARK:- Add padding to the left of the textfield rect.
    ///
    /// - Parameters:
    ///   - image: left image
    ///   - padding: amount of padding between icon and the left of textfield
    public func addPaddingLeftIcon(_ image: UIImage, padding: CGFloat) {
        let imageView = UIImageView(image: image)
        imageView.contentMode = .center
        self.leftView = imageView
        self.leftView?.frame.size = CGSize(width: image.size.width + padding, height: image.size.height)
        self.leftViewMode = UITextField.ViewMode.always
    }
}

extension UITextField {

    ///Adding gray view as leftside of UITextField
//    @IBInspectable var addLeftView: Bool {
//        get {
//            return false
//        } set {
//            if newValue {
//                let left: UIView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 15.0, height: self.frame.size.height))
//                left.backgroundColor = UIColor.gray
//                leftViewMode = .always
//                self.leftView = left
//
//            }
//        }
//    }
    
    //MARK:- Set Left image
    /*
     *    Below method is used to set left image to text field.
     *      User can select it from storyboard.
     *     Default widht of left image is 39.
     */
    func setLeftImage(image: UIImage?,width:CGFloat=30)  {
        guard let img=image else {return}
        let iconView = UIImageView(frame:
                       CGRect(x: 10, y: 10, width: width, height: 30))
        iconView.image = img
        iconView.contentMode = .center
        let iconContainerView: UIView = UIView(frame:
                       CGRect(x: 20, y: 0, width: 50, height: 50))
        iconContainerView.addSubview(iconView)
        leftViewMode = .always
        self.leftView = iconContainerView
    }
    //MARK:- Set Right image
    /*
     *    Below method is used to set right image to text field.
     *      User can select it from storyboard.
     *     Default widht of right image is 39.
     */
    func setRightImage(image: UIImage?,width:CGFloat=30)  {
        guard let img=image else {return}
        
        let iconView = UIImageView(frame:
                       CGRect(x: 10, y: 10, width: width, height: 30))
        iconView.image = img
        iconView.contentMode = .center
        let iconContainerView: UIView = UIView(frame:
                       CGRect(x: 20, y: 0, width: 50, height: 50))
        iconContainerView.addSubview(iconView)
        rightView = iconContainerView
        rightViewMode = .always
    }
    
}

