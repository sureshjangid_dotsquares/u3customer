//
//  UIImageView.swift
//  U3MAPPCustomer
//
//  Created by Abhishek on 16/03/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    func setImage(from url: URL, placeholderImage img: UIImage ){
        kf.setImage(with: url, placeholder: img)
    }
}


