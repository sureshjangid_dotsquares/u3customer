//
//  CurveView.swift
//  U3MAPPCustomer
//
//  Created by Ganesh on 27/02/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

class CurveView: UIView {
    var path: UIBezierPath!
 
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        self.setGradient(colors: [rFirstCgColor.cgColor,rSecondCgColor.cgColor,rThirdCgColor.cgColor], angle: 315)
        self.maskVsSublayer()
 
    }
 
     func maskVsSublayer() {
         self.createTriangle()
      
         let shapeLayer = CAShapeLayer()
         shapeLayer.path = path.cgPath
         shapeLayer.fillColor = UIColor.white.cgColor
         self.layer.addSublayer(shapeLayer)
     }
    func createTriangle() {
        path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: self.frame.size.height*0.60))
        path.addLine(to: CGPoint(x: 0.0, y: self.frame.size.height))
        path.addLine(to: CGPoint(x: self.frame.size.width, y: self.frame.size.height))
        path.close()
    }
    
    
}
