
//
//  CreateProfileWithSocialMedia.swift
//  SYND
//
//  Created by Gunjan on 28/08/19.
//  Copyright © 2019 Bhavik. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import Foundation

enum SocialType {
    case google
    case instagram
    case facebook
    case twitter
    case linkedin
}

enum SocialProfileType: String {
    case twitter    = "Twitter"
    case linkedin  = "Linkedin"
}
//Usage

/*CreateProfileWithSocialMedia.shared.loginWithSocialMedia(type: SocialType.instagram) { (result) in
 
 }*/


class CreateProfileWithSocialMedia : NSObject {
    
    // MARK: Shared Instance
    
    static let shared: CreateProfileWithSocialMedia = {
        return CreateProfileWithSocialMedia()
    }()
    
    var socialCompletion : ((_ socialDict:[String:Any]) -> ())?
    
    
    func loginWithSocialMedia(type:SocialType,completion:@escaping (_ socialDict:[String:Any]) -> ())  {
        socialCompletion = completion
        switch type {
        case .twitter:
            twitterLogin()
        case .linkedin:
            linkedinLogin()
        default :
            linkedinLogin()
        }
        
    }
    
    
    // MARK: LinkedinLogin
    func linkedinLogin() {
        guard  let rootViewController=(UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController else {
            return
        }
        let linkedinSharedVC: LinkedinSharedVC = LinkedinSharedVC.init(clientId: LinkedinConstants.clientId, redirectUri: LinkedinConstants.redirectUri) { (token, error) in
            UserDefaults.standard.set(token ?? "", forKey: "linkedin_authorization_code")
            
            rootViewController.dismiss(animated: true, completion: {
                if (UserDefaults.standard.value(forKey:"linkedin_authorization_code") != nil){
                    self.getLinkedinAccessToken()
                }else{
                    print("user is not logged in")
                }
                
            })
        }
        linkedinSharedVC.edgesForExtendedLayout = []
        rootViewController.present(UINavigationController(rootViewController: linkedinSharedVC), animated: true)
        
    }
    
}



// MARK: Linkedin Login Methods
extension CreateProfileWithSocialMedia  {
    
    fileprivate func getLinkedinAccessToken(){
        LinkedinUserProfile.shared.getLinkedinLoggedInUserAccessTokenWithUrl(comptionBlock: { (accessToken, status) in
            
            if status {
                if (UserDefaults.standard.value(forKey:"linkedin_access_token") != nil){
                    self.getLinkedinEmailAddress()
                }else{
                    print("access token expired")
                }
            }else{
                print("failed to fetch access token")
            }
        })
    }
    
    
    fileprivate func getLinkedinEmailAddress(){
        LinkedinUserProfile.shared.getLinkedinLoggedInUserEmailAddressWithUrl(comptionBlock: { (emailAddress, status) in
            
            if status {
                self.getLinkedinUserData(emailAddress: emailAddress ?? "" )
               
                
            }else{
                print("failed to fetch user data")
            }
        })
    }
    
    
    fileprivate func getLinkedinUserData(emailAddress:String){
        LinkedinUserProfile.shared.getLinkedinLoggedInUserInfoWithUrl(comptionBlock: { (linkedinParams, status) in
            if status {
                
                self.getLinkedinProfilePicture(emailAddress: emailAddress, linkedinParams: linkedinParams ?? SocialProfileUserModel() )
               
            }else{
                print("failed to fetch user data")
            }
        })
    }
    
    fileprivate func getLinkedinProfilePicture(emailAddress: String,linkedinParams:SocialProfileUserModel){
        
        LinkedinUserProfile.shared.getLinkedinLoggedInUserProfilePictureWithUrl(comptionBlock: { (profilePicture, status) in
            
            if status {
                var dict = linkedinParams.toDictionary()
                dict["email"] = emailAddress
                dict["profile_picture"] = profilePicture
                self.socialCompletion!((dict))
            }else{
                if profilePicture?.count == 0 {
                    var dict = linkedinParams.toDictionary()
                    dict["email"] = emailAddress
                    dict["profile_picture"] = profilePicture
                    self.socialCompletion!((dict))
                }
                else {
                    print("failed to fetch user data")
                    guard  let rootViewController=(UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController else {
                        return
                    }
                    rootViewController.presentAlertWith(message: "Having trouble in fetching data from linkedin. Please try again later".localized)
                }
                
            }
        })
    }
    
}





// MARK: Twitter Login Methods
extension CreateProfileWithSocialMedia  {
    
    func twitterLogin () {
        
    }
    
}

