//
//  FacebookRequest.swift
//  Created by Abhishek Jangid on 06/09/19.
//  Copyright © 2019 Abhishek Jangid. All rights reserved.
//

import Foundation
import FBSDKLoginKit

class FacebookRequest: NSObject {
    
    static var completionHandlerFB: ((_ isSuccess: Bool, _ response: FbModel?) -> ())!
    
    static func fbRedirectToFetchData(_ requestParameter:[String: AnyObject]?,isShowIndicator:Bool = true,inViewController:UIViewController,completionHander:@escaping ((_ isSuccess: Bool, _ response: FbModel?) -> ())) {
        
        completionHandlerFB = completionHander
        FacebookRequest.btnActImportFromFacebook(inViewController)
    }
    
    static func btnActImportFromFacebook(_ fromVC:UIViewController)
    {
        //showMBProgressHUD(fromVC.view)
        
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logOut()

        //fbLoginManager.loginBehavior = FBSDKLoginBehavior.SystemAccount
        fbLoginManager.logIn(permissions: ["public_profile","email"/*,"user_friends","user_about_me"*/],from: fromVC, handler: { (result, error) -> Void in
            
            if(error != nil) {
                
                
                
                //hideMBProgressHUD(fromVC.view)
                var dictFailResponce : [String : String] = [:]
                dictFailResponce["title"] = "Error"
                dictFailResponce["message"] = error.debugDescription
                
                let objFbModel = FbModel(fromDictionary: dictFailResponce)
                
                completionHandlerFB(false,objFbModel)
                
                if let loginManager: LoginManager = LoginManager() {
                    loginManager.logOut()
                }
                
            } else if (result?.isCancelled)! {
                
                //print("Facebook Login Cancelled")
                //hideMBProgressHUD(fromVC.view)
                completionHandlerFB(false,nil)

                
            } else {
                
                if (error == nil) {
                    
                    let fbloginresult : LoginManagerLoginResult = result!
                    
                    if (fbloginresult.isCancelled) {
                        
                        // Handle cancellations
                        //hideMBProgressHUD(fromVC.view)
                        completionHandlerFB(false, nil)
                        
                    } else if(fbloginresult.grantedPermissions.contains("email")) {
                        FacebookRequest.returnUserData(fromVC)
                    }
                }
            }
        })
    }

    
    //MARK: - Facebook Login
    static func returnUserData(_ fromVC:UIViewController) {
        
        if((AccessToken.current) != nil) {
            
            //showMBProgressHUD(fromVC.view)
            
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil) {
                    
                    if ((result as! [String:Any])["id"] as? String == nil) {
                        
                        //hideMBProgressHUD(fromVC.view)
                        //fromVC.showAlertWithTitle("Login with Facebook", message: "Please retry and kindly provide basic facebook permisions to create your account!.")
                        
                        var dictFailResponce : [String : String] = [:]
                        dictFailResponce["title"] = NSLocalizedString("err_FBLoginWith", comment: "")
                        dictFailResponce["message"] = NSLocalizedString("err_FBPermissionEnable", comment: "")
                        
                        let objFbModel = FbModel(fromDictionary: dictFailResponce)
                        
                        completionHandlerFB(false,objFbModel)
                        
                    } else if ((result as! [String:Any])["email"] as? String == nil) {
                        
                        //hideMBProgressHUD(fromVC.view)
                        //fromVC.showAlertWithTitle("Facebook Privacy", message: "Unable to access email-id from facebook!\nKindly update privacy settings of facebook and share email-id to continue")
                        
                        var dictFailResponce : [String : String] = [:]
                        dictFailResponce["title"] = NSLocalizedString("err_FBPrivacy", comment: "")
                        dictFailResponce["message"] = NSLocalizedString("err_FBEmailProblem", comment: "")
                        
                        let objFbModel = FbModel(fromDictionary: dictFailResponce)
                        
                        completionHandlerFB(false,objFbModel)
                        
                    } else {
                        
                        //print("result=====",result)
                        
                        var dictFBUserData : [String : String] = [:]
                        if((result as! [String:Any])["first_name"] as? String == nil && (result as! [String:Any])["last_name"] as? String == nil) {
                            dictFBUserData["full_name"] = (result as! [String:Any])["name"] as? String
                            dictFBUserData["first_name"] = ""
                            dictFBUserData["last_name"] = ""
                        } else {
                            let strFirstName = (result as! [String:Any])["first_name"] as! String
                            let strLastName = (result as! [String:Any])["last_name"] as! String
                            dictFBUserData["full_name"] = strFirstName + " " + strLastName
                            dictFBUserData["first_name"] = strFirstName
                            dictFBUserData["last_name"] = strLastName
                        }
                        
                        dictFBUserData["email"] = (result as! [String:Any])["email"] as? String
                        
                        dictFBUserData["facebook_token"] = AccessToken.current?.tokenString
                        dictFBUserData["facebook_id"] = (result as! [String:Any])["id"] as? String
                        
                        if((result as! [String:Any])["picture.type(large)"] != nil) {
                            dictFBUserData["profile_image"] = (result as! [String:Any])["picture.type(large)"] as? String
                        } else {
                            var strProfilePicUrl : NSString = ""
                            if((result as! [String:Any])["picture"] != nil) {
                                let dictPicture = (result as! [String:Any])["picture"] as? NSDictionary
                                if(dictPicture != nil && dictPicture?.value(forKey: "data") != nil) {
                                    let dictData = dictPicture?.value(forKey: "data") as? NSDictionary
                                    if(dictData != nil && dictData?.value(forKey: "url") != nil) {
                                        strProfilePicUrl = dictData?.value(forKey: "url") as! NSString
                                    }
                                }
                            }
                            dictFBUserData["profile_image"] = strProfilePicUrl as String
                        }
                        
                        let objFbModel = FbModel(fromDictionary: dictFBUserData)
                        
                        //hideMBProgressHUD(fromVC.view)
                        FacebookRequest.completionHandlerFB(true,objFbModel)
                        
                    }
                    
                } else {
                    
                    //hideMBProgressHUD(fromVC.view)
                    //fromVC.showAlertWithTitle(AppName, message: error.description)
                    
                    var dictFailResponce : [String : String] = [:]
                    dictFailResponce["title"] = APPLICATION_NAME
                    dictFailResponce["message"] = error.debugDescription
                    
                    let objFbModel = FbModel(fromDictionary: dictFailResponce)
                    
                    completionHandlerFB(false,objFbModel)
                    
                    if let loginManager: LoginManager = LoginManager() {
                        loginManager.logOut()
                    }
                }
                }
            )
        } else {
            
            //hideMBProgressHUD(fromVC.view)
            completionHandlerFB(false,nil)

            if let loginManager: LoginManager = LoginManager() {
                loginManager.logOut()
            }
        }
    }
    
}


