//
//  SocialHelperExtension.swift
//  U3MAPPCustomer
//
//  Created by Abhishek Jangid on 13/04/20.
//  Copyright © 2020 ds. All rights reserved.
//

import UIKit

import GoogleSignIn
import FBSDKLoginKit
import FBSDKCoreKit
import FBSDKLoginKit


enum SocialLoginType:String{
    case Google_Plus = "Google"
    case Facebook = "Facebook"
    
}


extension UIViewController{
    
    //MARK:- Google Sign In
     func loginWithGoogle()
    {
        
         GIDSignIn.sharedInstance()?.presentingViewController = self
         GIDSignIn.sharedInstance().signIn()
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.googleDataReceived(notification:)), name: .ReceivedGoogleData, object: nil)
    }
    
    //MARK:- GIDSignInUIDelegate
    
    public func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    public func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    public func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
}
