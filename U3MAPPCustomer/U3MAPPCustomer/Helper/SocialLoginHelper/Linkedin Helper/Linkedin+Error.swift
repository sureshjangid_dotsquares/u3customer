//
//  Linkedin+Error.swift
//  SYND
//
//  Created by Gunjan on 28/08/19.
//  Copyright © 2019 Bhavik. All rights reserved.
//

public struct LinkedinError: Error {
    
    // MARK: - Properties
    
    let kind: ErrorKind
    let message: String
    
    /// Retrieve the localized description for this error.
    public var localizedDescription: String {
        return "[\(kind.description)] - \(message)"
    }
    
    // MARK: - Types
    
    enum ErrorKind: CustomStringConvertible {
        case invalidRequest
        
        var description: String {
            switch self {
            case .invalidRequest:
                return "invalidRequest"
            }
        }
    }
    
}
