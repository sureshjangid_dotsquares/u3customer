//
//  Instagram+Access.swift
//  SYND
//
//  Created by Gunjan on 28/08/19.
//  Copyright © 2019 Bhavik. All rights reserved.
//

import Foundation

public enum LinkedinScope: String {
    
    case liteProfile = "r_liteprofile"
    //case fullProfile = "r_fullprofile"
    case emailAddress = "r_emailaddress"
    case memberSocial = "w_member_social"
}
