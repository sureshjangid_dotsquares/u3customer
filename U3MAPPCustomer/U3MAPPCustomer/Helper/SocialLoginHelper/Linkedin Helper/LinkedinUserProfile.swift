//
//  InstagramUserProfile.swift
//  SYND
//
//  Created by Gunjan on 28/08/19.
//  Copyright © 2019 Bhavik. All rights reserved.
//
import UIKit
import Alamofire

typealias LinkedinCompletionHandler = (( _ userInfo:SocialProfileUserModel?, _ status:Bool)->())?
typealias linkedinAccessTokenCompletionHandler = (( _ accessToken:String?, _ status:Bool)->())?
typealias linkedinEmailAddressCompletionHandler = (( _ emailAddress:String?, _ status:Bool)->())?

class LinkedinUserProfile: NSObject {
    static let shared = LinkedinUserProfile()
    
    // MARK: - Properties
    //private var client: (id: String, redirectUri: String)
    
    // MARK: - Public Properties
    
    
    public var linkedinScopes: [LinkedinScope] = [.emailAddress,.memberSocial,.liteProfile]
    
    var strNextPageURL = ""
    var isAPICallInProgress = false
    var isLoadedAllPhotos = false
    
    private enum API {
        static let authURL = "https://www.linkedin.com/oauth/v2/authorization"
        static let accessTokenURL = "https://www.linkedin.com/oauth/v2/accessToken"
        static let baseURL = "https://api.linkedin.com/v2"
    }
    
    func getLinkedinLoggedInUserAccessTokenWithUrl(comptionBlock:linkedinAccessTokenCompletionHandler){
        
        if isAPICallInProgress{
            return
        }
        
        let finalAPICallUrl = API.accessTokenURL
        getLinkedinLoggedInUserAccessToken(url: finalAPICallUrl, comptionBlock:comptionBlock)
    }
    
    func getLinkedinLoggedInUserInfoWithUrl(comptionBlock:LinkedinCompletionHandler){
        
        if isAPICallInProgress{
            return
        }
        let finalAPICallUrl = API.baseURL + "/me"
        getLinkedinLoggedInUserInfo(url: finalAPICallUrl, comptionBlock:comptionBlock)
    }
    
    func getLinkedinLoggedInUserEmailAddressWithUrl(comptionBlock:linkedinEmailAddressCompletionHandler){
        
        if isAPICallInProgress{
            return
        }
        
        let finalAPICallUrl = API.baseURL + "/emailAddress?q=members&projection=(elements*(handle~))"
        getLinkedinLoggedEmailAddress(url: finalAPICallUrl, comptionBlock:comptionBlock)
    }
    
    func getLinkedinLoggedInUserProfilePictureWithUrl(comptionBlock:linkedinEmailAddressCompletionHandler){
        
        if isAPICallInProgress{
            return
        }
        
        guard let profilePicId = UserDefaults.standard.value(forKey:"linkedin_profilePicId") as? String else {
            getLinkedinLoggedProfilePicture(url: "", comptionBlock:comptionBlock)
            return
        }
        let profileIdArr = profilePicId.components(separatedBy: ":")
        let profileId = profileIdArr.last ?? ""
        
        let finalAPICallUrl = "https://api.linkedin.com/v2/me?projection=(\(profileId),profilePicture(displayImage~:playableStreams))"
        getLinkedinLoggedProfilePicture(url: finalAPICallUrl, comptionBlock:comptionBlock)
    }
    
    //    MARK: - API calling with JSON data response
    func getLinkedinLoggedInUserInfo(url:String,comptionBlock:LinkedinCompletionHandler) {
        
        isAPICallInProgress = true
        
        var dictHeader : Dictionary<String,String> = [:]
        
        guard let accessToken = UserDefaults.standard.value(forKey:"linkedin_access_token") else {
            return
        }
        let authToken = "Bearer \(accessToken )"
        dictHeader["Authorization"] = authToken
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: dictHeader).validate().responseJSON { (response) in
            self.isAPICallInProgress = false
            
            switch response.result {
            case .success:
                let dictResponse = response.result.value as? [String:Any]
                let profilePicDict = dictResponse?["profilePicture"] as? [String:String]
                var dictionary = [String:Any]()
                dictionary["bio"] = ""
                dictionary["first_name"] = dictResponse?["localizedFirstName"]
                dictionary["last_name"] = dictResponse?["localizedLastName"]
                dictionary["full_name"] =  ""
                dictionary["id"] = dictResponse?["id"]
                dictionary["is_business"] = false
                let profilePicId = profilePicDict?["displayImage"]
                dictionary["profile_picture"] = profilePicId
                dictionary["username"] = ""
                dictionary["website"] = ""
                dictionary["user_id"] = ""
                dictionary["email"] = ""
                dictionary["refresh_token"] = accessToken
                
                UserDefaults.standard.set(profilePicDict?["displayImage"] , forKey: "linkedin_profilePicId")
                if ((dictResponse?["localizedFirstName"] as? String) != nil){
                    let userData = dictionary
                    let userModel = SocialProfileUserModel(fromDictionary: userData)
                    comptionBlock!(userModel,true)
                }else{
                    comptionBlock!(SocialProfileUserModel(),true)
                }
                
                
            case .failure(let error):
                print(error)
                comptionBlock!(SocialProfileUserModel(),false)
            }
        }
    }
    
    //    MARK: - API calling with JSON data response
    func getLinkedinLoggedEmailAddress(url:String,comptionBlock:linkedinEmailAddressCompletionHandler) {
        
        isAPICallInProgress = true
        
        var dictHeader : Dictionary<String,String> = [:]
        
        guard let accessToken = UserDefaults.standard.value(forKey:"linkedin_access_token") else {
            return
        }
        let authToken = "Bearer \(accessToken )"
        dictHeader["Authorization"] = authToken
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: dictHeader).validate().responseJSON { (response) in
            self.isAPICallInProgress = false
            
            switch response.result {
            case .success:
                let dictResponse = response.result.value as? NSDictionary
                let emailDict = dictResponse?.value(forKey:"elements") as? [[String : Any]]
                let firstElement = emailDict?.first
                if let emailHandle = firstElement?["handle~"] as? [String : Any]{
                    if let email = emailHandle["emailAddress"] as? String{
                        UserDefaults.standard.set(accessToken , forKey: "linkedin_email")
                        comptionBlock!(email,true)
                    }
                    
                }else{
                    comptionBlock!("",false)
                }
            case .failure(let error):
                print(error)
                comptionBlock!("",false)
            }
        }
    }
    
    func getLinkedinLoggedProfilePicture(url:String,comptionBlock:linkedinEmailAddressCompletionHandler) {
        
        if url.count == 0 {
             comptionBlock!("",false)
            return
        }
        
        isAPICallInProgress = true
        
        var dictHeader : Dictionary<String,String> = [:]
        
        guard let accessToken = UserDefaults.standard.value(forKey:"linkedin_access_token") else {
            return
        }
        let authToken = "Bearer \(accessToken )"
        dictHeader["Authorization"] = authToken
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: dictHeader).validate().responseJSON { (response) in
            self.isAPICallInProgress = false
            
            switch response.result {
            case .success:
                let dictResponse = response.result.value as? NSDictionary
                if let profilePicDict = dictResponse?.value(forKey:"profilePicture") as? [String : Any] {
                    if  let displayImageDict = profilePicDict["displayImage~"] as? [String : Any] {
                        if let elementsArray = displayImageDict["elements"] as? [[String : Any]] {
                            if let firstElement = elementsArray.first {
                                if let identifiersArray = firstElement["identifiers"] as? [[String : Any]]{
                                    let firstIdentifiersElement = identifiersArray.first
                                    if let profilePic = firstIdentifiersElement?["identifier"] as? String{
                                        UserDefaults.standard.set(accessToken , forKey: "linkedin_profilePic")
                                        comptionBlock!(profilePic,true)
                                    }
                                    
                                }else{
                                    comptionBlock!("",false)
                                }
                            }
                        }
                    }
                }
                
            case .failure(let error):
                print(error)
                comptionBlock!("",false)
            }
        }
    }
    
    func getLinkedinLoggedInUserAccessToken(url:String,comptionBlock:linkedinAccessTokenCompletionHandler) {
        
        isAPICallInProgress = true
        let authorizationCode = UserDefaults.standard.value(forKey:"linkedin_authorization_code")!
        let parameters: [String : Any] = [
            "grant_type": "authorization_code" as Any,
            "code": authorizationCode as Any,
            "redirect_uri": LinkedinConstants.redirectUri  as Any,
            "client_id": LinkedinConstants.clientId as Any,
            "client_secret": LinkedinConstants.clientSecret as Any
        ]
        
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).validate().responseJSON { (response) in
            self.isAPICallInProgress = false
            
            switch response.result {
            case .success:
                let dictResponse = response.result.value as? NSDictionary
                
                if let accessToken = dictResponse?.value(forKey:"access_token") as? String{
                    UserDefaults.standard.set(accessToken , forKey: "linkedin_access_token")
                    comptionBlock!(accessToken,true)
                }else{
                    comptionBlock!("",false)
                }
                
            case .failure(let error):
                print(error)
                comptionBlock!("",false)
            }
        }
    }
    
    
}
