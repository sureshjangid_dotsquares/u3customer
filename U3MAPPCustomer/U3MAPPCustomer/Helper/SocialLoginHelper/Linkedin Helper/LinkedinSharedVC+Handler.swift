//
//  InstagramSharedVC+Handler.swift
//  SYND
//
//  Created by Gunjan on 28/08/19.
//  Copyright © 2019 Bhavik. All rights reserved.
//

import Foundation

extension LinkedinSharedVC{
    
    @objc func dismissLoginViewController() {
        self.dismiss(animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "remove_hud_linkedin_login"), object: nil)
        }
    }
    
    @objc func refreshPage() {
        self.reloadPage()
    }
}
