//
//  InstagramLoginVC+WKWebview.swift
//  SYND
//
//  Created by Gunjan on 28/08/19.
//  Copyright © 2019 Bhavik. All rights reserved.
//

// MARK: - WKNavigationDelegate

import WebKit
import UIKit

extension LinkedinSharedVC: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        showHud()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        dissmissHud()
    }
    
    public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        dissmissHud()
        if navigationItem.title == nil {
            navigationItem.title = webView.title
        }
    }
    
    public func webView(_ webView: WKWebView,
                        decidePolicyFor navigationAction: WKNavigationAction,
                        decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url{
            if url.host == LinkedinConstants.host {
                if url.absoluteString.range(of: "code") != nil {
                    // Extract the authorization code
                    
                    let urlParts = url.absoluteString.components(separatedBy: "?")
                    let code = urlParts[1].components(separatedBy: "=")[1]
                    linkedinCompltionHand!(code,nil)
                    
                }
            }
            
        }
        
        
        decisionHandler(.allow)
    }
    
    public func webView(_ webView: WKWebView,
                        decidePolicyFor navigationResponse: WKNavigationResponse,
                        decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        
        if let response = navigationResponse.response as? HTTPURLResponse, response.statusCode == 400 {
            linkedinCompltionHand!(nil,LinkedinError(kind: .invalidRequest, message: "Invalid request"))
        }
        
        decisionHandler(.allow)
    }
}
